## 1.0.3

* Enabled videos for web support 
* IsarDB is still on-hold for [web support](https://github.com/isar/isar/issues/686) 
* In future there will be only web package with [HIVE](https://pub.dev/packages/hive) instead of IsarDB

## 1.0.2

* Introduced passing further Schema objects to post list

## 1.0.1

* pub.dev - lints analysis

## 1.0.0

* Refactored repositories
* Included new and detailed README
* Introduced create in example SocialApp Template

## 0.0.5

* Improved queries

## 0.0.4

* Added [performance_indicator](https://pub.dev/packages/performance_indicator)

## 0.0.3

* Improved speed for database queries

## 0.0.2

* Added example for template
* Added readme

## 0.0.1

* Init package.
