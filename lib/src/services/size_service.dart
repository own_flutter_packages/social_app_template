import 'dart:async';

import 'package:flutter/material.dart';
import 'package:maps_toolkit/maps_toolkit.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Provides functionality in size issues.
class SizeService {
  static final SizeService _singleton = SizeService._internal();

  factory SizeService() {
    return _singleton;
  }

  SizeService._internal();

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<SizeService> init({Function(int)? before}) async {
    if (before != null) {
      before(-1);
    }

    await Future.delayed(const Duration(milliseconds: 0));

    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Returns valid size from media.
  static Size? getSizeFromMedia(
    List<PlatformMedia> mediaList, {
    bool onlyFirst = false,
  }) {
    double? mediaWidth;
    double? mediaHeight;

    try {
      for (var media in mediaList) {
        final allowedToTake = onlyFirst ? mediaList.indexOf(media) == 0 : true;

        if (allowedToTake) {
          if (media.width != null &&
              SizeService.isSizeValueValid(media.width!)) {
            mediaWidth = media.width;
          }
          if (media.height != null &&
              SizeService.isSizeValueValid(media.height!)) {
            mediaHeight = media.height;
          }
        }
      }
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'getSizeFromMedia');
    }

    return mediaWidth != null && mediaHeight != null
        ? Size(mediaWidth, mediaHeight)
        : null;
  }

  /// Returns valid size from media adjusted by width.
  ///
  /// Media height is adjusted by width.
  static Size getSizeFromMediaAdjustedByWidth(
    List<PlatformMedia> mediaList,
    double width, {
    bool onlyFirst = false,
  }) {
    final size = getSizeFromMedia(mediaList, onlyFirst: onlyFirst);

    return size != null
        ? SizeService.adjustHeightToWidth(size, width)
        : Size(width, width);
  }

  /// Returns valid size from media adjusted for storage in memory.
  static Size? getDecodedSize(
    double screenWidth, {
    Size? visibleSize,
    Size? originalSize,
  }) {
    int? decodeWidth;
    int? decodeHeight;

    double? visibleWidth = visibleSize?.width;
    double? visibleHeight = visibleSize?.height;

    if (originalSize != null) {
      try {
        final int deltaWidth = visibleWidth != null && visibleWidth != 0
            ? originalSize.width ~/ visibleWidth
            : 0;
        final int deltaHeight = visibleHeight != null && visibleHeight != 0
            ? originalSize.height ~/ visibleHeight
            : 0;

        final int delta = deltaWidth > deltaHeight ? deltaWidth : deltaHeight;

        final double? screenDelta = visibleWidth != null && visibleWidth != 0
            ? screenWidth / visibleWidth
            : null;
        final double? factor = screenDelta != null
            ? (1 / delta) *
                2.5 *
                (screenDelta > 1.1 ? 0.5 + (1 / screenDelta) : 1)
            : null;

        if ((delta > 2)) {
          if (factor != null) {
            decodeWidth = (originalSize.width * factor).toInt();
          }

          if (factor != null) {
            decodeHeight = (originalSize.height * factor).toInt();
          }
        } else {
          decodeWidth = originalSize.width.toInt();
          decodeHeight = originalSize.height.toInt();
        }
      } catch (e) {
        return ErrorService.onErrorSync(e, activity: 'getDecodedSize');
      }
    }

    if (decodeWidth != null && decodeHeight != null) {
      return Size(decodeWidth.toDouble(), decodeHeight.toDouble());
    }

    return null;
  }

  /// Returns bound for map based on max values.
  static Map<String, double> getMapBounds(
    double latitude,
    double longitude,
    int radiusInMeter,
  ) {
    try {
      return {
        'latMaxNorth': SphericalUtil.computeOffset(
                LatLng(latitude, longitude), radiusInMeter, 0)
            .latitude,
        'lonMaxEast': SphericalUtil.computeOffset(
                LatLng(latitude, longitude), radiusInMeter, 90)
            .longitude,
        'latMaxSouth': SphericalUtil.computeOffset(
                LatLng(latitude, longitude), radiusInMeter, 180)
            .latitude,
        'lonMaxWest': SphericalUtil.computeOffset(
                LatLng(latitude, longitude), radiusInMeter, 270)
            .longitude,
      };
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'getMapBounds');
    }
  }

  /// Adjusts height to width.
  static Size adjustHeightToWidth(Size size, double width) {
    if (!isSizeValid(size)) return size;

    if (size.height != 0) {
      try {
        final ratio = size.width / size.height;

        final double? height = ratio != 0 ? width / ratio : null;

        return height != null ? Size(width, height) : size;
      } catch (e) {
        return ErrorService.onErrorSync(e, activity: 'adjustHeightToWidth');
      }
    }

    return size;
  }

  /// Returns true if size is valid.
  static bool isSizeValid(Size? size) {
    if (size == null) return false;

    if (!isSizeValueValid(size.width)) return false;
    if (!isSizeValueValid(size.height)) return false;

    return true;
  }

  /// Returns true if size value is valid.
  static bool isSizeValueValid(double size) {
    return !size.isNaN && !size.isInfinite && !size.isNegative;
  }
}
