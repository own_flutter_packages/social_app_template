import 'dart:async';
import 'dart:io' as io;

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:queue/queue.dart';
import 'package:social_app_template/src/services/error_service.dart';
import 'package:social_app_template/src/widgets/core/media/video/cached/custom_video_cache_manager.dart';

/// Provides functionality for videos.
class VideoService {
  static final VideoService _singleton = VideoService._internal();

  factory VideoService() {
    return _singleton;
  }

  VideoService._internal();

  Queue? queue;

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<VideoService> init({Function(int)? before}) async {
    if (before != null) {
      before(-1);
    }

    queue ??= Queue(delay: const Duration(milliseconds: 100));
    await Future.delayed(const Duration(milliseconds: 0));

    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Return size of video by access of video player controller.
  static Size? getSizeOfVideo(
      CachedVideoPlayerController videoPlayerController) {
    try {
      final value = videoPlayerController.value;
      final videoWidth = value.size.width;
      final videoHeight = value.size.height;

      return Size(videoWidth, videoHeight);
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'getSizeOfVideo');
    }
  }

  /// Loads video from web that is cached.
  Future<io.File?> loadVideoFromWeb(String url, {Duration? timeout}) async {
    io.File? videoFile;

    videoFile = await _getVideo(url, timeout: timeout)
        .onError(ErrorService.onErrorWithTrace);

    return videoFile;
  }

  /// Loads video from file system.
  static io.File? loadVideoFromPath(String url) {
    try {
      return io.File(url);
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'loadVideoFromPath');
    }
  }

  Future<io.File?> _getVideo(String url, {Duration? timeout}) async {
    try {
      return timeout != null
          ? await CustomVideoCacheManager().getSingleFile(url).timeout(timeout)
          : await CustomVideoCacheManager().getSingleFile(url);
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'loadVideoFromPath');
    }
  }
}
