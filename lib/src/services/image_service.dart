import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/src/services/error_service.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/manager/custom_image_cache_manager.dart';

typedef MarkImageAsNotFound = Future<void> Function(String url);
typedef IsImageMarkedAsNotFound = bool Function(String url);

/// Provides functionality for images.
class ImageService {
  static final ImageService _singleton = ImageService._internal();

  factory ImageService() {
    return _singleton;
  }

  ImageService._internal();

  static Size? _originalImageSizeProfileImages;
  static MarkImageAsNotFound? _markImageAsNotFound;
  static IsImageMarkedAsNotFound? _isImageMarkedAsNotFound;

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<ImageService> init({
    Function(int)? before,
    Size? originalImageSizeProfileImages,
    MarkImageAsNotFound? markImageAsNotFound,
    IsImageMarkedAsNotFound? isImageMarkedAsNotFound,
  }) async {
    if (before != null) {
      before(-1);
    }

    _originalImageSizeProfileImages = originalImageSizeProfileImages;
    _markImageAsNotFound = markImageAsNotFound;
    _isImageMarkedAsNotFound = isImageMarkedAsNotFound;

    await Future.delayed(const Duration(milliseconds: 0));

    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Loads image from web that is cached.
  Future<File?> loadImageFromWeb(String url) async {
    final notFound = isImageMarkedAsNotFound(url);

    if (notFound) {
      return null;
    }

    final image = await _loadImage(url).onError(ErrorService.onErrorWithTrace);

    if (image?.existsSync() ?? false) {
      return image;
    } else {
      markImageAsNotFound(url);
      return null;
    }
  }

  /// Returns size of image by url.
  static Future<Size?> getSize(String url) async {
    bool notFound = isImageMarkedAsNotFound(url);

    if (notFound) return null;

    final File? image =
        await _loadImage(url).onError(ErrorService.onErrorWithTrace);

    if (image == null) {
      await markImageAsNotFound(url).onError(ErrorService.onErrorWithTrace);

      return null;
    }

    try {
      final readAsBytesSync = image.readAsBytesSync();
      final decodedImage = await decodeImageFromList(readAsBytesSync);

      return Size(
        decodedImage.width.toDouble(),
        decodedImage.height.toDouble(),
      );
    } catch (e) {
      return await ErrorService.onError(e, activity: 'decodeImageFromList');
    }
  }

  /// Marks image as not found.
  ///
  /// by provided markImageAsNotFound on init.
  static Future<void> markImageAsNotFound(String url) async {
    if (_markImageAsNotFound != null) {
      await _markImageAsNotFound!(url).onError(ErrorService.onErrorWithTrace);
    }
  }

  /// Returns true is image was not found before.
  ///
  /// by provided isImageMarkedAsNotFound on init.
  static bool isImageMarkedAsNotFound(String url) {
    return _isImageMarkedAsNotFound != null
        ? _isImageMarkedAsNotFound!(url)
        : false;
  }

  /// Converts image to lower bytes.
  static Future<Uint8List> convertToLowerBytes(
    Uint8List resizedMarkerImageBytes, {
    int? targetWidth,
  }) async {
    if (resizedMarkerImageBytes.isEmpty) return resizedMarkerImageBytes;

    await Future(() async {
      final ui.Codec markerImageCodec = await ui.instantiateImageCodec(
        resizedMarkerImageBytes,
        targetWidth: targetWidth ?? 300,
      );

      final ui.FrameInfo frameInfo = await markerImageCodec.getNextFrame();
      final ByteData? byteData = await frameInfo.image.toByteData(
        format: ui.ImageByteFormat.png,
      );

      if (byteData != null) {
        resizedMarkerImageBytes = byteData.buffer.asUint8List();
      }
    }).onError(ErrorService.onErrorWithTrace);

    return resizedMarkerImageBytes;
  }

  /// Converts image url to twitter sized url.
  static String? convertUrlToTwitterSize(int imageSize, String imageUrl) {
    if (imageUrl.isEmpty) return '';

    String url;

    try {
      switch (imageSize) {
        case 0: // medium
          url = imageUrl.replaceAll('normal', 'bigger');
          break;
        case 1: // mini
          url = imageUrl.replaceAll('normal', 'mini');
          break;
        case 2: // large
          url = imageUrl.replaceAll('_normal', '');
          break;
        default:
          url = imageUrl.replaceAll('_normal', '');
      }
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'convertUrlToTwitterSize');
    }

    return url;
  }

  static Future<File?> _loadImage(String url) async {
    try {
      return await CustomImageCacheManager().getSingleFile(url);
    } catch (e) {
      markImageAsNotFound(url);
      return await ErrorService.onError(e, activity: '_loadCachedImage');
    }
  }

  /// Original image size for profile image.
  ///
  /// if provided loading of profile images is improved
  static Size? get originalImageSizeProfileImages =>
      _originalImageSizeProfileImages;
}
