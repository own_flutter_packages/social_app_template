import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/focus/focus.dart'
    as social_focus;
import 'package:social_app_template/src/services/error_service.dart';
import 'package:social_app_template/src/services/video_service.dart';

class PostsListWithExamples extends StatefulWidget {
  const PostsListWithExamples({
    Key? key,
  }) : super(key: key);

  @override
  PostsListWithExamplesState createState() => PostsListWithExamplesState();
}

class PostsListWithExamplesState extends State<PostsListWithExamples> {
  late Future<List<String>> future;

  Future<List<String>> _getData() async {
    await _createExamples().onError(ErrorService.onErrorWithTrace);

    final searchResult = await PlatformPostRepository.getByIds(
      _example.map((e) => e.id).toList(),
    ).onError(ErrorService.onErrorWithTraceIterable);

    return searchResult.where((e) => e != null).map((e) => e!.id).toList();
  }

  @override
  void initState() {
    final videoService = VideoService();
    videoService.init();

    future = _getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        return snapshot.connectionState == ConnectionState.waiting
            ? Container()
            : SocialApp.postListWithProgress(
                ids: snapshot.data,
                onRefresh: () async => _example.map((e) => e.id).toList(),
                colorRefreshIndicator: Colors.white,
              );
      },
    );
  }

  static Future<void> _createExamples() async {
    await IsarDB.isar
        .writeTxn(() => IsarDB.isar.clear())
        .onError(ErrorService.onErrorWithTrace);

    final location = LocationQuery.of(
      id: 0,
      query: 'Hamburg',
      latitude: 9.50,
      longitude: 10.0,
      address: MapBoxPlace.of(
        id: 'place_1',
        text: 'Neue Flora',
        placeName: 'Altona, Hamburg',
        context: [
          MapBoxPlaceContext(
            id: 'country_place_1',
            mapBoxPlaceId: 'place_1',
            text: 'Germany',
          ),
          MapBoxPlaceContext(
            id: 'city_place_1',
            mapBoxPlaceId: 'place_1',
            text: 'Hamburg',
          ),
        ],
      ),
    );

    final locationMentioned = LocationQuery.of(
      id: 1,
      query: 'Berlin',
      latitude: 8.50,
      longitude: 10.0,
      address: MapBoxPlace.of(
        id: 'place_2',
        text: 'Lindenallee',
        placeName: 'Zentrum, Berlin',
        context: [
          MapBoxPlaceContext(
            id: 'country_place_2',
            mapBoxPlaceId: 'place_2',
            text: 'Germany',
          ),
          MapBoxPlaceContext(
            id: 'city_place_2',
            mapBoxPlaceId: 'place_2',
            text: 'Berlin',
          ),
        ],
      ),
    );

    final image1 = PlatformMedia(
      id: '0',
      url:
          'https://storage.googleapis.com/cms-storage-bucket/6f183a9db312d0e1b535.png',
      isVideo: false,
    );

    final image2 = PlatformMedia(
      id: '1',
      url:
          'https://storage.googleapis.com/cms-storage-bucket/6165afcf2c3c6d5580d9.png',
      isVideo: false,
    );

    final authorAge = social_focus.Focus.of(
      name: 'age',
      value: '32',
      isGroup: false,
    );
    final authorAgeMetrics = FocusMetrics(
      id: authorAge.id,
      focusId: authorAge.id,
      count: 1,
      lastUpdated: DateTime.now(),
    );
    authorAge.metrics.value = authorAgeMetrics;

    final authorFavColor = social_focus.Focus.of(
      name: 'color',
      value: 'green',
      isGroup: false,
    );
    final authorFavColorMetrics = FocusMetrics(
      id: authorFavColor.id,
      focusId: authorFavColor.id,
      count: 1,
      lastUpdated: DateTime.now(),
    );
    authorFavColor.metrics.value = authorFavColorMetrics;

    final mentionedAge = social_focus.Focus.of(
      name: 'age',
      value: '29',
      isGroup: false,
    );
    final mentionedAgeMetrics = FocusMetrics(
      id: mentionedAge.id,
      focusId: mentionedAge.id,
      count: 1,
      lastUpdated: DateTime.now(),
    );
    mentionedAge.metrics.value = mentionedAgeMetrics;

    final mentionedFavColor = social_focus.Focus.of(
      name: 'color',
      value: 'blue',
      isGroup: false,
    );
    final mentionedFavColorMetrics = FocusMetrics(
      id: mentionedFavColor.id,
      focusId: mentionedFavColor.id,
      count: 1,
      lastUpdated: DateTime.now(),
    );
    mentionedFavColor.metrics.value = mentionedFavColorMetrics;

    final author = PlatformUser.of(
      id: 'moin',
      name: 'Ilja Lichtenberg',
      location: location,
      profileImageUrl:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJYm3L7KZT8z0D-WEFI25afCOjIfzYOa6jwg&usqp=CAU',
      focuses: [authorAge, authorFavColor],
    );

    final userMentioned1 = PlatformUser.of(
      id: 'moin_1',
      name: 'Test the app',
      location: locationMentioned,
      profileImageUrl:
          'https://storage.googleapis.com/cms-storage-bucket/6165afcf2c3c6d5580d9.png',
      focuses: [mentionedAge, mentionedFavColor],
    );

    final userMentioned2 = PlatformUser.of(
      id: 'moin_2',
      name: 'Load',
      location: locationMentioned,
      profileImageUrl:
          'https://storage.googleapis.com/cms-storage-bucket/6165afcf2c3c6d5580d9.png',
      focuses: [mentionedAge, mentionedFavColor],
    );

    await Future.forEach(_example, (PlatformPost examplePost) async {
      final index = _example.indexOf(examplePost);

      examplePost.entities.value = (index % 2 == 0)
          ? PlatformPostEntities.of(
              postId: examplePost.id,
              media: [image1, image2],
              userMentions: [userMentioned1],
            )
          : PlatformPostEntities.of(
              postId: examplePost.id,
              media: [image1],
              userMentions: [userMentioned1, userMentioned2],
            );

      examplePost.user.value = author;
    });

    await PlatformPostRepository.createAll(_example)
        .onError(ErrorService.onErrorWithTrace);
  }

  static final List<PlatformPost> _example = List.generate(
    10,
    (index) => PlatformPost.of(id: '$index', text: 'hi_$index'),
  );
}
