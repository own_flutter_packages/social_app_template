import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/impl/examples/posts_list_with_examples.dart';
import 'package:social_app_template/src/services/image_service.dart';
import 'package:social_app_template/src/services/video_service.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';

/// Not part of public API
class SocialAppTemplateImpl implements SocialAppTemplateInterface {
  /// Not part of public API
  SocialAppTemplateImpl();

  @override
  PlatformPostList postList({
    Key? key,
    List<String>? ids,
    bool shouldReloadProfileImage = false,
    Color? colorRefreshIndicator,
    Size? originalImageSizeProfileImages,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
    OnRefresh? onRefresh,
    OnSwipeLeft? onSwipeLeft,
    OnInitPost? onInitPost,
    OnTapPost? onTapPost,
    OnReloadProfileImage? onReloadProfileImage,
    OnChangedContentQuality? onChangedContentQuality,
    IsImageMarkedAsNotFound? isImageMarkedAsNotFound,
    MarkImageAsNotFound? markImageAsNotFound,
    List<CollectionSchema>? furtherSchema,
  }) {
    if (furtherSchema != null) {
      IsarDB().init(furtherSchema: furtherSchema);
    }

    final videoService = VideoService();
    videoService.init();

    final imageService = ImageService();
    imageService.init(
      originalImageSizeProfileImages: originalImageSizeProfileImages,
      isImageMarkedAsNotFound: isImageMarkedAsNotFound,
      markImageAsNotFound: markImageAsNotFound,
    );

    return PlatformPostList(
      key: key,
      ids: ids ?? [],
      shouldReloadProfileImage: shouldReloadProfileImage,
      colorRefreshIndicator: Colors.white,
      noMoreItemsIndicatorBuilder: noMoreItemsIndicatorBuilder,
      noFoundItemsIndicatorBuilder: noFoundItemsIndicatorBuilder,
      onInitPost: onInitPost,
      onTapPost: onTapPost,
      onRefresh: onRefresh,
      onSwipeLeft: onSwipeLeft,
      onReloadProfileImage: onReloadProfileImage,
      onChangedContentQuality: onChangedContentQuality,
    );
  }

  @override
  PlatformPostListWithProgress postListWithProgress({
    Key? key,
    List<String>? ids,
    bool shouldReloadProfileImage = false,
    Color? colorRefreshIndicator,
    Alignment? progressAlignment,
    Color? progressMainColor,
    Color? progressBackgroundColor,
    Size? originalImageSizeProfileImages,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
    OnInitPost? onInitPost,
    OnTapPost? onTapPost,
    OnRefresh? onRefresh,
    OnSwipeLeft? onSwipeLeft,
    OnReloadProfileImage? onReloadProfileImage,
    OnChangedContentQuality? onChangedContentQuality,
    IsImageMarkedAsNotFound? isImageMarkedAsNotFound,
    MarkImageAsNotFound? markImageAsNotFound,
    List<CollectionSchema>? furtherSchema,
  }) {
    if (furtherSchema != null) {
      IsarDB().init(furtherSchema: furtherSchema);
    }

    final videoService = VideoService();
    videoService.init();

    final imageService = ImageService();
    imageService.init(
      originalImageSizeProfileImages: originalImageSizeProfileImages,
      isImageMarkedAsNotFound: isImageMarkedAsNotFound,
      markImageAsNotFound: markImageAsNotFound,
    );

    return PlatformPostListWithProgress(
      key: key,
      ids: ids ?? [],
      shouldReloadProfileImage: shouldReloadProfileImage,
      colorRefreshIndicator: Colors.white,
      progressAlignment: progressAlignment,
      progressMainColor: progressMainColor,
      progressBackgroundColor: progressBackgroundColor,
      noMoreItemsIndicatorBuilder: noMoreItemsIndicatorBuilder,
      noFoundItemsIndicatorBuilder: noFoundItemsIndicatorBuilder,
      onInitPost: onInitPost,
      onTapPost: onTapPost,
      onRefresh: onRefresh,
      onSwipeLeft: onSwipeLeft,
      onReloadProfileImage: onReloadProfileImage,
      onChangedContentQuality: onChangedContentQuality,
    );
  }

  @override
  PostsListWithExamples templateWithExample() {
    return const PostsListWithExamples();
  }
}
