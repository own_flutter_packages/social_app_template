import 'package:flutter/material.dart';
import 'package:social_app_template/src/widgets/core/blur/blur_background.dart';

class Circle extends StatelessWidget {
  const Circle({
    Key? key,
    this.color,
    this.blurColor,
    this.child,
    this.size,
    this.border,
    this.withBlur = false,
    this.noBlurForBorder = false,
  }) : super(key: key);

  final Color? color;
  final Color? blurColor;
  final Widget? child;
  final double? size;
  final Border? border;
  final bool withBlur;
  final bool noBlurForBorder;

  @override
  Widget build(BuildContext context) {
    final colorAdjusted = withBlur
        ? blurColor ?? (Colors.black45).withOpacity(0.5)
        : color ?? Colors.transparent;

    final Widget circle = ClipOval(
      child: Container(
        height: size,
        width: size,
        decoration: withBlur
            ? noBlurForBorder
                ? BoxDecoration(
                    color: colorAdjusted,
                    shape: BoxShape.circle,
                  )
                : BoxDecoration(
                    color: colorAdjusted,
                    border: border ??
                        Border.all(
                          color: color ?? Colors.black,
                          width: 0.5,
                        ),
                    shape: BoxShape.circle,
                  )
            : border != null
                ? BoxDecoration(
                    color: colorAdjusted,
                    border: border,
                    shape: BoxShape.circle,
                  )
                : ShapeDecoration(
                    color: colorAdjusted,
                    shape: const CircleBorder(),
                  ),
        child: child == null
            ? Container()
            : withBlur
                ? BlurBackground(sigmaX: 1.9, sigmaY: 1.9, child: child!)
                : child,
      ),
    );

    return noBlurForBorder && withBlur && border != null
        ? Container(
            decoration: BoxDecoration(
              border: border,
              shape: BoxShape.circle,
            ),
            child: circle,
          )
        : circle;
  }
}
