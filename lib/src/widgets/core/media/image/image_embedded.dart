import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:social_app_template/src/repo/isar/model/image/blob_image.dart';
import 'package:social_app_template/src/services/size_service.dart';
import 'package:social_app_template/src/widgets/core/circle/circle.dart';

class ImageEmbedded extends StatelessWidget {
  const ImageEmbedded({
    Key? key,
    required this.imageUrl,
    this.borderRounding = 25.0,
    this.withBorder = false,
    this.circle = false,
    this.rounded = false,
    this.sizeInDouble,
    this.size,
    this.decodedSize,
    this.fit,
    this.backgroundColor,
    this.borderColor,
    this.borderWidth,
    this.borderRadius,
  }) : super(key: key);

  final dynamic imageUrl;
  final double borderRounding;
  final bool withBorder;
  final bool circle;
  final bool rounded;
  final double? sizeInDouble;
  final Size? size;
  final Size? decodedSize;
  final BoxFit? fit;
  final Color? backgroundColor;
  final Color? borderColor;
  final double? borderWidth;
  final BorderRadius? borderRadius;

  static empty(
    double size, {
    Color? color,
  }) {
    return Container(
      color: color,
      height: size,
      width: size,
    );
  }

  static emptyWithIcon(
    double size, {
    Color? color,
    Color? iconColor,
    IconData? emptyIcon,
  }) {
    return Circle(
      color: color,
      size: size,
      child: Center(
        child: Icon(
          emptyIcon ?? MdiIcons.account,
          color: iconColor,
          size: size * 0.75,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size?.width ?? sizeInDouble,
      height: size?.height ?? sizeInDouble,
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
        color: backgroundColor ?? Colors.transparent,
        shape: circle ? BoxShape.circle : BoxShape.rectangle,
        borderRadius: rounded && !circle
            ? borderRadius ?? BorderRadius.all(Radius.circular(borderRounding))
            : null,
        border: withBorder
            ? Border.all(
                width: borderWidth ?? 1,
                color: borderColor ?? Colors.black,
              )
            : null,
        image: imageUrl != null
            ? DecorationImage(
                fit: fit ?? BoxFit.fill,
                image: imageUrl is BlobImage
                    ? MemoryImage(imageUrl.byteData)
                    : imageUrl is Uint8List
                        ? MemoryImage(imageUrl)
                        : imageUrl is CachedNetworkImageProvider
                            ? SizeService.isSizeValid(decodedSize)
                                ? ResizeImage(
                                    imageUrl,
                                    width: decodedSize!.width.toInt(),
                                    height: decodedSize!.height.toInt(),
                                  )
                                : imageUrl
                            : NetworkImage(imageUrl),
              )
            : null,
      ),
    );
  }
}
