import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';

typedef OnFoundOriginalSize = Function(Size originalSize);

class CachedImageAlwaysWithSize extends StatefulWidget {
  const CachedImageAlwaysWithSize({
    Key? key,
    this.imageUrl,
    this.originalSize,
    this.size,
    this.backgroundColor,
    this.circle = false,
    this.fit = BoxFit.contain,
    this.errorWidget,
    this.onFoundOriginalSize,
  }) : super(key: key);

  final String? imageUrl;
  final Size? originalSize;
  final Size? size;
  final Color? backgroundColor;
  final bool? circle;
  final BoxFit? fit;
  final Widget? errorWidget;
  final OnFoundOriginalSize? onFoundOriginalSize;

  @override
  CachedImageAlwaysWithSizeState createState() =>
      CachedImageAlwaysWithSizeState();
}

class CachedImageAlwaysWithSizeState extends State<CachedImageAlwaysWithSize> {
  static const int fadeDurationInMs = 200;

  late Future<Size?> future;

  String? imageUrl;
  Size? originalSize;
  Size? size;
  bool notFound = false;
  bool checkedIfExists = false;
  bool initializedSize = false;

  Future<Size?> _getData() async {
    if (imageUrl?.isEmpty ?? true) return null;
    if (checkedIfExists && initializedSize) return size;

    notFound = ImageService.isImageMarkedAsNotFound(imageUrl!);

    if (notFound) return _onImageNotFound();

    if (originalSize == null) {
      await _onCalcOriginalSize();

      _onFoundOriginalSize(originalSize);
    } else {
      await _onCheckIfStillExistsInWeb();
    }

    _onSetSize();

    return originalSize;
  }

  @override
  void initState() {
    imageUrl = widget.imageUrl;
    originalSize = widget.originalSize;
    size = widget.size;

    future = _getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        final futureDone = snapshot.connectionState == ConnectionState.done;
        final isAvailable = !notFound && (imageUrl?.isNotEmpty ?? false);

        if (futureDone && !isAvailable) {
          return widget.errorWidget ??
              ImageEmbedded.empty(
                _size()?.height ?? 0,
                color: widget.backgroundColor ?? Colors.black,
              );
        }

        if (!futureDone) {
          return SizedBox(
            width: _size()?.width,
            height: _size()?.height,
            child: Container(),
          );
        }

        return CachedImage(
          imageUrl: imageUrl,
          fit: widget.fit,
          circle: widget.circle,
          errorWidget: widget.errorWidget,
          size: _size(),
          decodedSize: _getDecodedSize(),
        );
      },
    );
  }

  _onFoundOriginalSize(Size? size) {
    if (size != null && widget.onFoundOriginalSize != null) {
      widget.onFoundOriginalSize!(originalSize!);
    }
  }

  Future<void> _onCalcOriginalSize() async {
    if (imageUrl == null) return;

    final size = await ImageService.getSize(
      imageUrl!,
    ).onError(_onGetSizeError);

    if (size != null) {
      originalSize = size;
    }

    if (originalSize != null) {
      _onImageFound();
    } else {
      _onImageNotFound();
    }
  }

  _onSetSize() {
    final availableWidth = widget.size?.width;
    final isQuadratic = widget.circle ?? false;

    if (availableWidth == null) {
      return;
    }

    if (isQuadratic) {
      size = Size(availableWidth, availableWidth);
    }

    if (originalSize != null) {
      size = SizeService.adjustHeightToWidth(originalSize!, availableWidth);
    }

    initializedSize = true;
  }

  Future<void> _onCheckIfStillExistsInWeb() async {
    if (!checkedIfExists) {
      notFound = (await ImageService().loadImageFromWeb(imageUrl!)) == null;
      checkedIfExists = true;

      if (notFound) {
        _onImageNotFound();
      } else {
        _onImageFound();
      }
    }
  }

  _onImageFound() {
    notFound = false;
  }

  _onImageNotFound() {
    notFound = true;

    if (imageUrl == null) return;

    if (!ImageService.isImageMarkedAsNotFound(imageUrl!)) {
      ImageService.markImageAsNotFound(imageUrl!);
    }

    return null;
  }

  Size? _getDecodedSize() {
    final screenSize = MediaQuery.of(context).size;

    return SizeService.getDecodedSize(
      screenSize.width,
      visibleSize: _size(),
      originalSize: originalSize,
    );
  }

  Size? _size() {
    return size != null && SizeService.isSizeValid(size!) ? size : null;
  }

  Future<Size?> _onGetSizeError(error, stacktrace) async {
    if (kDebugMode) {
      print(
        '[ERROR] when getting size of image (will reload url -> | Error: ${error.toString()}',
      );
    }

    return null;
  }
}
