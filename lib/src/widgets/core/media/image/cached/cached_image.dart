import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/manager/custom_image_cache_manager.dart';

class CachedImage extends StatelessWidget {
  const CachedImage({
    Key? key,
    this.imageUrl,
    this.decodedSize,
    this.originalSize,
    this.size,
    this.errorWidget,
    this.circle = false,
    this.fit = BoxFit.contain,
  }) : super(key: key);

  final String? imageUrl;
  final Size? decodedSize;
  final Size? originalSize;
  final Size? size;
  final Widget? errorWidget;
  final BoxFit? fit;
  final bool? circle;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return CachedNetworkImage(
      imageUrl: imageUrl!,
      cacheManager: CustomImageCacheManager(),
      memCacheWidth: _decodedSize(screenSize)?.width.toInt(),
      memCacheHeight: _decodedSize(screenSize)?.height.toInt(),
      maxWidthDiskCache: originalSize?.width.toInt(),
      maxHeightDiskCache: originalSize?.height.toInt(),
      fit: _boxFit(),
      fadeInCurve: Curves.easeIn,
      fadeOutCurve: Curves.easeOut,
      fadeInDuration: const Duration(milliseconds: 50),
      fadeOutDuration: const Duration(milliseconds: 50),
      imageBuilder: (context, ImageProvider? image) {
        if (image == null) return _onImageNotFound();

        return ImageEmbedded(
          imageUrl: image,
          size: _size() != null ? Size(_width()!, _height()!) : null,
          decodedSize: _decodedSize(screenSize),
          circle: circle ?? false,
          fit: _boxFit(),
        );
      },
      errorWidget: (_, __, ___) => _onImageNotFound(),
      progressIndicatorBuilder: (_, __, ___) => Container(),
    );
  }

  Widget _onImageNotFound() {
    if (imageUrl == null) return errorWidget ?? Container();

    if (!ImageService.isImageMarkedAsNotFound(imageUrl!)) {
      ImageService.markImageAsNotFound(imageUrl!);
    }

    return errorWidget ?? Container();
  }

  Size? _size() {
    return SizeService.isSizeValid(size) ? size : null;
  }

  double? _width() {
    return _size()?.width;
  }

  double? _height() {
    return (circle ?? false) ? _size()?.width : _size()?.height;
  }

  Size? _decodedSize(Size screenSize) {
    return decodedSize ??
        SizeService.getDecodedSize(
          screenSize.width,
          visibleSize: _size() != null ? Size(_width()!, _height()!) : null,
          originalSize: originalSize,
        );
  }

  BoxFit _boxFit() => fit ?? BoxFit.fill;
}
