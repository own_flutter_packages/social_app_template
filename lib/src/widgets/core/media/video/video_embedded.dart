import 'dart:io';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/src/services/error_service.dart';
import 'package:social_app_template/src/services/video_service.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/sized/cached_image_sized.dart';

class VideoEmbedded extends StatefulWidget {
  const VideoEmbedded({
    Key? key,
    required this.url,
    this.fromFile = false,
    this.videoFile,
    this.onFoundOriginalSize,
  }) : super(key: key);

  final String url;
  final bool? fromFile;
  final File? videoFile;
  final OnFoundOriginalSize? onFoundOriginalSize;

  @override
  VideoEmbeddedState createState() => VideoEmbeddedState();
}

class VideoEmbeddedState extends State<VideoEmbedded> {
  late Future<void> future;
  late String url;

  bool playable = true;

  CachedVideoPlayerController? videoPlayerController;
  File? videoFile;
  int? secondsWhenDisposed;

  onReload(String url) {
    this.url = url;
    videoFile = null;

    future = _getData();

    if (mounted) {
      setState(() {});
    }
  }

  Future<void> _getData() async {
    videoFile = await _onLoadVideoFile();

    if (videoFile != null) {
      videoPlayerController = CachedVideoPlayerController.file(
        videoFile!,
        videoPlayerOptions: VideoPlayerOptions(
          mixWithOthers: true,
        ),
      );
    }

    if (videoPlayerController != null) {
      await _onInitializeVideoController();
    }

    await onSetVolume();
    await _onChangeVideoPlayability(playable: playable);
  }

  @override
  void initState() {
    videoFile = widget.videoFile;
    url = widget.url;

    future = _getData();

    super.initState();
  }

  @override
  void dispose() {
    onDisposeVideoController();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return videoFile == null && url.isEmpty
        ? Container()
        : FutureBuilder(
            future: future,
            builder: (context, snapshot) {
              final done = snapshot.connectionState == ConnectionState.done;

              return done && _isVideoPlayerInitialized(withChewie: true)
                  ? CachedVideoPlayer(videoPlayerController!)
                  : Container();
            },
          );
  }

  Future<void> onStart() async {
    if (videoPlayerController?.value == null) return;

    await videoPlayerController!.play();

    if (kDebugMode) {
      print('[VIDEO_PLAYER] - started url: $url');
    }
  }

  Future<void> onPause() async {
    if (videoPlayerController?.value == null) return;

    secondsWhenDisposed = videoPlayerController!.value.position.inSeconds;
    await videoPlayerController!.pause();

    if (kDebugMode) {
      print('[VIDEO_PLAYER] - paused at $secondsWhenDisposed url: $url');
    }
  }

  Future<void> onContinueFrom({int? from}) async {
    if (videoPlayerController?.value == null) return;

    final continueFrom = from ?? secondsWhenDisposed;

    if (continueFrom != null) {
      await videoPlayerController!.seekTo(
        Duration(seconds: continueFrom),
      );

      await videoPlayerController!.play();

      if (kDebugMode) {
        print('[VIDEO_PLAYER] - continued from $continueFrom url: $url');
      }
    }
  }

  Future<void> onSetVolume({double volume = 0.0}) async {
    await videoPlayerController?.setVolume(volume);
  }

  Future<void> onReloadPlayability({bool playable = false}) async {
    if (playable != this.playable) {
      this.playable = playable;

      await _onChangeVideoPlayability(playable: playable);
    }
  }

  onDisposeVideoController() {
    if (videoPlayerController == null) return;

    videoFile = null;

    try {
      videoPlayerController!.dispose();
      videoPlayerController = null;

      if (kDebugMode) {
        print('[VIDEO_PLAYER] disposed - $url');
      }
    } catch (e) {
      if (kDebugMode) {
        print('[ERROR] - when disposing video controller | $e');
      }
    }

    secondsWhenDisposed = videoPlayerController?.value.position.inSeconds;
  }

  Future<File?> _onLoadVideoFile() async {
    final isFromFile = widget.fromFile ?? false;

    if (isFromFile && videoFile != null) return videoFile;

    if (url.isEmpty) return videoFile;

    if (url.isNotEmpty) {
      if (widget.fromFile ?? false) {
        videoFile = VideoService.loadVideoFromPath(url);
      } else {
        videoFile = await VideoService().loadVideoFromWeb(url);
      }
    } else {
      if (kDebugMode) {
        print('[ERROR] - on load video file. No url! From path: $isFromFile.');
      }
    }

    if (videoFile == null && widget.onFoundOriginalSize != null) {
      widget.onFoundOriginalSize!(const Size(0, 0));
    }

    return videoFile;
  }

  Future<void> _onInitializeVideoController() async {
    if (!_isVideoPlayerInitialized()) {
      try {
        videoPlayerController!.setLooping(true);
        await videoPlayerController!.initialize().onError(_onErrorDuringInit);

        _onFindOriginalSize();
      } catch (error) {
        if (kDebugMode) {
          print('[ERROR] - on init video. Error: $error.');
        }
      }
    }
  }

  void _onFindOriginalSize() {
    if (widget.onFoundOriginalSize != null) {
      final size = VideoService.getSizeOfVideo(videoPlayerController!);

      if (size != null) {
        widget.onFoundOriginalSize!(size);
      } else {
        if (kDebugMode) {
          print('[ERROR] - size was null when getting from video. $url');
        }
      }
    }
  }

  Future<void> _onChangeVideoPlayability({bool playable = false}) async {
    if (videoPlayerController?.value == null) return;

    final isPlaying = videoPlayerController!.value.isPlaying;

    if (playable && !isPlaying) {
      if (secondsWhenDisposed != null) {
        await onContinueFrom();
      } else {
        await onStart();
      }
    } else if (!playable && isPlaying) {
      await onPause();
    }
  }

  _onErrorDuringInit(Error e, StackTrace stackTrace) {
    if (kDebugMode) {
      ErrorService.onErrorSync(e, activity: 'initVideoController');
    }

    return null;
  }

  bool _isVideoPlayerInitialized({bool withChewie = false}) {
    final exists = videoPlayerController != null;
    final hasValue = videoPlayerController?.value != null;
    final isInitialized = videoPlayerController?.value.isInitialized ?? false;

    return exists && hasValue && isInitialized;
  }
}
