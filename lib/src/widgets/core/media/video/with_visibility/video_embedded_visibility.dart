import 'dart:io';

import 'package:flutter/material.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/sized/cached_image_sized.dart';
import 'package:social_app_template/src/widgets/core/media/video/video_embedded.dart';
import 'package:visibility_detector/visibility_detector.dart';

class VideoEmbeddedWithVisibility extends StatefulWidget {
  const VideoEmbeddedWithVisibility({
    Key? key,
    required this.url,
    this.fromFile = false,
    this.videoFile,
    this.onFoundOriginalSize,
  }) : super(key: key);

  final String url;
  final bool? fromFile;
  final File? videoFile;
  final OnFoundOriginalSize? onFoundOriginalSize;

  @override
  VideoEmbeddedWithVisibilityState createState() =>
      VideoEmbeddedWithVisibilityState();
}

class VideoEmbeddedWithVisibilityState<T extends PlatformPost>
    extends State<VideoEmbeddedWithVisibility> {
  final _keyVideo = GlobalKey<VideoEmbeddedState>();

  late Key keyVisibility;

  onReload(String url) {
    final videoEmbedded = _keyVideo.currentState;

    if (videoEmbedded != null) {
      videoEmbedded.onReload(url);
    }
  }

  @override
  void initState() {
    keyVisibility = Key(
      'video_visibility_${widget.url}_${widget.hashCode}',
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      key: keyVisibility,
      onVisibilityChanged: _onVisibilityChanged,
      child: VideoEmbedded(
        key: _keyVideo,
        url: widget.url,
        videoFile: widget.videoFile,
        fromFile: widget.fromFile,
        onFoundOriginalSize: widget.onFoundOriginalSize,
      ),
    );
  }

  _onVisibilityChanged(VisibilityInfo info) {
    final visibleFraction = info.size.height > 0 ? info.visibleFraction : 0;
    final shouldPlay = visibleFraction > 0.4;

    if (_keyVideo.currentState != null) {
      _keyVideo.currentState!.onReloadPlayability(playable: shouldPlay);
    }
  }
}
