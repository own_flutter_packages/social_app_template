import 'dart:io';

class InfiniteListItem<T> {
  InfiniteListItem({
    required this.item,
    this.videoFile,
  });

  final T item;
  final File? videoFile;
}
