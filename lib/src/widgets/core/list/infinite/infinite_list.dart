import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

typedef InfiniteListItemBuilder<S> = Widget Function(
  BuildContext context,
  S item,
  int index,
);

typedef GetData<T> = Future<List<T>> Function(
  int startIndex,
  int nextItemsCount,
);

class InfiniteList<T> extends StatefulWidget {
  const InfiniteList({
    Key? key,
    required this.getData,
    required this.itemsBuilder,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
    this.itemsCount,
    this.itemsExtent,
    this.pageSize = 7,
  }) : super(key: key);

  final GetData<T> getData;
  final InfiniteListItemBuilder<T> itemsBuilder;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;
  final int? itemsCount;
  final double? itemsExtent;
  final int pageSize;

  @override
  InfiniteListState<T> createState() => InfiniteListState<T>();
}

class InfiniteListState<T> extends State<InfiniteList<T>> {
  final PagingController<int, T> _pagingController = PagingController(
    firstPageKey: 0,
    invisibleItemsThreshold: 1,
  );

  int? itemsCount;
  int? pageIndex;

  onReload({int? itemsCount}) {
    this.itemsCount = itemsCount;
    _pagingController.refresh();
  }

  Future<void> _fetchPage(int pageKey) async {
    pageIndex = pageKey ~/ widget.pageSize;

    try {
      final items = await widget
          .getData(pageKey, widget.pageSize)
          .onError(_onErrorGetData);

      final isLastPage = itemsCount != null
          ? pageKey >= (itemsCount! - widget.pageSize)
          : items.length < widget.pageSize;

      if (isLastPage) {
        _pagingController.appendLastPage(items);
      } else {
        final int nextPageKey = pageKey + items.length;
        _pagingController.appendPage(items, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    itemsCount = widget.itemsCount;
    _pagingController.addPageRequestListener(_fetchPage);

    super.initState();
  }

  @override
  void dispose() {
    _pagingController.removePageRequestListener(_fetchPage);
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, T>(
        pagingController: _pagingController,
        physics: const BouncingScrollPhysics(),
        cacheExtent: _getCacheExtent(),
        addAutomaticKeepAlives: false,
        builderDelegate: PagedChildBuilderDelegate<T>(
          itemBuilder: widget.itemsBuilder,
          noMoreItemsIndicatorBuilder: widget.noMoreItemsIndicatorBuilder,
          noItemsFoundIndicatorBuilder: widget.noFoundItemsIndicatorBuilder,
          firstPageProgressIndicatorBuilder: (_) => Container(),
          newPageProgressIndicatorBuilder: (_) => Container(),
        ),
      );

  double _getCacheExtent() => widget.itemsExtent != null
      ? (widget.itemsExtent! * widget.pageSize)
      : 2000;

  FutureOr<List<T>> _onErrorGetData(error, stackTrace) {
    if (kDebugMode) {
      print('ERROR - when receiving data. Error: $error');
    }

    return [];
  }
}
