import 'dart:async';

import 'package:flutter/material.dart';
import 'package:social_app_template/src/widgets/core/indicator/progress/components/progress_indicator_bar.dart';

class ProgressIndicator extends StatefulWidget {
  const ProgressIndicator({
    Key? key,
    this.progress,
    this.backgroundColor,
    this.progressColor,
    this.descriptionColor,
    this.heightBar,
    this.withDescription = false,
  }) : super(key: key);

  final double? progress;
  final Color? backgroundColor;
  final Color? progressColor;
  final Color? descriptionColor;
  final double? heightBar;
  final bool withDescription;

  @override
  ProgressIndicatorState createState() => ProgressIndicatorState();
}

class ProgressIndicatorState extends State<ProgressIndicator> {
  late StreamController<double> loadSC;
  late StreamSink loadSink;
  late Stream<double> loadStream;
  late double progress;

  onReload(double progress) {
    this.progress = progress;

    loadSink.add(this.progress);
  }

  @override
  void initState() {
    loadSC = StreamController<double>();
    loadSink = loadSC.sink;
    loadStream = loadSC.stream;

    progress = widget.progress ?? 0.0;

    loadSink.add(progress);

    super.initState();
  }

  @override
  void dispose() {
    loadSC.close();
    loadSink.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: loadStream,
      builder: (context, snapshot) {
        return ProgressIndicatorBar(
          progress: progress,
          backgroundColor: widget.backgroundColor,
          progressColor: widget.progressColor,
          descriptionColor: widget.descriptionColor,
          heightBar: widget.heightBar,
          withDescription: widget.withDescription,
        );
      },
    );
  }
}
