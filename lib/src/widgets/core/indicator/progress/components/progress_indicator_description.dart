import 'package:flutter/material.dart';

class ProgressIndicatorDescription extends StatelessWidget {
  const ProgressIndicatorDescription({
    Key? key,
    required this.progress,
    this.descriptionColor,
  }) : super(key: key);

  final double progress;
  final Color? descriptionColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      height: 15,
      child: Center(
        child: Text(
          '${(progress * 100).toStringAsPrecision(3)}%',
          style: TextStyle(
            fontSize: 13,
            color: descriptionColor ?? Colors.black,
          ),
        ),
      ),
    );
  }
}
