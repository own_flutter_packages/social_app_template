import 'package:flutter/material.dart';
import 'package:social_app_template/src/widgets/core/indicator/progress/components/progress_indicator_description.dart';

class ProgressIndicatorBar extends StatelessWidget {
  const ProgressIndicatorBar({
    Key? key,
    required this.progress,
    this.backgroundColor,
    this.progressColor,
    this.descriptionColor,
    this.heightBar,
    this.withDescription = false,
  }) : super(key: key);

  final double progress;
  final Color? backgroundColor;
  final Color? progressColor;
  final Color? descriptionColor;
  final double? heightBar;

  final bool withDescription;

  @override
  Widget build(BuildContext context) {
    final double height =
        (heightBar ?? 1) + (withDescription ? 10.0 + 15.0 : 0);

    return Visibility(
      visible: _isVisible(),
      child: SizedBox(
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: heightBar ?? 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: LinearProgressIndicator(
                  backgroundColor: backgroundColor ?? Colors.black,
                  semanticsLabel: 'semantic',
                  semanticsValue: 'value',
                  value: progress,
                  valueColor: AlwaysStoppedAnimation<Color>(
                    progressColor ?? Colors.white,
                  ),
                ),
              ),
            ),
            withDescription
                ? ProgressIndicatorDescription(
                    progress: progress,
                    descriptionColor: descriptionColor,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  bool _isVisible() => progress != 0 && progress != 1;
}
