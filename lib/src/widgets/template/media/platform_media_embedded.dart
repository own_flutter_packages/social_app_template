import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_image_embedded.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';

typedef OnTapMedia = Function(PlatformMedia media);
typedef OnZoomUpdate = Function(bool inZoom);

class PlatformMediaEmbedded extends StatefulWidget {
  const PlatformMediaEmbedded({
    Key? key,
    required this.media,
    required this.availableWidth,
    this.hasSize = false,
    this.bottomPadding,
    this.videoFile,
    this.onZoomUpdate,
    this.onTapMedia,
    this.onSwipeLeft,
    this.onChangedContentQuality,
  }) : super(key: key);

  final List<PlatformMedia> media;
  final double availableWidth;
  final bool hasSize;
  final double? bottomPadding;
  final File? videoFile;
  final OnZoomUpdate? onZoomUpdate;
  final OnTapMedia? onTapMedia;
  final OnSwipeLeft? onSwipeLeft;
  final OnChangedContentQuality? onChangedContentQuality;

  @override
  PlatformMediaEmbeddedState createState() => PlatformMediaEmbeddedState();
}

class PlatformMediaEmbeddedState extends State<PlatformMediaEmbedded>
    with TickerProviderStateMixin {
  final _keyVideo = GlobalKey<PlatformVideoEmbeddedState>();
  final _keyImage = GlobalKey<PlatformImageEmbeddedState>();

  final List<PlatformMedia> media = [];
  bool isVideo = false;

  @override
  void initState() {
    media.addAll(widget.media);

    isVideo = _getVideo(onlyValid: false) != null;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _getVideo() != null
        ? PlatformVideoEmbedded(
            key: _keyVideo,
            video: _getVideo()!,
            videoFile: widget.videoFile,
            bottomPadding: widget.bottomPadding ?? 0,
            availableWidth: widget.availableWidth,
            onTapMedia: widget.onTapMedia,
            onChangedContentQuality: widget.onChangedContentQuality,
            onFoundOriginalSize: widget.hasSize ? null : _onFoundOriginalSize,
          )
        : PlatformImageEmbedded(
            key: _keyImage,
            images: _getPictures(),
            availableWidth: widget.availableWidth,
            onZoomUpdate: widget.onZoomUpdate,
            onSwipeLeft: widget.onSwipeLeft,
            onTapMedia: widget.onTapMedia,
            onFoundOriginalSize: widget.hasSize ? null : _onFoundOriginalSize,
          );
  }

  Future<void> onReloadMedia(List<PlatformMedia> media) async {
    if (media.isNotEmpty) {
      this.media.removeWhere(
            (oldMedia) => media
                .where((newMedia) => newMedia.id == oldMedia.id)
                .isNotEmpty,
          );

      this.media.addAll(media);

      if (mounted) {
        setState(() => {});
      }

      if (isVideo) {
        final videoEmbedded = _keyVideo.currentState;

        if (videoEmbedded != null) {
          final video = _getVideo(onlyValid: true);

          if (video != null) {
            videoEmbedded.onReload(video);
          }
        }
      }
    }
  }

  onReloadZoomMode(newZoomMode) {
    final image = _keyImage.currentState;
    if (image != null) {
      image.onReloadZoomMode(newZoomMode);
    }
  }

  _onFoundOriginalSize(Size originalSize) {
    final mediaWidth = originalSize.width;
    final mediaHeight = originalSize.height;

    media.where((element) => element.width == null).forEach((element) {
      element.width = mediaWidth;
    });

    media.where((element) => element.height == null).forEach((element) {
      element.height = mediaHeight;
    });

    for (var mediaInList in media) {
      PlatformMediaRepository.create(mediaInList);
    }

    if (mounted) {
      setState(() {});
    }
  }

  List<PlatformMedia> _getPictures({bool? onlyValid = true}) {
    return _hasMedia(onlyValid: onlyValid ?? true)
        ? media.where((element) => !(element.isVideo ?? false)).toList()
        : <PlatformMedia>[];
  }

  PlatformMedia? _getVideo({bool? onlyValid = true}) {
    final hasVideo = media.where((m) => m.isVideo ?? false).isNotEmpty;

    final video = _hasMedia(onlyValid: onlyValid ?? true) && hasVideo
        ? media.where((element) => element.isVideo ?? false).first
        : null;

    return video;
  }

  bool _hasMedia({bool onlyValid = false}) {
    return onlyValid
        ? media.where((element) => element.isValid).isNotEmpty
        : media.isNotEmpty;
  }
}
