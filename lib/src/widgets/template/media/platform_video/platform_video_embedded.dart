import 'dart:io';

import 'package:flutter/material.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/sized/cached_image_sized.dart';
import 'package:social_app_template/src/widgets/core/media/video/with_visibility/video_embedded_visibility.dart';
import 'package:social_app_template/src/widgets/template/media/platform_media_embedded.dart';

typedef OnChangedContentQuality = Future<List<PlatformMedia>> Function(
  String id,
  QualityPreference quality,
);

class PlatformVideoEmbedded extends StatefulWidget {
  const PlatformVideoEmbedded({
    Key? key,
    required this.video,
    required this.bottomPadding,
    required this.availableWidth,
    this.videoFile,
    this.onTapMedia,
    this.onFoundOriginalSize,
    this.onChangedContentQuality,
  }) : super(key: key);

  final PlatformMedia video;
  final double bottomPadding;
  final double availableWidth;
  final File? videoFile;
  final OnTapMedia? onTapMedia;
  final OnFoundOriginalSize? onFoundOriginalSize;
  final OnChangedContentQuality? onChangedContentQuality;

  @override
  PlatformVideoEmbeddedState createState() => PlatformVideoEmbeddedState();
}

class PlatformVideoEmbeddedState extends State<PlatformVideoEmbedded> {
  final _keyVideo = GlobalKey<VideoEmbeddedWithVisibilityState>();

  onReload(PlatformMedia video) {
    final videoEmbedded = _keyVideo.currentState;

    if (videoEmbedded != null && video.isValid) {
      videoEmbedded.onReload(video.url!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PerformanceIndicator.contentWithQualityConverter(
      contentId: widget.video.id,
      onChangedContentQuality: (_, __) => _onChangedContentQuality(_, __),
      qualityConverterWidth: 25,
      qualityConverterAlignment: const Alignment(0.95, -0.85),
      child: GestureDetector(
        onTapDown: (_) => _onTap(),
        child: widget.video.isValid
            ? Container(
                width: _getSize()?.width,
                height: _getSize()?.height,
                padding: EdgeInsets.only(bottom: widget.bottomPadding),
                color: Colors.transparent,
                child: VideoEmbeddedWithVisibility(
                  key: _keyVideo,
                  url: widget.video.url!,
                  videoFile: widget.videoFile,
                  onFoundOriginalSize: widget.onFoundOriginalSize,
                ),
              )
            : Container(),
      ),
    );
  }

  Future<void> _onChangedContentQuality(
    String contentId,
    QualityPreference quality,
  ) async {
    if (widget.onChangedContentQuality != null) {
      final reloadedMedia =
          await widget.onChangedContentQuality!(contentId, quality);

      final reloadedVideos = reloadedMedia
          .where((element) => element.isValid && (element.isVideo ?? false))
          .toList();

      if (reloadedVideos.isNotEmpty) {
        onReload(reloadedVideos.first);
      }
    }
  }

  _onTap() {
    if (widget.onTapMedia != null) {
      widget.onTapMedia!(widget.video);
    }
  }

  Size? _getSize() {
    final size = SizeService.getSizeFromMedia([widget.video]);

    return size != null
        ? SizeService.adjustHeightToWidth(size, widget.availableWidth)
        : null;
  }
}
