import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/sized/cached_image_sized.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_media_embedded.dart';

class PlatformImageEmbedded extends StatefulWidget {
  const PlatformImageEmbedded({
    Key? key,
    required this.images,
    required this.availableWidth,
    this.onTapMedia,
    this.onZoomUpdate,
    this.onSwipeLeft,
    this.onFoundOriginalSize,
  }) : super(key: key);

  final List<PlatformMedia> images;
  final double availableWidth;
  final OnTapMedia? onTapMedia;
  final OnZoomUpdate? onZoomUpdate;
  final OnSwipeLeft? onSwipeLeft;
  final OnFoundOriginalSize? onFoundOriginalSize;

  @override
  PlatformImageEmbeddedState createState() => PlatformImageEmbeddedState();
}

class PlatformImageEmbeddedState extends State<PlatformImageEmbedded> {
  late TransformationController controllerTransformation;

  bool isInZoomMode = false;
  double minScale = 1;
  double maxScale = 4;

  @override
  void initState() {
    controllerTransformation = TransformationController();
    super.initState();
  }

  @override
  void dispose() {
    controllerTransformation.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.images.isEmpty) {
      return Container();
    }

    return widget.images.length == 1
        ? GestureDetector(
            onTap: _onTapMedia,
            child: SizedBox(
              width: _getSize()?.width,
              height: _getSize()?.height,
              child: InteractiveViewer(
                boundaryMargin: EdgeInsets.zero,
                transformationController: controllerTransformation,
                clipBehavior: Clip.none,
                constrained: true,
                scaleEnabled: true,
                panEnabled: true,
                minScale: 1,
                maxScale: 4,
                onInteractionStart: _onZoomStart,
                onInteractionEnd: _onZoomEnd,
                child: CachedImageAlwaysWithSize(
                  imageUrl: widget.images.first.url ?? '',
                  size: _getSize(),
                  originalSize: widget.images.first.size,
                  fit: BoxFit.cover,
                  circle: false,
                  onFoundOriginalSize: widget.onFoundOriginalSize,
                ),
              ),
            ),
          )
        : PlatformImageEmbeddedGallery(
            images: widget.images,
            controllerTransformation: controllerTransformation,
            fadeDurationInMs: 50,
            availableWidth: widget.availableWidth,
            minScale: minScale,
            onTapMedia: widget.onTapMedia,
            onSwipeToLeft: widget.onSwipeLeft,
            onFoundOriginalSize: widget.onFoundOriginalSize,
            onUpdateZoomMode: onReloadZoomMode,
          );
  }

  onReloadZoomMode(bool isInZoomMode) {
    if (this.isInZoomMode != isInZoomMode) {
      this.isInZoomMode = isInZoomMode;
      if (mounted) {
        setState(() {});
      }

      if (widget.onZoomUpdate != null) {
        widget.onZoomUpdate!(this.isInZoomMode);
      }
    }
  }

  _onTapMedia() {
    if (widget.onTapMedia != null) {
      widget.onTapMedia!(widget.images.first);
    }
  }

  _onZoomStart(details) {
    onReloadZoomMode(true);
  }

  _onZoomEnd(details) {
    double scale = controllerTransformation.value.getMaxScaleOnAxis();

    if (scale == minScale) {
      onReloadZoomMode(false);
    }
  }

  Size? _getSize() {
    final size = SizeService.getSizeFromMedia(widget.images);

    return size != null
        ? SizeService.adjustHeightToWidth(size, widget.availableWidth)
        : null;
  }
}
