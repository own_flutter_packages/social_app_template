import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';

typedef OnReloadProfileImage = Future<String> Function(PlatformUser user);

class PlatformUserProfileImage extends StatefulWidget {
  const PlatformUserProfileImage({
    Key? key,
    required this.user,
    required this.size,
    this.circle = false,
    this.borderRounding = 5.0,
    this.borderWidth,
    this.borderColor,
    this.borderRadius,
    this.fit,
    this.errorWidget,
    this.onReloadProfileImage,
  }) : super(key: key);

  final PlatformUser user;
  final Size size;
  final bool circle;
  final double borderRounding;
  final double? borderWidth;
  final Color? borderColor;
  final BorderRadius? borderRadius;
  final BoxFit? fit;
  final Widget? errorWidget;
  final OnReloadProfileImage? onReloadProfileImage;

  @override
  PlatformUserProfileImageState createState() =>
      PlatformUserProfileImageState();
}

class PlatformUserProfileImageState extends State<PlatformUserProfileImage> {
  late Future<void> future;

  String? profileImageUrl;
  bool notFound = false;
  bool checkedIfNotFound = false;

  Future<void> _getData() async {
    if (_isImageValid()) {
      notFound = ImageService.isImageMarkedAsNotFound(profileImageUrl!);
    }

    checkedIfNotFound = true;

    final isImageUrlNotAvailable = profileImageUrl?.isEmpty ?? true;

    if ((notFound || isImageUrlNotAvailable) &&
        widget.onReloadProfileImage != null) {
      profileImageUrl = await widget.onReloadProfileImage!(widget.user);

      if (_isImageValid()) {
        notFound = ImageService.isImageMarkedAsNotFound(profileImageUrl!);
        return;
      }

      notFound = true;
    }
  }

  @override
  void initState() {
    profileImageUrl = widget.user.profileImageUrl;

    future = _getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!notFound && checkedIfNotFound && _isImageValid()) {
      _onInitProfileImage();

      return CachedImage(
        imageUrl: profileImageUrl,
        size: widget.size,
        fit: widget.fit ?? BoxFit.cover,
        circle: widget.circle,
        originalSize: _getOriginalSize(),
        errorWidget: _getErrorWidget(),
      );
    }

    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        final futureDone = snapshot.connectionState == ConnectionState.done;
        final imageValid = profileImageUrl?.isNotEmpty ?? false;

        if (futureDone && imageValid && !notFound) {
          _onInitProfileImage();

          return CachedImage(
            imageUrl: profileImageUrl,
            size: widget.size,
            fit: widget.fit ?? BoxFit.cover,
            circle: widget.circle,
            originalSize: _getOriginalSize(),
            errorWidget: _getErrorWidget(),
          );
        }

        return _getErrorWidget(futureDone: futureDone, imageValid: imageValid);
      },
    );
  }

  bool _isImageValid() => (profileImageUrl?.isNotEmpty ?? false);

  _onInitProfileImage() {
    if (profileImageUrl != null) {
      profileImageUrl = ImageService.convertUrlToTwitterSize(
        2,
        profileImageUrl!,
      );
    } else {
      if (kDebugMode) {
        print(
          'no profile image url found ${widget.user.name} ${widget.user.username} ${widget.user.id} -> will reload in cached_image.',
        );
      }
    }
  }

  Widget _getErrorWidget({bool? futureDone, bool? imageValid}) {
    if (futureDone ?? false) {
      if (kDebugMode) {
        print(
          'returning empty image | futureDone:$futureDone imageValid:$imageValid notFound:$notFound || $profileImageUrl',
        );
      }
    }

    return widget.errorWidget ??
        ImageEmbedded.emptyWithIcon(
          widget.size.width,
          iconColor: Colors.white,
          color: Colors.black,
        );
  }

  Size? _getOriginalSize() => ImageService.originalImageSizeProfileImages;
}
