import 'package:align_positioned/align_positioned.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/core/circle/circle.dart';
import 'package:social_app_template/src/widgets/core/media/image/cached/sized/cached_image_sized.dart';

typedef OnSwipeLeft = Function(double offset);

class PlatformImageEmbeddedGallery extends StatefulWidget {
  const PlatformImageEmbeddedGallery({
    Key? key,
    required this.images,
    required this.controllerTransformation,
    required this.availableWidth,
    this.fadeDurationInMs,
    this.minScale,
    this.onUpdateZoomMode,
    this.onTapMedia,
    this.onSwipeToLeft,
    this.onFoundOriginalSize,
  }) : super(key: key);

  final List<PlatformMedia> images;
  final TransformationController controllerTransformation;
  final double availableWidth;
  final int? fadeDurationInMs;
  final double? minScale;
  final Function(bool)? onUpdateZoomMode;
  final Function(PlatformMedia)? onTapMedia;
  final OnSwipeLeft? onSwipeToLeft;
  final OnFoundOriginalSize? onFoundOriginalSize;

  @override
  PlatformImageEmbeddedGalleryState createState() =>
      PlatformImageEmbeddedGalleryState();
}

class PlatformImageEmbeddedGalleryState
    extends State<PlatformImageEmbeddedGallery> with TickerProviderStateMixin {
  late PageController _horCtrl;
  late AnimationController controllerFade;
  late Animation<double> animationFade;

  @override
  void initState() {
    controllerFade = AnimationController(
      duration: const Duration(milliseconds: 10),
      vsync: this,
      value: 1,
    );

    animationFade = CurvedAnimation(
      parent: controllerFade,
      curve: Curves.linear,
    );

    _horCtrl = PageController();
    _horCtrl.addListener(_onScrollInGallery);

    super.initState();
  }

  @override
  void dispose() {
    controllerFade.dispose();
    _horCtrl.removeListener(_onScrollInGallery);
    _horCtrl.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: _getSize().height,
      width: _getSize().width,
      child: PageView.builder(
        controller: _horCtrl,
        scrollDirection: Axis.horizontal,
        itemCount: widget.images.length,
        itemBuilder: (_, index) {
          PlatformMedia? image = widget.images[index];

          return GestureDetector(
            onTap: () => _onTap(image),
            child: FadeTransition(
              opacity: animationFade,
              child: Stack(
                children: [
                  AlignPositioned(
                    child: InteractiveViewer(
                      boundaryMargin: EdgeInsets.zero,
                      transformationController: widget.controllerTransformation,
                      clipBehavior: Clip.none,
                      constrained: true,
                      scaleEnabled: true,
                      panEnabled: true,
                      minScale: 1,
                      maxScale: 4,
                      onInteractionStart: _onZoomStart,
                      onInteractionEnd: _onZoomEnd,
                      child: CachedImageAlwaysWithSize(
                        imageUrl: (image.isVideo ?? false)
                            ? image.previewUrl
                            : image.url,
                        size: _getSize(),
                        circle: false,
                        originalSize: image.size,
                        fit: BoxFit.cover,
                        onFoundOriginalSize: widget.onFoundOriginalSize,
                      ),
                    ),
                  ),
                  AlignPositioned(
                    alignment: Alignment.centerLeft,
                    moveByChildWidth: 0.25,
                    child: GestureDetector(
                      onTap: _onTapPrevious,
                      child: Circle(
                        size: 40,
                        color: Colors.black45.withOpacity(0.4),
                        child: const Icon(
                          MdiIcons.chevronLeft,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  AlignPositioned(
                    alignment: Alignment.centerRight,
                    moveByChildWidth: -0.25,
                    child: GestureDetector(
                      onTap: _onTapNext,
                      child: Circle(
                        size: 40,
                        color: Colors.black45.withOpacity(0.4),
                        child: const Icon(
                          MdiIcons.chevronRight,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Size _getSize() {
    final size = SizeService.getSizeFromMedia(widget.images);

    return size != null
        ? SizeService.adjustHeightToWidth(size, widget.availableWidth)
        : Size(widget.availableWidth, 0);
  }

  _onTap(PlatformMedia image) {
    if (widget.onTapMedia != null) {
      widget.onTapMedia!(image);
    }
  }

  _onScrollInGallery() {
    if (_horCtrl.positions.isNotEmpty) {
      final double offset = _horCtrl.offset;
      final double newValue = offset > -30
          ? 1
          : controllerFade.value +
              (offset / _horCtrl.position.viewportDimension) * 0.3;

      controllerFade.value = newValue;

      if (widget.onSwipeToLeft != null) {
        widget.onSwipeToLeft!(offset);
      }
    }
  }

  _onZoomStart(details) {
    if (widget.onUpdateZoomMode != null) {
      widget.onUpdateZoomMode!(true);
    }
  }

  _onZoomEnd(details) {
    double scale = widget.controllerTransformation.value.getMaxScaleOnAxis();

    final isSameScale = scale == widget.minScale;
    if (widget.onUpdateZoomMode != null && isSameScale) {
      widget.onUpdateZoomMode!(false);
    }
  }

  _onTapPrevious() {
    if ((_horCtrl.page ?? 0) > 0) {
      _horCtrl.previousPage(
        duration: const Duration(milliseconds: 50),
        curve: Curves.linear,
      );
    }
  }

  _onTapNext() {
    if ((_horCtrl.page ?? 0) < widget.images.length - 1) {
      _horCtrl.nextPage(
        duration: const Duration(milliseconds: 50),
        curve: Curves.linear,
      );
    }
  }
}
