import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';

class PlatformPostInListAuthor extends StatelessWidget {
  const PlatformPostInListAuthor({
    Key? key,
    required this.height,
    this.author,
    this.onReloadProfileImage,
  }) : super(key: key);

  final double height;
  final PlatformUser? author;
  final OnReloadProfileImage? onReloadProfileImage;

  @override
  Widget build(BuildContext context) {
    const double paddingTop = 10;
    const double paddingBottom = 10;
    final double sizeImage = height - (paddingTop + paddingBottom);

    return SizedBox(
      height: sizeImage,
      width: sizeImage,
      child: (author == null || _wontHaveSuccess())
          ? ImageEmbedded.emptyWithIcon(
              sizeImage,
              color: Colors.black,
              iconColor: Colors.white,
            )
          : PlatformUserProfileImage(
              user: author!,
              size: Size(sizeImage, sizeImage),
              circle: true,
              borderWidth: 0.2,
              borderColor: Colors.black,
              fit: BoxFit.contain,
              onReloadProfileImage: onReloadProfileImage,
            ),
    );
  }

  bool _wontHaveSuccess() =>
      (author?.profileImageUrl?.isEmpty ?? true) &&
      onReloadProfileImage == null;
}
