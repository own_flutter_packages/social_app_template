import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';

class PostInListBottom extends StatelessWidget {
  const PostInListBottom({
    Key? key,
    required this.height,
    required this.date,
    required this.authorName,
    this.fontColor,
  }) : super(key: key);

  final double height;
  final DateTime date;
  final String authorName;
  final Color? fontColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: StaggeredGrid.count(
        crossAxisCount: 5,
        children: [
          StaggeredGridTile.extent(
            crossAxisCellCount: 2,
            mainAxisExtent: height,
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                _dateAsString(),
                style: _textStyleDate(),
              ),
            ),
          ),
          StaggeredGridTile.extent(
            crossAxisCellCount: 3,
            mainAxisExtent: height,
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                authorName,
                style: _textStyleAuthorName(),
              ),
            ),
          )
        ],
      ),
    );
  }

  TextStyle _textStyleAuthorName() {
    return TextStyle(
      color: fontColor ?? Colors.black,
      fontSize: 13,
    );
  }

  TextStyle _textStyleDate() {
    return _textStyleAuthorName();
  }

  String _dateAsString() {
    final String? date = _convertDate();
    final String? time = _convertTime();

    return '${date ?? ''}${time != null ? '  $time' : ''}';
  }

  String? _convertTime() => DateFormat('HH:mm').format(date);

  String? _convertDate() => DateFormat('dd MMM yy').format(date);
}
