import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';
import 'package:social_app_template/src/widgets/template/post/in_list/components/platform_post_in_list_author.dart';
import 'package:social_app_template/src/widgets/template/post/in_list/components/post_in_list_bottom.dart';

class PlatformPostInList extends StatelessWidget {
  const PlatformPostInList({
    Key? key,
    required this.post,
    required this.index,
    this.heightDescriptionMin,
    this.videoFile,
    this.onReloadProfileImage,
    this.onSwipeLeft,
    this.onTapPost,
    this.onChangedContentQuality,
  }) : super(key: key);

  final PlatformPost post;
  final int index;
  final double? heightDescriptionMin;
  final File? videoFile;
  final OnReloadProfileImage? onReloadProfileImage;
  final OnSwipeLeft? onSwipeLeft;
  final OnTapPost? onTapPost;
  final OnChangedContentQuality? onChangedContentQuality;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final postMedia = post.mediaWithUrlValid.toList();
    final hasMedia = postMedia.isNotEmpty;

    return Container(
      width: screenSize.width,
      color: Colors.black,
      child: StaggeredGrid.count(
        crossAxisCount: 12,
        children: [
          hasMedia
              ? StaggeredGridTile.fit(
                  crossAxisCellCount: 12,
                  child: PlatformMediaEmbedded(
                    media: postMedia,
                    videoFile: videoFile,
                    availableWidth: screenSize.width,
                    onSwipeLeft: onSwipeLeft,
                    onChangedContentQuality: onChangedContentQuality,
                    onTapMedia: _onTapMedia,
                  ),
                )
              : Container(),
          StaggeredGridTile.fit(
            crossAxisCellCount: 9,
            child: GestureDetector(
              onTap: _onTapPost,
              child: Container(
                constraints: BoxConstraints(
                  minHeight: _getHeightDescriptionMin(screenSize),
                ),
                color: Colors.transparent,
                padding: const EdgeInsets.only(top: 10, left: 10.0),
                alignment: Alignment.centerLeft,
                child: _description(),
              ),
            ),
          ),
          StaggeredGridTile.extent(
            crossAxisCellCount: 3,
            mainAxisExtent: _getHeightDescriptionMin(screenSize),
            child: GestureDetector(
              onTap: _onTapPost,
              child: Container(
                color: Colors.transparent,
                alignment: Alignment.centerRight,
                child: PlatformPostInListAuthor(
                  author: post.user.value,
                  height: _getHeightDescriptionMin(screenSize) * 0.8,
                  onReloadProfileImage: onReloadProfileImage,
                ),
              ),
            ),
          ),
          StaggeredGridTile.extent(
            crossAxisCellCount: 12,
            mainAxisExtent: 50,
            child: GestureDetector(
              onTap: _onTapPost,
              child: Container(
                color: Colors.transparent,
                child: PostInListBottom(
                  authorName: _authorName() ?? '',
                  height: 50,
                  date: _createdAtDate() ?? DateTime.now(),
                  fontColor: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  double _getHeightDescriptionMin(Size screenSize) {
    return heightDescriptionMin ?? screenSize.height * 0.1;
  }

  _onTapPost() {
    if (onTapPost != null) {
      onTapPost!(post, index);
    }
  }

  _onTapMedia(media) {
    if (onTapPost != null) {
      onTapPost!(post, index);
    }
  }

  PlatformPost _textPost() {
    return post;
  }

  Widget _description() {
    final textPost = _textPost();

    return Text(
      textPost.text ?? '',
      style: const TextStyle(color: Colors.white),
    );
  }

  PlatformUser? _author() {
    final textPost = _textPost();

    return textPost.user.value;
  }

  String? _authorName() {
    return _author()?.username ?? _author()?.name;
  }

  DateTime? _createdAtDate() {
    final createdAtPost = _textPost();
    final createdAt = createdAtPost.createdAt;

    return createdAt != null
        ? DateTime.fromMillisecondsSinceEpoch(createdAt)
        : null;
  }
}
