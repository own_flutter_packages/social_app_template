import 'package:align_positioned/align_positioned.dart';
import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/widgets/core/indicator/progress/progress_indicator.dart'
    as progress_indicator;
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';

class PlatformPostListWithProgress extends StatefulWidget {
  const PlatformPostListWithProgress({
    Key? key,
    required this.ids,
    this.shouldReloadProfileImage = false,
    this.colorRefreshIndicator,
    this.progressAlignment,
    this.progressMainColor,
    this.progressBackgroundColor,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
    this.onInitPost,
    this.onTapPost,
    this.onRefresh,
    this.onSwipeLeft,
    this.onReloadProfileImage,
    this.onChangedContentQuality,
  }) : super(key: key);

  final List<String> ids;
  final bool shouldReloadProfileImage;
  final Color? colorRefreshIndicator;
  final Alignment? progressAlignment;
  final Color? progressMainColor;
  final Color? progressBackgroundColor;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;
  final OnInitPost? onInitPost;
  final OnTapPost? onTapPost;
  final OnRefresh? onRefresh;
  final OnSwipeLeft? onSwipeLeft;
  final OnReloadProfileImage? onReloadProfileImage;
  final OnChangedContentQuality? onChangedContentQuality;

  @override
  PlatformPostListWithProgressState createState() =>
      PlatformPostListWithProgressState();
}

class PlatformPostListWithProgressState
    extends State<PlatformPostListWithProgress> {
  final _keyProgress = GlobalKey<progress_indicator.ProgressIndicatorState>();
  final _keyList = GlobalKey<PlatformPostListState>();

  onReload(List<String> newPostIds, {double? progress}) {
    if (progress != null) {
      _onReloadProgress(progress);
    }

    _onReloadList(newPostIds);
  }

  @override
  Widget build(BuildContext context) {
    return AlignPositioned.relative(
      child: progress_indicator.ProgressIndicator(
        key: _keyProgress,
        progressColor: widget.progressMainColor,
        descriptionColor: widget.progressMainColor,
        backgroundColor: widget.progressBackgroundColor,
        withDescription: false,
        heightBar: 2,
      ),
      container: PlatformPostList(
        key: _keyList,
        ids: widget.ids,
        shouldReloadProfileImage: widget.shouldReloadProfileImage,
        noMoreItemsIndicatorBuilder: widget.noMoreItemsIndicatorBuilder,
        colorRefreshIndicator: widget.colorRefreshIndicator,
        noFoundItemsIndicatorBuilder: widget.noFoundItemsIndicatorBuilder,
        onInitPost: widget.onInitPost,
        onTapPost: widget.onTapPost,
        onRefresh: widget.onRefresh,
        onSwipeLeft: widget.onSwipeLeft,
        onReloadProfileImage: widget.onReloadProfileImage,
        onChangedContentQuality: widget.onChangedContentQuality,
        onReloadProgress: _onReloadProgress,
      ),
      alignment: widget.progressAlignment ?? const Alignment(0.5, -0.9),
    );
  }

  _onReloadList(List<String> newPostIds) {
    final listView = _keyList.currentState;

    if (listView != null) {
      listView.onReload(newPostIds);
    }
  }

  _onReloadProgress(double progress) {
    final progressIndicator = _keyProgress.currentState;

    if (progressIndicator != null) {
      progressIndicator.onReload(progress);
    }
  }
}
