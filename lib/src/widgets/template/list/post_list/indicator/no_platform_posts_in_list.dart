import 'package:flutter/material.dart';

class NoPostsInList extends StatelessWidget {
  const NoPostsInList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: Container()),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 0.5),
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Text('Sorry, no posts were found', style: _titleStyle()),
              const SizedBox(height: 10),
              Text('Try to optimize your filters', style: _descriptionStyle()),
            ],
          ),
        ),
        Expanded(child: Container()),
      ],
    );
  }

  TextStyle _titleStyle() => const TextStyle(
        fontSize: 18,
        color: Colors.white,
      );

  TextStyle _descriptionStyle() => const TextStyle(
        fontSize: 15,
        color: Colors.white,
      );
}
