import 'package:flutter/material.dart';

class NoMorePostsInList extends StatelessWidget {
  const NoMorePostsInList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 0.5),
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(10),
          child: Text(
            'Sorry, no more posts were found',
            style: _textStyleMessage(),
          ),
        ),
      ),
    );
  }

  TextStyle _textStyleMessage() {
    return const TextStyle(color: Colors.white);
  }
}
