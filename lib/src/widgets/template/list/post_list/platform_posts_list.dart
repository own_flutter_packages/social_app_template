import 'package:flutter/material.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/services/video_service.dart';
import 'package:social_app_template/src/widgets/core/list/infinite/infinite_list.dart';
import 'package:social_app_template/src/widgets/core/list/infinite/item/infinite_list_item.dart';
import 'package:social_app_template/src/widgets/template/list/post_list/indicator/no_more_platform_posts_in_list.dart';
import 'package:social_app_template/src/widgets/template/list/post_list/indicator/no_platform_posts_in_list.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';
import 'package:social_app_template/src/widgets/template/post/in_list/post_in_list.dart';

typedef OnInitPost = Future<PlatformPost?> Function(PlatformPost post);
typedef OnTapPost = Function(PlatformPost post, int index);
typedef OnRefresh = Future<List<String>> Function();

typedef OnReloadProgress = Function(double progress);

class PlatformPostList extends StatefulWidget {
  const PlatformPostList({
    Key? key,
    required this.ids,
    this.shouldReloadProfileImage = false,
    this.colorRefreshIndicator,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
    this.onInitPost,
    this.onTapPost,
    this.onRefresh,
    this.onSwipeLeft,
    this.onReloadProgress,
    this.onReloadProfileImage,
    this.onChangedContentQuality,
  }) : super(key: key);

  final List<String> ids;
  final bool shouldReloadProfileImage;
  final Color? colorRefreshIndicator;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;
  final OnInitPost? onInitPost;
  final OnTapPost? onTapPost;
  final OnRefresh? onRefresh;
  final OnSwipeLeft? onSwipeLeft;
  final OnReloadProgress? onReloadProgress;
  final OnReloadProfileImage? onReloadProfileImage;
  final OnChangedContentQuality? onChangedContentQuality;

  @override
  State<PlatformPostList> createState() => PlatformPostListState();
}

class PlatformPostListState extends State<PlatformPostList> {
  final _keyList = GlobalKey<InfiniteListState>();

  List<String> postIds = [];
  List<String> loadedIds = [];

  onReload(
    List<String> newPostIds,
  ) {
    postIds = postIds
      ..clear()
      ..addAll(newPostIds)
      ..toSet()
      ..toList();

    _keyList.currentState?.onReload(itemsCount: postIds.length);
  }

  @override
  void initState() {
    postIds.addAll(widget.ids);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final heightDescriptionMin = MediaQuery.of(context).size.height * 0.1;

    return RefreshIndicator(
      onRefresh: _onRefreshByPull,
      color: widget.colorRefreshIndicator,
      backgroundColor: Colors.transparent,
      child: InfiniteList<InfiniteListItem<PlatformPost>>(
        key: _keyList,
        getData: _onLoadPostsPage,
        itemsCount: postIds.length,
        itemsExtent: heightDescriptionMin,
        itemsBuilder: (ctx, item, index) => PlatformPostInList(
          post: item.item,
          videoFile: item.videoFile,
          index: index,
          onSwipeLeft: widget.onSwipeLeft,
          onReloadProfileImage: widget.shouldReloadProfileImage
              ? widget.onReloadProfileImage
              : null,
          onTapPost: widget.onTapPost,
          onChangedContentQuality: widget.onChangedContentQuality,
        ),
        noMoreItemsIndicatorBuilder: _noMoreItemsIndicatorBuilder(),
        noFoundItemsIndicatorBuilder: _noFoundItemsIndicatorBuilder(),
      ),
    );
  }

  Future<List<InfiniteListItem<PlatformPost>>> _onLoadPostsPage(
    int startIndex,
    int nextItemsCount,
  ) async {
    final nextItems = _getNextItems(startIndex, nextItemsCount);

    if (nextItems.isNotEmpty) {
      _onReloadProgress(0.15);
      final posts = await PlatformPostRepository.getByIds(nextItems);

      if (posts.isNotEmpty) {
        _onReloadProgress(0.2);
        return await _onInitPosts(posts);
      }
    }

    _onReloadProgress(1.0);
    return <InfiniteListItem<PlatformPost>>[];
  }

  Future<List<InfiniteListItem<PlatformPost>>> _onInitPosts(
    List<PlatformPost?> posts,
  ) async {
    final List<InfiniteListItem<PlatformPost>> newItems = [];
    final postsCount = posts.length;

    await Future.forEach(posts, (PlatformPost? post) async {
      if (post == null) return;

      final index = posts.indexOf(post);

      if (widget.onInitPost != null) {
        final postInitialized = await widget.onInitPost!(post);

        if (postInitialized != null) {
          post = postInitialized;
        }
      }

      newItems.add(await _onInitInfiniteListItem(post));

      _onReloadProgress((index + 1) / postsCount);
    });

    _onReloadProgress(1.0);
    return newItems;
  }

  Future<InfiniteListItem<PlatformPost>> _onInitInfiniteListItem(
    PlatformPost post,
  ) async {
    final videos = post.mediaWithUrlValid
        .where((element) => element.isVideo ?? false)
        .toList();

    if (videos.isNotEmpty) {
      final videoUrl = videos.first.url;
      final videoFile = videoUrl != null
          ? await VideoService().loadVideoFromWeb(
              videoUrl,
              timeout: const Duration(milliseconds: 1500),
            )
          : null;

      if (videoFile != null) {
        return InfiniteListItem<PlatformPost>(
          item: post,
          videoFile: videoFile,
        );
      } else {
        return InfiniteListItem<PlatformPost>(item: post);
      }
    } else {
      return InfiniteListItem<PlatformPost>(item: post);
    }
  }

  Future<void> _onRefreshByPull() async {
    if (widget.onRefresh != null) {
      postIds.clear();

      final newPostIds = await widget.onRefresh!();

      onReload(newPostIds);
    }
  }

  _onReloadProgress(double progress) {
    if (widget.onReloadProgress != null) {
      widget.onReloadProgress!(progress);
    }
  }

  List<String> _getNextItems(int startIndex, int nextItemsCount) {
    final start = startIndex < postIds.length ? startIndex : 0;

    int end = start + nextItemsCount;
    end = end < postIds.length ? end : postIds.length;

    if (start == 0 && end == 0) {
      return <String>[];
    }

    return List.of(postIds.sublist(startIndex, end));
  }

  WidgetBuilder _noMoreItemsIndicatorBuilder() {
    return widget.noMoreItemsIndicatorBuilder ??
        (context) => const NoMorePostsInList();
  }

  WidgetBuilder _noFoundItemsIndicatorBuilder() {
    return widget.noFoundItemsIndicatorBuilder ??
        (context) => const NoPostsInList();
  }
}
