// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'twitter_context_entity.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetTwitterContextEntityCollection on Isar {
  IsarCollection<TwitterContextEntity> get twitterContextEntity =>
      this.collection();
}

const TwitterContextEntitySchema = CollectionSchema(
  name: r'TwitterContextEntity',
  id: 6277891008519828234,
  properties: {
    r'domains': PropertySchema(
      id: 0,
      name: r'domains',
      type: IsarType.stringList,
    ),
    r'entityId': PropertySchema(
      id: 1,
      name: r'entityId',
      type: IsarType.string,
    ),
    r'entityName': PropertySchema(
      id: 2,
      name: r'entityName',
      type: IsarType.string,
    ),
    r'isUnifiedTwitterTaxonomy': PropertySchema(
      id: 3,
      name: r'isUnifiedTwitterTaxonomy',
      type: IsarType.bool,
    )
  },
  estimateSize: _twitterContextEntityEstimateSize,
  serialize: _twitterContextEntitySerialize,
  deserialize: _twitterContextEntityDeserialize,
  deserializeProp: _twitterContextEntityDeserializeProp,
  idName: r'id',
  indexes: {
    r'entityId': IndexSchema(
      id: 745355021660786263,
      name: r'entityId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'entityId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'entityName': IndexSchema(
      id: -1749110902930819992,
      name: r'entityName',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'entityName',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'isUnifiedTwitterTaxonomy': IndexSchema(
      id: 4019585217352375209,
      name: r'isUnifiedTwitterTaxonomy',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'isUnifiedTwitterTaxonomy',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _twitterContextEntityGetId,
  getLinks: _twitterContextEntityGetLinks,
  attach: _twitterContextEntityAttach,
  version: '3.0.5',
);

int _twitterContextEntityEstimateSize(
  TwitterContextEntity object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final list = object.domains;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.entityId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.entityName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _twitterContextEntitySerialize(
  TwitterContextEntity object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeStringList(offsets[0], object.domains);
  writer.writeString(offsets[1], object.entityId);
  writer.writeString(offsets[2], object.entityName);
  writer.writeBool(offsets[3], object.isUnifiedTwitterTaxonomy);
}

TwitterContextEntity _twitterContextEntityDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = TwitterContextEntity(
    domains: reader.readStringList(offsets[0]),
    entityId: reader.readStringOrNull(offsets[1]),
    entityName: reader.readStringOrNull(offsets[2]),
    isUnifiedTwitterTaxonomy: reader.readBoolOrNull(offsets[3]),
  );
  object.id = id;
  return object;
}

P _twitterContextEntityDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringList(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _twitterContextEntityGetId(TwitterContextEntity object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _twitterContextEntityGetLinks(
    TwitterContextEntity object) {
  return [];
}

void _twitterContextEntityAttach(
    IsarCollection<dynamic> col, Id id, TwitterContextEntity object) {
  object.id = id;
}

extension TwitterContextEntityQueryWhereSort
    on QueryBuilder<TwitterContextEntity, TwitterContextEntity, QWhere> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhere>
      anyEntityId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'entityId'),
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhere>
      anyEntityName() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'entityName'),
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhere>
      anyIsUnifiedTwitterTaxonomy() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'isUnifiedTwitterTaxonomy'),
      );
    });
  }
}

extension TwitterContextEntityQueryWhere
    on QueryBuilder<TwitterContextEntity, TwitterContextEntity, QWhereClause> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityId',
        value: [null],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdEqualTo(String? entityId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityId',
        value: [entityId],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdNotEqualTo(String? entityId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityId',
              lower: [],
              upper: [entityId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityId',
              lower: [entityId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityId',
              lower: [entityId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityId',
              lower: [],
              upper: [entityId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdGreaterThan(
    String? entityId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityId',
        lower: [entityId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdLessThan(
    String? entityId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityId',
        lower: [],
        upper: [entityId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdBetween(
    String? lowerEntityId,
    String? upperEntityId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityId',
        lower: [lowerEntityId],
        includeLower: includeLower,
        upper: [upperEntityId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdStartsWith(String EntityIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityId',
        lower: [EntityIdPrefix],
        upper: ['$EntityIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityId',
        value: [''],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'entityId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'entityId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'entityId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'entityId',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityName',
        value: [null],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityName',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameEqualTo(String? entityName) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityName',
        value: [entityName],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameNotEqualTo(String? entityName) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityName',
              lower: [],
              upper: [entityName],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityName',
              lower: [entityName],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityName',
              lower: [entityName],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'entityName',
              lower: [],
              upper: [entityName],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameGreaterThan(
    String? entityName, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityName',
        lower: [entityName],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameLessThan(
    String? entityName, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityName',
        lower: [],
        upper: [entityName],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameBetween(
    String? lowerEntityName,
    String? upperEntityName, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityName',
        lower: [lowerEntityName],
        includeLower: includeLower,
        upper: [upperEntityName],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameStartsWith(String EntityNamePrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'entityName',
        lower: [EntityNamePrefix],
        upper: ['$EntityNamePrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'entityName',
        value: [''],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      entityNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'entityName',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'entityName',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'entityName',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'entityName',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      isUnifiedTwitterTaxonomyIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'isUnifiedTwitterTaxonomy',
        value: [null],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      isUnifiedTwitterTaxonomyIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'isUnifiedTwitterTaxonomy',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      isUnifiedTwitterTaxonomyEqualTo(bool? isUnifiedTwitterTaxonomy) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'isUnifiedTwitterTaxonomy',
        value: [isUnifiedTwitterTaxonomy],
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterWhereClause>
      isUnifiedTwitterTaxonomyNotEqualTo(bool? isUnifiedTwitterTaxonomy) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isUnifiedTwitterTaxonomy',
              lower: [],
              upper: [isUnifiedTwitterTaxonomy],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isUnifiedTwitterTaxonomy',
              lower: [isUnifiedTwitterTaxonomy],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isUnifiedTwitterTaxonomy',
              lower: [isUnifiedTwitterTaxonomy],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isUnifiedTwitterTaxonomy',
              lower: [],
              upper: [isUnifiedTwitterTaxonomy],
              includeUpper: false,
            ));
      }
    });
  }
}

extension TwitterContextEntityQueryFilter on QueryBuilder<TwitterContextEntity,
    TwitterContextEntity, QFilterCondition> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'domains',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'domains',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'domains',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      domainsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'domains',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      domainsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'domains',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'domains',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'domains',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> domainsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'domains',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'entityId',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'entityId',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'entityId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      entityIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'entityId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      entityIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'entityId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'entityId',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'entityId',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'entityName',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'entityName',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'entityName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      entityNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'entityName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
          QAfterFilterCondition>
      entityNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'entityName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'entityName',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> entityNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'entityName',
        value: '',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> isUnifiedTwitterTaxonomyIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isUnifiedTwitterTaxonomy',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> isUnifiedTwitterTaxonomyIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isUnifiedTwitterTaxonomy',
      ));
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity,
      QAfterFilterCondition> isUnifiedTwitterTaxonomyEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isUnifiedTwitterTaxonomy',
        value: value,
      ));
    });
  }
}

extension TwitterContextEntityQueryObject on QueryBuilder<TwitterContextEntity,
    TwitterContextEntity, QFilterCondition> {}

extension TwitterContextEntityQueryLinks on QueryBuilder<TwitterContextEntity,
    TwitterContextEntity, QFilterCondition> {}

extension TwitterContextEntityQuerySortBy
    on QueryBuilder<TwitterContextEntity, TwitterContextEntity, QSortBy> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByEntityId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityId', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByEntityIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityId', Sort.desc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByEntityName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityName', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByEntityNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityName', Sort.desc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByIsUnifiedTwitterTaxonomy() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isUnifiedTwitterTaxonomy', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      sortByIsUnifiedTwitterTaxonomyDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isUnifiedTwitterTaxonomy', Sort.desc);
    });
  }
}

extension TwitterContextEntityQuerySortThenBy
    on QueryBuilder<TwitterContextEntity, TwitterContextEntity, QSortThenBy> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByEntityId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityId', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByEntityIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityId', Sort.desc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByEntityName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityName', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByEntityNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'entityName', Sort.desc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByIsUnifiedTwitterTaxonomy() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isUnifiedTwitterTaxonomy', Sort.asc);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QAfterSortBy>
      thenByIsUnifiedTwitterTaxonomyDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isUnifiedTwitterTaxonomy', Sort.desc);
    });
  }
}

extension TwitterContextEntityQueryWhereDistinct
    on QueryBuilder<TwitterContextEntity, TwitterContextEntity, QDistinct> {
  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QDistinct>
      distinctByDomains() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'domains');
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QDistinct>
      distinctByEntityId({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'entityId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QDistinct>
      distinctByEntityName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'entityName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TwitterContextEntity, TwitterContextEntity, QDistinct>
      distinctByIsUnifiedTwitterTaxonomy() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isUnifiedTwitterTaxonomy');
    });
  }
}

extension TwitterContextEntityQueryProperty on QueryBuilder<
    TwitterContextEntity, TwitterContextEntity, QQueryProperty> {
  QueryBuilder<TwitterContextEntity, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<TwitterContextEntity, List<String>?, QQueryOperations>
      domainsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'domains');
    });
  }

  QueryBuilder<TwitterContextEntity, String?, QQueryOperations>
      entityIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'entityId');
    });
  }

  QueryBuilder<TwitterContextEntity, String?, QQueryOperations>
      entityNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'entityName');
    });
  }

  QueryBuilder<TwitterContextEntity, bool?, QQueryOperations>
      isUnifiedTwitterTaxonomyProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isUnifiedTwitterTaxonomy');
    });
  }
}
