import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/modules/social_media/twitter/repo/model/twitter_context_entity.dart';

class TwitterContextEntityRepository {
  static create(TwitterContextEntity contextEntity) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.twitterContextEntity.putSync(contextEntity),
    );
  }

  static TwitterContextEntity? get(int id) {
    return IsarDB.isar.twitterContextEntity.getSync(id);
  }

  static List<TwitterContextEntity> getByQuery(String query) {
    return IsarDB.isar.twitterContextEntity
        .where()
        .filter()
        .entityNameContains(query.toLowerCase(), caseSensitive: false)
        .findAllSync();
  }

  static bool existsAny() {
    return IsarDB.isar.twitterContextEntity.countSync() != 0;
  }

  static List<TwitterContextEntity> getAll() {
    return IsarDB.isar.twitterContextEntity.where().findAllSync();
  }

  static delete(int id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.twitterContextEntity.deleteSync(id),
    );
  }
}
