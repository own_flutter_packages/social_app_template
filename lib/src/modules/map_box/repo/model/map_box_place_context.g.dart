// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_box_place_context.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetMapBoxPlaceContextCollection on Isar {
  IsarCollection<MapBoxPlaceContext> get mapBoxPlaceContext =>
      this.collection();
}

const MapBoxPlaceContextSchema = CollectionSchema(
  name: r'MapBoxPlaceContext',
  id: 8455270108081781221,
  properties: {
    r'id': PropertySchema(
      id: 0,
      name: r'id',
      type: IsarType.string,
    ),
    r'mapBoxPlaceId': PropertySchema(
      id: 1,
      name: r'mapBoxPlaceId',
      type: IsarType.string,
    ),
    r'shortCode': PropertySchema(
      id: 2,
      name: r'shortCode',
      type: IsarType.string,
    ),
    r'text': PropertySchema(
      id: 3,
      name: r'text',
      type: IsarType.string,
    ),
    r'wikiData': PropertySchema(
      id: 4,
      name: r'wikiData',
      type: IsarType.string,
    )
  },
  estimateSize: _mapBoxPlaceContextEstimateSize,
  serialize: _mapBoxPlaceContextSerialize,
  deserialize: _mapBoxPlaceContextDeserialize,
  deserializeProp: _mapBoxPlaceContextDeserializeProp,
  idName: r'isarId',
  indexes: {
    r'id': IndexSchema(
      id: -3268401673993471357,
      name: r'id',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'id',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'mapBoxPlaceId': IndexSchema(
      id: 3770883193820087212,
      name: r'mapBoxPlaceId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'mapBoxPlaceId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _mapBoxPlaceContextGetId,
  getLinks: _mapBoxPlaceContextGetLinks,
  attach: _mapBoxPlaceContextAttach,
  version: '3.0.5',
);

int _mapBoxPlaceContextEstimateSize(
  MapBoxPlaceContext object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.id.length * 3;
  bytesCount += 3 + object.mapBoxPlaceId.length * 3;
  {
    final value = object.shortCode;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.text;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.wikiData;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _mapBoxPlaceContextSerialize(
  MapBoxPlaceContext object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.id);
  writer.writeString(offsets[1], object.mapBoxPlaceId);
  writer.writeString(offsets[2], object.shortCode);
  writer.writeString(offsets[3], object.text);
  writer.writeString(offsets[4], object.wikiData);
}

MapBoxPlaceContext _mapBoxPlaceContextDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = MapBoxPlaceContext(
    id: reader.readString(offsets[0]),
    mapBoxPlaceId: reader.readString(offsets[1]),
    shortCode: reader.readStringOrNull(offsets[2]),
    text: reader.readStringOrNull(offsets[3]),
    wikiData: reader.readStringOrNull(offsets[4]),
  );
  return object;
}

P _mapBoxPlaceContextDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readString(offset)) as P;
    case 1:
      return (reader.readString(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _mapBoxPlaceContextGetId(MapBoxPlaceContext object) {
  return object.isarId;
}

List<IsarLinkBase<dynamic>> _mapBoxPlaceContextGetLinks(
    MapBoxPlaceContext object) {
  return [];
}

void _mapBoxPlaceContextAttach(
    IsarCollection<dynamic> col, Id id, MapBoxPlaceContext object) {}

extension MapBoxPlaceContextByIndex on IsarCollection<MapBoxPlaceContext> {
  Future<MapBoxPlaceContext?> getById(String id) {
    return getByIndex(r'id', [id]);
  }

  MapBoxPlaceContext? getByIdSync(String id) {
    return getByIndexSync(r'id', [id]);
  }

  Future<bool> deleteById(String id) {
    return deleteByIndex(r'id', [id]);
  }

  bool deleteByIdSync(String id) {
    return deleteByIndexSync(r'id', [id]);
  }

  Future<List<MapBoxPlaceContext?>> getAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndex(r'id', values);
  }

  List<MapBoxPlaceContext?> getAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'id', values);
  }

  Future<int> deleteAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'id', values);
  }

  int deleteAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'id', values);
  }

  Future<Id> putById(MapBoxPlaceContext object) {
    return putByIndex(r'id', object);
  }

  Id putByIdSync(MapBoxPlaceContext object, {bool saveLinks = true}) {
    return putByIndexSync(r'id', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllById(List<MapBoxPlaceContext> objects) {
    return putAllByIndex(r'id', objects);
  }

  List<Id> putAllByIdSync(List<MapBoxPlaceContext> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'id', objects, saveLinks: saveLinks);
  }
}

extension MapBoxPlaceContextQueryWhereSort
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QWhere> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhere>
      anyIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'id'),
      );
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhere>
      anyMapBoxPlaceId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'mapBoxPlaceId'),
      );
    });
  }
}

extension MapBoxPlaceContextQueryWhere
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QWhereClause> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      isarIdEqualTo(Id isarId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: isarId,
        upper: isarId,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      isarIdNotEqualTo(Id isarId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      isarIdGreaterThan(Id isarId, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: isarId, includeLower: include),
      );
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      isarIdLessThan(Id isarId, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: isarId, includeUpper: include),
      );
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      isarIdBetween(
    Id lowerIsarId,
    Id upperIsarId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerIsarId,
        includeLower: includeLower,
        upper: upperIsarId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idEqualTo(String id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [id],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idNotEqualTo(String id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idGreaterThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [id],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idLessThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [],
        upper: [id],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idBetween(
    String lowerId,
    String upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [lowerId],
        includeLower: includeLower,
        upper: [upperId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idStartsWith(String IdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [IdPrefix],
        upper: ['$IdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [''],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdEqualTo(String mapBoxPlaceId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'mapBoxPlaceId',
        value: [mapBoxPlaceId],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdNotEqualTo(String mapBoxPlaceId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'mapBoxPlaceId',
              lower: [],
              upper: [mapBoxPlaceId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'mapBoxPlaceId',
              lower: [mapBoxPlaceId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'mapBoxPlaceId',
              lower: [mapBoxPlaceId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'mapBoxPlaceId',
              lower: [],
              upper: [mapBoxPlaceId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdGreaterThan(
    String mapBoxPlaceId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'mapBoxPlaceId',
        lower: [mapBoxPlaceId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdLessThan(
    String mapBoxPlaceId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'mapBoxPlaceId',
        lower: [],
        upper: [mapBoxPlaceId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdBetween(
    String lowerMapBoxPlaceId,
    String upperMapBoxPlaceId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'mapBoxPlaceId',
        lower: [lowerMapBoxPlaceId],
        includeLower: includeLower,
        upper: [upperMapBoxPlaceId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdStartsWith(String MapBoxPlaceIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'mapBoxPlaceId',
        lower: [MapBoxPlaceIdPrefix],
        upper: ['$MapBoxPlaceIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'mapBoxPlaceId',
        value: [''],
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterWhereClause>
      mapBoxPlaceIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'mapBoxPlaceId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'mapBoxPlaceId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'mapBoxPlaceId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'mapBoxPlaceId',
              upper: [''],
            ));
      }
    });
  }
}

extension MapBoxPlaceContextQueryFilter
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QFilterCondition> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'id',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      isarIdEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      isarIdGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      isarIdLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      isarIdBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'isarId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'mapBoxPlaceId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'mapBoxPlaceId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'mapBoxPlaceId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'mapBoxPlaceId',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      mapBoxPlaceIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'mapBoxPlaceId',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'shortCode',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'shortCode',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'shortCode',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'shortCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'shortCode',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'shortCode',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      shortCodeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'shortCode',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'text',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'text',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'text',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'text',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'text',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      textIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'text',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'wikiData',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'wikiData',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'wikiData',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'wikiData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'wikiData',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wikiData',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterFilterCondition>
      wikiDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'wikiData',
        value: '',
      ));
    });
  }
}

extension MapBoxPlaceContextQueryObject
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QFilterCondition> {}

extension MapBoxPlaceContextQueryLinks
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QFilterCondition> {}

extension MapBoxPlaceContextQuerySortBy
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QSortBy> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByMapBoxPlaceId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mapBoxPlaceId', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByMapBoxPlaceIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mapBoxPlaceId', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByShortCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shortCode', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByShortCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shortCode', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByWikiData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wikiData', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      sortByWikiDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wikiData', Sort.desc);
    });
  }
}

extension MapBoxPlaceContextQuerySortThenBy
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QSortThenBy> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByIsarIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByMapBoxPlaceId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mapBoxPlaceId', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByMapBoxPlaceIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mapBoxPlaceId', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByShortCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shortCode', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByShortCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shortCode', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByWikiData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wikiData', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QAfterSortBy>
      thenByWikiDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wikiData', Sort.desc);
    });
  }
}

extension MapBoxPlaceContextQueryWhereDistinct
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct> {
  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct> distinctById(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'id', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct>
      distinctByMapBoxPlaceId({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'mapBoxPlaceId',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct>
      distinctByShortCode({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'shortCode', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct>
      distinctByText({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'text', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QDistinct>
      distinctByWikiData({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'wikiData', caseSensitive: caseSensitive);
    });
  }
}

extension MapBoxPlaceContextQueryProperty
    on QueryBuilder<MapBoxPlaceContext, MapBoxPlaceContext, QQueryProperty> {
  QueryBuilder<MapBoxPlaceContext, int, QQueryOperations> isarIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isarId');
    });
  }

  QueryBuilder<MapBoxPlaceContext, String, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<MapBoxPlaceContext, String, QQueryOperations>
      mapBoxPlaceIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'mapBoxPlaceId');
    });
  }

  QueryBuilder<MapBoxPlaceContext, String?, QQueryOperations>
      shortCodeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'shortCode');
    });
  }

  QueryBuilder<MapBoxPlaceContext, String?, QQueryOperations> textProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'text');
    });
  }

  QueryBuilder<MapBoxPlaceContext, String?, QQueryOperations>
      wikiDataProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'wikiData');
    });
  }
}
