// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_box_place.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetMapBoxPlaceCollection on Isar {
  IsarCollection<MapBoxPlace> get mapBoxPlace => this.collection();
}

const MapBoxPlaceSchema = CollectionSchema(
  name: r'MapBoxPlace',
  id: -7753694647971908146,
  properties: {
    r'id': PropertySchema(
      id: 0,
      name: r'id',
      type: IsarType.string,
    ),
    r'matchingPlaceName': PropertySchema(
      id: 1,
      name: r'matchingPlaceName',
      type: IsarType.string,
    ),
    r'matchingText': PropertySchema(
      id: 2,
      name: r'matchingText',
      type: IsarType.string,
    ),
    r'placeName': PropertySchema(
      id: 3,
      name: r'placeName',
      type: IsarType.string,
    ),
    r'text': PropertySchema(
      id: 4,
      name: r'text',
      type: IsarType.string,
    )
  },
  estimateSize: _mapBoxPlaceEstimateSize,
  serialize: _mapBoxPlaceSerialize,
  deserialize: _mapBoxPlaceDeserialize,
  deserializeProp: _mapBoxPlaceDeserializeProp,
  idName: r'isarId',
  indexes: {
    r'id': IndexSchema(
      id: -3268401673993471357,
      name: r'id',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'id',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'text': IndexSchema(
      id: 5145922347574273553,
      name: r'text',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'text',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'placeName': IndexSchema(
      id: 4623679150504479978,
      name: r'placeName',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'placeName',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {
    r'context': LinkSchema(
      id: 6526727140714017176,
      name: r'context',
      target: r'MapBoxPlaceContext',
      single: false,
    )
  },
  embeddedSchemas: {},
  getId: _mapBoxPlaceGetId,
  getLinks: _mapBoxPlaceGetLinks,
  attach: _mapBoxPlaceAttach,
  version: '3.0.5',
);

int _mapBoxPlaceEstimateSize(
  MapBoxPlace object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.id.length * 3;
  {
    final value = object.matchingPlaceName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.matchingText;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.placeName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.text;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _mapBoxPlaceSerialize(
  MapBoxPlace object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.id);
  writer.writeString(offsets[1], object.matchingPlaceName);
  writer.writeString(offsets[2], object.matchingText);
  writer.writeString(offsets[3], object.placeName);
  writer.writeString(offsets[4], object.text);
}

MapBoxPlace _mapBoxPlaceDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = MapBoxPlace();
  object.id = reader.readString(offsets[0]);
  object.matchingPlaceName = reader.readStringOrNull(offsets[1]);
  object.matchingText = reader.readStringOrNull(offsets[2]);
  object.placeName = reader.readStringOrNull(offsets[3]);
  object.text = reader.readStringOrNull(offsets[4]);
  return object;
}

P _mapBoxPlaceDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readString(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _mapBoxPlaceGetId(MapBoxPlace object) {
  return object.isarId;
}

List<IsarLinkBase<dynamic>> _mapBoxPlaceGetLinks(MapBoxPlace object) {
  return [object.context];
}

void _mapBoxPlaceAttach(
    IsarCollection<dynamic> col, Id id, MapBoxPlace object) {
  object.context
      .attach(col, col.isar.collection<MapBoxPlaceContext>(), r'context', id);
}

extension MapBoxPlaceByIndex on IsarCollection<MapBoxPlace> {
  Future<MapBoxPlace?> getById(String id) {
    return getByIndex(r'id', [id]);
  }

  MapBoxPlace? getByIdSync(String id) {
    return getByIndexSync(r'id', [id]);
  }

  Future<bool> deleteById(String id) {
    return deleteByIndex(r'id', [id]);
  }

  bool deleteByIdSync(String id) {
    return deleteByIndexSync(r'id', [id]);
  }

  Future<List<MapBoxPlace?>> getAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndex(r'id', values);
  }

  List<MapBoxPlace?> getAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'id', values);
  }

  Future<int> deleteAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'id', values);
  }

  int deleteAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'id', values);
  }

  Future<Id> putById(MapBoxPlace object) {
    return putByIndex(r'id', object);
  }

  Id putByIdSync(MapBoxPlace object, {bool saveLinks = true}) {
    return putByIndexSync(r'id', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllById(List<MapBoxPlace> objects) {
    return putAllByIndex(r'id', objects);
  }

  List<Id> putAllByIdSync(List<MapBoxPlace> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'id', objects, saveLinks: saveLinks);
  }
}

extension MapBoxPlaceQueryWhereSort
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QWhere> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhere> anyIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'id'),
      );
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhere> anyText() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'text'),
      );
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhere> anyPlaceName() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'placeName'),
      );
    });
  }
}

extension MapBoxPlaceQueryWhere
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QWhereClause> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> isarIdEqualTo(
      Id isarId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: isarId,
        upper: isarId,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> isarIdNotEqualTo(
      Id isarId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> isarIdGreaterThan(
      Id isarId,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: isarId, includeLower: include),
      );
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> isarIdLessThan(
      Id isarId,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: isarId, includeUpper: include),
      );
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> isarIdBetween(
    Id lowerIsarId,
    Id upperIsarId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerIsarId,
        includeLower: includeLower,
        upper: upperIsarId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [id],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idNotEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idGreaterThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [id],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idLessThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [],
        upper: [id],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idBetween(
    String lowerId,
    String upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [lowerId],
        includeLower: includeLower,
        upper: [upperId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idStartsWith(
      String IdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [IdPrefix],
        upper: ['$IdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [''],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'text',
        value: [null],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'text',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textEqualTo(
      String? text) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'text',
        value: [text],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textNotEqualTo(
      String? text) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'text',
              lower: [],
              upper: [text],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'text',
              lower: [text],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'text',
              lower: [text],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'text',
              lower: [],
              upper: [text],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textGreaterThan(
    String? text, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'text',
        lower: [text],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textLessThan(
    String? text, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'text',
        lower: [],
        upper: [text],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textBetween(
    String? lowerText,
    String? upperText, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'text',
        lower: [lowerText],
        includeLower: includeLower,
        upper: [upperText],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textStartsWith(
      String TextPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'text',
        lower: [TextPrefix],
        upper: ['$TextPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'text',
        value: [''],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> textIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'text',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'text',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'text',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'text',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'placeName',
        value: [null],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause>
      placeNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'placeName',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameEqualTo(
      String? placeName) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'placeName',
        value: [placeName],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameNotEqualTo(
      String? placeName) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'placeName',
              lower: [],
              upper: [placeName],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'placeName',
              lower: [placeName],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'placeName',
              lower: [placeName],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'placeName',
              lower: [],
              upper: [placeName],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause>
      placeNameGreaterThan(
    String? placeName, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'placeName',
        lower: [placeName],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameLessThan(
    String? placeName, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'placeName',
        lower: [],
        upper: [placeName],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameBetween(
    String? lowerPlaceName,
    String? upperPlaceName, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'placeName',
        lower: [lowerPlaceName],
        includeLower: includeLower,
        upper: [upperPlaceName],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameStartsWith(
      String PlaceNamePrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'placeName',
        lower: [PlaceNamePrefix],
        upper: ['$PlaceNamePrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause> placeNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'placeName',
        value: [''],
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterWhereClause>
      placeNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'placeName',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'placeName',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'placeName',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'placeName',
              upper: [''],
            ));
      }
    });
  }
}

extension MapBoxPlaceQueryFilter
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QFilterCondition> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'id',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> isarIdEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      isarIdGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> isarIdLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> isarIdBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'isarId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'matchingPlaceName',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'matchingPlaceName',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'matchingPlaceName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'matchingPlaceName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'matchingPlaceName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'matchingPlaceName',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingPlaceNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'matchingPlaceName',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'matchingText',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'matchingText',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'matchingText',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'matchingText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'matchingText',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'matchingText',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      matchingTextIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'matchingText',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'placeName',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'placeName',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'placeName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'placeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'placeName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'placeName',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      placeNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'placeName',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'text',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      textIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'text',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'text',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'text',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'text',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> textIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'text',
        value: '',
      ));
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      textIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'text',
        value: '',
      ));
    });
  }
}

extension MapBoxPlaceQueryObject
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QFilterCondition> {}

extension MapBoxPlaceQueryLinks
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QFilterCondition> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition> context(
      FilterQuery<MapBoxPlaceContext> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'context');
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'context', length, true, length, true);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'context', 0, true, 0, true);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'context', 0, false, 999999, true);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'context', 0, true, length, include);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'context', length, include, 999999, true);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterFilterCondition>
      contextLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'context', lower, includeLower, upper, includeUpper);
    });
  }
}

extension MapBoxPlaceQuerySortBy
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QSortBy> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      sortByMatchingPlaceName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingPlaceName', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      sortByMatchingPlaceNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingPlaceName', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByMatchingText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingText', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      sortByMatchingTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingText', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByPlaceName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'placeName', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByPlaceNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'placeName', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> sortByTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.desc);
    });
  }
}

extension MapBoxPlaceQuerySortThenBy
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QSortThenBy> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByIsarIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      thenByMatchingPlaceName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingPlaceName', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      thenByMatchingPlaceNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingPlaceName', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByMatchingText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingText', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy>
      thenByMatchingTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'matchingText', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByPlaceName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'placeName', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByPlaceNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'placeName', Sort.desc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.asc);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QAfterSortBy> thenByTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'text', Sort.desc);
    });
  }
}

extension MapBoxPlaceQueryWhereDistinct
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> {
  QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> distinctById(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'id', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> distinctByMatchingPlaceName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'matchingPlaceName',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> distinctByMatchingText(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'matchingText', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> distinctByPlaceName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'placeName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MapBoxPlace, MapBoxPlace, QDistinct> distinctByText(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'text', caseSensitive: caseSensitive);
    });
  }
}

extension MapBoxPlaceQueryProperty
    on QueryBuilder<MapBoxPlace, MapBoxPlace, QQueryProperty> {
  QueryBuilder<MapBoxPlace, int, QQueryOperations> isarIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isarId');
    });
  }

  QueryBuilder<MapBoxPlace, String, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<MapBoxPlace, String?, QQueryOperations>
      matchingPlaceNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'matchingPlaceName');
    });
  }

  QueryBuilder<MapBoxPlace, String?, QQueryOperations> matchingTextProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'matchingText');
    });
  }

  QueryBuilder<MapBoxPlace, String?, QQueryOperations> placeNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'placeName');
    });
  }

  QueryBuilder<MapBoxPlace, String?, QQueryOperations> textProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'text');
    });
  }
}
