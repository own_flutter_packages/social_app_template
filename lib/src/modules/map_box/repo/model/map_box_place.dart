import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'map_box_place.g.dart';

@Collection(accessor: 'mapBoxPlace')
class MapBoxPlace {
  Id get isarId => IsarDB.fastHash(id);

  @Index(type: IndexType.value, replace: true, unique: true)
  late String id;
  @Index(type: IndexType.value)
  String? text;
  @Index(type: IndexType.value)
  String? placeName;
  String? matchingText;
  String? matchingPlaceName;
  final IsarLinks<MapBoxPlaceContext> context = IsarLinks<MapBoxPlaceContext>();

  static of({
    required String id,
    String? text,
    String? placeName,
    String? matchingText,
    String? matchingPlaceName,
    List<MapBoxPlaceContext>? context,
  }) {
    return MapBoxPlace()
      ..id = id
      ..text = text
      ..placeName = placeName
      ..matchingText = matchingText
      ..matchingPlaceName = matchingPlaceName
      ..context.addAll(context ?? []);
  }
}
