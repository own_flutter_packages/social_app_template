import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'map_box_place_context.g.dart';

@Collection(accessor: 'mapBoxPlaceContext')
class MapBoxPlaceContext {
  Id get isarId => IsarDB.fastHash(id);

  MapBoxPlaceContext({
    required this.id,
    required this.mapBoxPlaceId,
    this.shortCode,
    this.wikiData,
    this.text,
  });

  @Index(type: IndexType.value, replace: true, unique: true)
  final String id;
  @Index(type: IndexType.value)
  final String mapBoxPlaceId;
  String? shortCode;
  String? wikiData;
  String? text;
}
