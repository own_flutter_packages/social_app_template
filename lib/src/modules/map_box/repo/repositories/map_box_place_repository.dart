import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage address objects.
class MapBoxPlaceRepository {
  /// When address is created also embedded objects are persisted and created.
  static Future<void> create(MapBoxPlace place) async {
    await IsarDB.isar
        .writeTxn(() => init(place: place, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static MapBoxPlace? get(String id) {
    return IsarDB.isar.mapBoxPlace.getSync(IsarDB.fastHash(id));
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<MapBoxPlace> getAll({bool withSort = false}) {
    return IsarDB.isar.mapBoxPlace.where().findAllSync();
  }

  /// Returns addresses by ids.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<MapBoxPlace?> getByIds(List<String> ids) {
    final isarIds = ids.map((id) => IsarDB.fastHash(id)).toSet().toList();

    return IsarDB.isar.mapBoxPlace.getAllSync(isarIds);
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(String id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.mapBoxPlace.deleteSync(IsarDB.fastHash(id)),
    );
  }

  /// Creates, updates and persists address and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init({
    MapBoxPlace? place,
    IsarLink<MapBoxPlace>? placeLink,
    bool withPut = true,
  }) async {
    if (placeLink != null && !placeLink.isChanged) return;

    final address = placeLink?.value ?? place;
    if (address == null) return;

    if (withPut) {
      await IsarDB.isar.mapBoxPlace
          .put(address)
          .onError(ErrorService.onErrorWithTraceInteger);
    }

    if (placeLink != null) {
      await placeLink.save().onError(ErrorService.onErrorWithTrace);
    }

    await MapBoxPlaceContextRepository.initAll(contextLink: address.context)
        .onError(ErrorService.onErrorWithTrace);
  }
}
