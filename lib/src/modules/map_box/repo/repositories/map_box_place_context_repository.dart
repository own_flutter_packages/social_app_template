import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place_context.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage address context objects.
class MapBoxPlaceContextRepository {
  /// Creates address context.
  static Future<void> create(MapBoxPlaceContext placeContext) async {
    await IsarDB.isar
        .writeTxn(() => init(placeContext))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Creates address contexts.
  static Future<void> createAll(List<MapBoxPlaceContext> placeContexts) async {
    await IsarDB.isar
        .writeTxn(() => initAll(context: placeContexts))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static MapBoxPlaceContext? get(String id) {
    return IsarDB.isar.mapBoxPlaceContext.getSync(IsarDB.fastHash(id));
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<MapBoxPlaceContext> getAll() {
    return IsarDB.isar.mapBoxPlaceContext.where().findAllSync();
  }

  /// Returns address contexts by ids.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<MapBoxPlaceContext?> getByIds(List<String> ids) {
    final isarIds = ids.map((id) => IsarDB.fastHash(id)).toSet().toList();
    return IsarDB.isar.mapBoxPlaceContext.getAllSync(isarIds);
  }

  /// Returns address contexts by address id.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<MapBoxPlaceContext> getByPlaceId(String placeId) {
    return IsarDB.isar.mapBoxPlaceContext
        .where()
        .mapBoxPlaceIdEqualTo(placeId)
        .findAllSync();
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(String id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.mapBoxPlaceContext.deleteSync(
        IsarDB.fastHash(id),
      ),
    );
  }

  /// Creates, updates and persists address context - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init(MapBoxPlaceContext context) async {
    await IsarDB.isar.mapBoxPlaceContext
        .put(context)
        .onError(ErrorService.onErrorWithTraceInteger);
  }

  /// Creates, updates and persists all of address contexts - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> initAll({
    List<MapBoxPlaceContext>? context,
    IsarLinks<MapBoxPlaceContext>? contextLink,
  }) async {
    if (contextLink == null && context == null) return;

    if (contextLink != null && contextLink.isChanged) {
      await contextLink.load().onError(ErrorService.onErrorWithTrace);
      await IsarDB.isar.mapBoxPlaceContext
          .putAll(contextLink.toList())
          .onError(ErrorService.onErrorWithTraceIterable);
      await contextLink.save().onError(ErrorService.onErrorWithTrace);
    } else if (context != null) {
      await IsarDB.isar.mapBoxPlaceContext
          .putAll(context)
          .onError(ErrorService.onErrorWithTraceIterable);
    }
  }
}
