part of social_app_template;

/// The main API interface of SocialAppInterface. Available through the `SocialApp` constant.
abstract class SocialAppTemplateInterface {
  /// Widget of post list scalable to infinite and paged by seven items.
  ///
  /// When user reaches the sixth item next items are loaded.
  /// The list loads the posts from database by provided ids.
  PlatformPostList postList({
    Key? key,
    List<String>? ids,
    bool shouldReloadProfileImage = false,
    Color? colorRefreshIndicator,
    Size? originalImageSizeProfileImages,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
    OnInitPost? onInitPost,
    OnTapPost? onTapPost,
    OnRefresh? onRefresh,
    OnSwipeLeft? onSwipeLeft,
    OnReloadProfileImage? onReloadProfileImage,
    OnChangedContentQuality? onChangedContentQuality,
    IsImageMarkedAsNotFound? isImageMarkedAsNotFound,
    MarkImageAsNotFound? markImageAsNotFound,
    List<CollectionSchema> furtherSchema,
  });

  /// Widget of post list that has a progress indicator.
  ///
  /// When user reaches the sixth item a progress bar is shown for loading.
  /// The list loads the posts from database by provided ids.
  /// Please specify progressAlignment to position the progress bar.
  PlatformPostListWithProgress postListWithProgress({
    Key? key,
    List<String>? ids,
    bool shouldReloadProfileImage = false,
    Color? colorRefreshIndicator,
    Alignment? progressAlignment,
    Color? progressMainColor,
    Color? progressBackgroundColor,
    Size? originalImageSizeProfileImages,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
    OnInitPost? onInitPost,
    OnTapPost? onTapPost,
    OnRefresh? onRefresh,
    OnSwipeLeft? onSwipeLeft,
    OnReloadProfileImage? onReloadProfileImage,
    OnChangedContentQuality? onChangedContentQuality,
    IsImageMarkedAsNotFound? isImageMarkedAsNotFound,
    MarkImageAsNotFound? markImageAsNotFound,
    List<CollectionSchema> furtherSchema,
  });

  /// Example of post list with preloaded objects in database.
  ///
  /// You can use this template for learning issues.
  PostsListWithExamples templateWithExample();
}
