import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_media.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage media objects.
class PlatformMediaRepository {
  /// Creates media.
  static Future<void> create(PlatformMedia media) async {
    await IsarDB.isar
        .writeTxn(() => init(media))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Creates multiple media.
  static Future<void> createAll(List<PlatformMedia> media) async {
    await IsarDB.isar
        .writeTxn(() => initAll(media: media))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static PlatformMedia? get(String id) {
    return IsarDB.isar.platformMedia.getSync(IsarDB.fastHash(id));
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<PlatformMedia> getAll() {
    return IsarDB.isar.platformMedia.where().findAllSync();
  }

  /// Returns list of media filtered by ids.
  ///
  /// Sync getAll call (should not be used in context with async create)
  ///
  /// When no ids are provided no database entries are considered
  static List<PlatformMedia?> getByIds(List<String> ids) {
    final isarIds = ids.map((id) => IsarDB.fastHash(id)).toSet().toList();

    return IsarDB.isar.platformMedia.getAllSync(isarIds);
  }

  /// Returns list of media filtered by ids.
  ///
  /// Async getAll call
  ///
  /// Id does not have to equal, contains is enough.
  static Future<List<PlatformMedia>> getAllByIdsContains(
    List<String> ids,
  ) async {
    return await IsarDB.isar.platformMedia
        .where()
        .filter()
        .anyOf(ids, (q, String id) => q.idContains(id))
        .findAll()
        .onError(ErrorService.onErrorWithTraceIterable);
  }

  /// Returns list of media filtered by parent id.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<PlatformMedia> getByParentId(String parentId) {
    return IsarDB.isar.platformMedia
        .where()
        .parentIdEqualTo(parentId)
        .findAllSync();
  }

  /// Returns list of media filtered by parent id.
  ///
  /// Async getAll call
  static Future<List<PlatformMedia>> getByParentIdAsync(String parentId) async {
    return await IsarDB.isar.platformMedia
        .where()
        .parentIdEqualTo(parentId)
        .findAll()
        .onError(ErrorService.onErrorWithTraceIterable);
  }

  /// Returns list of media filtered by author id.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<PlatformMedia> getByAuthorId(String authorId) {
    return IsarDB.isar.platformMedia
        .where()
        .authorIdEqualTo(authorId)
        .findAllSync();
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(String id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.platformMedia.deleteSync(
        IsarDB.fastHash(id),
      ),
    );
  }

  /// Creates, updates and persists media - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init(PlatformMedia media) async {
    await IsarDB.isar.platformMedia
        .put(media)
        .onError(ErrorService.onErrorWithTraceInteger);
  }

  /// Creates, updates and persists all of media - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> initAll({
    List<PlatformMedia>? media,
    IsarLinks<PlatformMedia>? mediaLink,
  }) async {
    if (mediaLink == null && media == null) return;

    if (mediaLink != null && mediaLink.isChanged) {
      await mediaLink.load().onError(ErrorService.onErrorWithTrace);
      await IsarDB.isar.platformMedia
          .putAll(mediaLink.toList())
          .onError(ErrorService.onErrorWithTraceIterable);
      await mediaLink.save().onError(ErrorService.onErrorWithTrace);
    } else if (media != null) {
      await IsarDB.isar.platformMedia
          .putAll(media)
          .onError(ErrorService.onErrorWithTraceIterable);
    }
  }
}
