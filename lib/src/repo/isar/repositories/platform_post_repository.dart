import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place.dart';
import 'package:social_app_template/src/repo/isar/model/geo/location_query.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post_entities.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_user.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage post objects.
class PlatformPostRepository {
  /// When post is created also embedded objects are persisted and created.
  static Future<void> create(PlatformPost post) async {
    await IsarDB.isar
        .writeTxn(() => _init(post, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// When posts are created also embedded objects are persisted and created.
  static Future<void> createAll(List<PlatformPost> posts) async {
    await IsarDB.isar
        .writeTxn(() => _initAll(posts))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Checks if post exists in database.
  static bool exists(String id) {
    return get(id) != null;
  }

  /// Sync get call (should not be used in context with async create).
  static PlatformPost? get(String id) {
    return IsarDB.isar.platformPosts.getSync(IsarDB.fastHash(id));
  }

  /// Async get call.
  static Future<PlatformPost?> getAsync(String id) async {
    return await IsarDB.isar.platformPosts
        .get(IsarDB.fastHash(id))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Counts all posts that exist in database.
  static int countAll() {
    return IsarDB.isar.platformPosts.countSync();
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<PlatformPost> getAll({bool? sort = false}) {
    return sort ?? false
        ? IsarDB.isar.platformPosts.where().sortById().findAllSync()
        : IsarDB.isar.platformPosts.where().findAllSync();
  }

  /// Returns posts filtered by conversation id.
  ///
  /// sync getAll call (should not be used in context with async create)
  static List<PlatformPost> getByConversationId(String id) {
    return IsarDB.isar.platformPosts
        .where()
        .conversationIdEqualTo(id)
        .findAllSync();
  }

  /// Async getAll call.
  ///
  /// If ids are not provided all posts are returned.
  static Future<List<PlatformPost?>> getByIds(List<String> ids) async {
    if (ids.isEmpty) {
      return await IsarDB.isar.platformPosts
          .where()
          .findAll()
          .onError(ErrorService.onErrorWithTraceIterable);
    }

    final isarIds = List.of(ids).map((id) => IsarDB.fastHash(id)).toList();
    return await IsarDB.isar.platformPosts
        .getAll(isarIds)
        .onError(ErrorService.onErrorWithTraceIterable);
  }

  /// Returns posts filtered by author id.
  ///
  /// sync getAll (should not be used in context with async create)
  static List<PlatformPost> getByUserId(String userId) {
    return IsarDB.isar.platformPosts
        .where()
        .userIdEqualTo(userId)
        .findAllSync();
  }

  /// Returns posts filtered by author id.
  static Future<List<PlatformPost>> getByUserIdAsync(String userId) async {
    return await IsarDB.isar.platformPosts
        .where()
        .userIdEqualTo(userId)
        .findAll()
        .onError(ErrorService.onErrorWithTraceIterable);
  }

  /// Returns posts filtered by author ids.
  ///
  /// sync getAll (should not be used in context with async create)
  static List<PlatformPost> getByUserIds(List<String> userIds) {
    return IsarDB.isar.platformPosts
        .where()
        .anyOf(userIds, (q, String element) => q.userIdEqualTo(element))
        .sortByCreatedAtDesc()
        .findAllSync();
  }

  /// Creates, updates and persists post and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> _init(
    PlatformPost? post, {
    withPut = true,
  }) async {
    if (post == null) return;

    await IsarDB.isar.platformPosts
        .put(post)
        .onError(ErrorService.onErrorWithTraceInteger);

    await PlatformUserRepository.init(
      userLink: post.user,
      withPut: withPut,
    ).onError(ErrorService.onErrorWithTraceIterable);

    await PlatformPostEntitiesRepository.init(
      entitiesLink: post.entities,
      withPut: withPut,
    ).onError(ErrorService.onErrorWithTraceIterable);
  }

  /// Creates, updates and persists all of posts and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> _initAll(List<PlatformPost> posts) async {
    if (posts.isEmpty) return;

    await IsarDB.isar.platformPosts.putAll(posts).onError(
          ErrorService.onErrorWithTraceIterable,
        );

    final users = posts
        .where((e) => e.user.value != null)
        .map((e) => e.user.value!)
        .toList();

    final userLocations = posts
        .where((e) => e.user.value?.location.value != null)
        .map((e) => e.user.value!.location)
        .where((e) => e.isChanged)
        .map((e) => e.value!)
        .toList();

    final userLocationAddresses = userLocations
        .where((e) => e.address.value != null)
        .map((e) => e.address)
        .where((e) => e.isChanged)
        .map((e) => e.value!)
        .toList();

    final entities = posts
        .where((e) => e.entities.value != null)
        .map((e) => e.entities)
        .where((e) => e.isChanged)
        .map((e) => e.value!)
        .toList();

    await IsarDB.isar.platformUsers
        .putAll(users)
        .onError(ErrorService.onErrorWithTraceIterable);

    await IsarDB.isar.locationQuery
        .putAll(userLocations)
        .onError(ErrorService.onErrorWithTraceIterable);

    await IsarDB.isar.mapBoxPlace
        .putAll(userLocationAddresses)
        .onError(ErrorService.onErrorWithTraceIterable);

    await IsarDB.isar.platformPostEntities
        .putAll(entities)
        .onError(ErrorService.onErrorWithTraceIterable);

    await Future.forEach(
      posts,
      (PlatformPost post) async {
        await _init(post, withPut: false).onError(
          ErrorService.onErrorWithTraceIterable,
        );
      },
    );
  }
}
