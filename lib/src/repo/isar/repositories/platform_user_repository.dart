import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_user.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage user objects.
class PlatformUserRepository {
  /// When user is created also embedded objects are persisted and created.
  static Future<void> create(PlatformUser user) async {
    await IsarDB.isar
        .writeTxn(() => init(user: user, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// When users are created also embedded objects are persisted and created.
  static Future<void> createAll(List<PlatformUser> users) async {
    await IsarDB.isar
        .writeTxn(() => initAll(users: users))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static PlatformUser? get(String id) {
    return IsarDB.isar.platformUsers.getSync(IsarDB.fastHash(id));
  }

  /// Async get call.
  static Future<PlatformUser?> getAsync(String id) async {
    return await IsarDB.isar.platformUsers
        .get(IsarDB.fastHash(id))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<PlatformUser> getAll() {
    return IsarDB.isar.platformUsers.where().findAllSync();
  }

  /// Returns list of ids of filtered posts.
  ///
  /// Async getAll call
  ///
  /// When no ids are provided all database entries are considered
  /// When preferredMinAge is provided database entries are filtered by age
  /// When preferredMaxAge is provided database entries are filtered by age
  ///
  /// When radiusInMeter, latitude and longitude are provided database entries are filtered by distance
  static Future<List<String>> getAllByAsIds({
    List<String>? ids,
    int? preferredMinAge,
    int? preferredMaxAge,
    double? latitude,
    double? longitude,
    int? radiusInMeter,
  }) async {
    final users = await getAllBy(
      ids: ids,
      preferredMinAge: preferredMinAge,
      preferredMaxAge: preferredMaxAge,
      latitude: latitude,
      longitude: longitude,
      radiusInMeter: radiusInMeter,
    ).onError(ErrorService.onErrorWithTraceIterable);

    return List.of(users).where((e) => e != null).map((e) => e!.id).toList();
  }

  /// Returns list of ids of filtered posts.
  ///
  /// Sync getAll call (should not be used in context with async create)
  ///
  /// When no ids are provided all database entries are considered
  /// When preferredMinAge is provided database entries are filtered by age
  /// When preferredMaxAge is provided database entries are filtered by age
  ///
  /// When radiusInMeter, latitude and longitude are provided database entries are filtered by distance
  static Future<List<PlatformUser?>> getAllBy({
    List<String>? ids,
    int? preferredMinAge,
    int? preferredMaxAge,
    double? latitude,
    double? longitude,
    int? radiusInMeter,
  }) async {
    final isSetIds = ids?.isNotEmpty ?? false;
    final isSetAge = preferredMinAge != null && preferredMaxAge != null;
    final isSetRadius = radiusInMeter != null;

    double? latMaxNorth;
    double? lonMaxEast;
    double? latMaxSouth;
    double? lonMaxWest;

    if (latitude != null && longitude != null && isSetRadius) {
      final bounds = SizeService.getMapBounds(
        latitude,
        longitude,
        radiusInMeter!,
      );
      latMaxNorth = bounds['latMaxNorth'];
      lonMaxEast = bounds['lonMaxEast'];
      latMaxSouth = bounds['latMaxSouth'];
      lonMaxWest = bounds['lonMaxWest'];
    }

    final isSetLocation = latMaxNorth != null &&
        lonMaxEast != null &&
        latMaxSouth != null &&
        lonMaxWest != null;

    List<PlatformUser?>? users = await IsarDB.isar.txn(() {
      if (isSetLocation && isSetIds && isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .latitudeBetween(latMaxSouth, latMaxNorth)
            .longitudeBetween(lonMaxWest, lonMaxEast)
            .ageBetween(preferredMinAge, preferredMaxAge)
            .anyOf(ids!, (q, String id) => q.idEqualTo(id))
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (isSetLocation && !isSetIds && isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .latitudeBetween(latMaxSouth, latMaxNorth)
            .longitudeBetween(lonMaxWest, lonMaxEast)
            .ageBetween(preferredMinAge, preferredMaxAge)
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (isSetLocation && !isSetIds && !isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .latitudeBetween(latMaxSouth, latMaxNorth)
            .longitudeBetween(lonMaxWest, lonMaxEast)
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (isSetLocation && isSetIds && !isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .latitudeBetween(latMaxSouth, latMaxNorth)
            .longitudeBetween(lonMaxWest, lonMaxEast)
            .anyOf(ids!, (q, String id) => q.idEqualTo(id))
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (!isSetLocation && isSetIds && isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .ageBetween(preferredMinAge, preferredMaxAge)
            .anyOf(ids!, (q, String id) => q.idEqualTo(id))
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (!isSetLocation && !isSetIds && isSetAge) {
        return IsarDB.isar.platformUsers
            .where()
            .filter()
            .ageBetween(preferredMinAge, preferredMaxAge)
            .findAll()
            .onError(ErrorService.onErrorWithTraceIterable);
      } else if (!isSetLocation && isSetIds && !isSetAge) {
        final isarIds = ids!.map((id) => IsarDB.fastHash(id)).toSet().toList();
        return IsarDB.isar.platformUsers
            .getAll(isarIds)
            .onError(ErrorService.onErrorWithTraceIterable);
      }

      return IsarDB.isar.platformUsers
          .where()
          .findAll()
          .onError(ErrorService.onErrorWithTraceIterable);
    });

    return users != null ? List.of(users) : [];
  }

  /// Deletes user by id.
  static delete(String id) {
    IsarDB.isar
        .writeTxnSync(
          () => IsarDB.isar.platformUsers.delete(IsarDB.fastHash(id)),
        )
        .onError(ErrorService.onErrorWithTraceBool);
  }

  /// Creates, updates and persists user and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init({
    PlatformUser? user,
    IsarLink<PlatformUser>? userLink,
    bool withPut = true,
  }) async {
    if (userLink != null && !userLink.isChanged) return;

    final userValue = userLink?.value ?? user;
    if (userValue == null) return;

    if (withPut) {
      await IsarDB.isar.platformUsers
          .put(userValue)
          .onError(ErrorService.onErrorWithTraceInteger);
    }

    if (userLink != null) {
      await userLink.save().onError(ErrorService.onErrorWithTrace);
    }

    await LocationQueryRepository.init(locationLink: userValue.location)
        .onError(ErrorService.onErrorWithTrace);
    await FocusRepository.initAll(focusesLink: userValue.focuses)
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Creates, updates and persists all of users and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> initAll({
    List<PlatformUser>? users,
    IsarLinks<PlatformUser>? usersLink,
  }) async {
    if (usersLink == null && users == null) return;

    if (usersLink != null && usersLink.isChanged) {
      await usersLink.load().onError(ErrorService.onErrorWithTrace);
      final userMentions = usersLink.toList();
      await IsarDB.isar.platformUsers
          .putAll(usersLink.toList())
          .onError(ErrorService.onErrorWithTraceIterable);

      await Future.forEach(userMentions, (PlatformUser userMention) async {
        await LocationQueryRepository.init(locationLink: userMention.location)
            .onError(ErrorService.onErrorWithTrace);
        await FocusRepository.initAll(focusesLink: userMention.focuses)
            .onError(ErrorService.onErrorWithTrace);
      }).onError(ErrorService.onErrorWithTrace);

      await usersLink.save().onError(ErrorService.onErrorWithTrace);
    } else if (users != null) {
      await IsarDB.isar.platformUsers
          .putAll(users)
          .onError(ErrorService.onErrorWithTraceIterable);

      await Future.forEach(users, (PlatformUser userMention) async {
        await LocationQueryRepository.init(locationLink: userMention.location)
            .onError(ErrorService.onErrorWithTrace);
        await FocusRepository.initAll(focusesLink: userMention.focuses)
            .onError(ErrorService.onErrorWithTrace);
      }).onError(ErrorService.onErrorWithTrace);
    }
  }
}
