import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/focus/focus_metrics.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage focus metrics objects.
class FocusMetricsRepository {
  /// Creates focus metrics.
  static Future<void> create(FocusMetrics metrics) async {
    await IsarDB.isar
        .writeTxn(() => init(metrics))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Creates multiple focus metrics.
  static Future<void> createAll(List<FocusMetrics> metrics) async {
    await IsarDB.isar
        .writeTxn(() => initAll(metrics: metrics))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static FocusMetrics? get(String id) {
    return IsarDB.isar.focusMetrics.getSync(IsarDB.fastHash(id));
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<FocusMetrics> getAll() {
    return IsarDB.isar.focusMetrics.where().findAllSync();
  }

  /// Returns focuses by ids.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<FocusMetrics?> getByIds(List<String> ids) {
    final isarIds = ids.map((id) => IsarDB.fastHash(id)).toSet().toList();

    return IsarDB.isar.focusMetrics.getAllSync(isarIds);
  }

  /// Returns focuses by focus id.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<FocusMetrics> getAllByFocusId(String focusId) {
    return IsarDB.isar.focusMetrics
        .where()
        .focusIdEqualTo(focusId)
        .findAllSync();
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(String id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.focusMetrics.deleteSync(
        IsarDB.fastHash(id),
      ),
    );
  }

  /// Creates, updates and persists focus metrics - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init(FocusMetrics metrics) async {
    await IsarDB.isar.focusMetrics
        .put(metrics)
        .onError(ErrorService.onErrorWithTraceInteger);
  }

  /// Creates, updates and persists all of focus metrics - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> initAll({
    List<FocusMetrics>? metrics,
    IsarLinks<Focus>? focusesLink,
  }) async {
    if (focusesLink == null && metrics == null) return;

    if (focusesLink != null) {
      final metricsOfFocusesLink = focusesLink
          .toList()
          .where((e) => e.metrics.value != null)
          .where((e) => e.metrics.isChanged)
          .map((e) => e.metrics)
          .toList();

      await IsarDB.isar.focusMetrics
          .putAll(
            metricsOfFocusesLink.map((e) => e.value!).toList(),
          )
          .onError(ErrorService.onErrorWithTraceIterable);

      await Future.forEach(
        metricsOfFocusesLink,
        (IsarLink<FocusMetrics> metric) async => await metric.save().onError(
              ErrorService.onErrorWithTrace,
            ),
      ).onError(ErrorService.onErrorWithTrace);
    } else {
      await IsarDB.isar.focusMetrics
          .putAll(metrics!)
          .onError(ErrorService.onErrorWithTraceIterable);
    }
  }
}
