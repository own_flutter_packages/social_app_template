import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/focus/focus.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage user objects.
class FocusRepository {
  /// When focus is created also embedded objects are persisted and created.
  static Future<void> create(Focus focus) async {
    await IsarDB.isar
        .writeTxn(() => init(focus: focus, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// When focuses are created also embedded objects are persisted and created.
  static Future<void> createAll(List<Focus> focuses) async {
    await IsarDB.isar
        .writeTxn(() => initAll(focuses: focuses))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static Focus? get(String id) {
    return IsarDB.isar.focus.getSync(IsarDB.fastHash(id));
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<Focus> getAll({bool withSort = false}) {
    return withSort
        ? IsarDB.isar.focus.where().sortByCountDesc().findAllSync()
        : IsarDB.isar.focus.where().findAllSync();
  }

  /// Returns focuses by ids.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus?> getByIds(List<String> ids) {
    return IsarDB.isar.focus.getAllSync(
      ids.map((e) => IsarDB.fastHash(e)).toList(),
    );
  }

  /// Returns focuses by its name and value.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus> getAllByNameAndValue(String name, String value) {
    return IsarDB.isar.focus
        .where()
        .nameEqualTo(name)
        .filter()
        .valueEqualTo(value)
        .findAllSync();
  }

  /// Returns focuses by user id.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus> getAllByUser(String userId, {bool sort = true}) {
    final user = PlatformUserRepository.get(userId);

    List<Focus> userFocuses = user?.focuses.toList() ?? [];

    if (sort) {
      userFocuses.sort((e1, e2) {
        var e1Count = e1.metrics.value?.count;
        var e2Count = e2.metrics.value?.count;

        return (e1Count != null && e2Count != null)
            ? e1Count.compareTo(e2Count)
            : 0;
      });
    }

    return userFocuses;
  }

  /// Returns focuses by parent id.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus> getAllByParent(String parentId) {
    return IsarDB.isar.focus
        .where()
        .connectedFocusValueEqualTo(parentId)
        .findAllSync();
  }

  /// Returns focuses by its name.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus> getAllByName(String name, {bool sort = false}) {
    return sort
        ? IsarDB.isar.focus
            .where()
            .nameEqualTo(name)
            .sortByCountDesc()
            .findAllSync()
        : IsarDB.isar.focus.where().nameEqualTo(name).findAllSync();
  }

  /// Returns focuses by its value.
  ///
  /// Sync get call (should not be used in context with async create)
  static List<Focus> getAllByValue(String value, {bool sort = false}) {
    return sort
        ? IsarDB.isar.focus
            .where()
            .valueEqualTo(value)
            .sortByCountDesc()
            .findAllSync()
        : IsarDB.isar.focus.where().valueEqualTo(value).findAllSync();
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(String id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.focus.deleteSync(IsarDB.fastHash(id)),
    );
  }

  /// Creates, updates and persists focus and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init({
    Focus? focus,
    IsarLink<Focus>? focusLink,
    bool withPut = true,
  }) async {
    if (focusLink != null && !focusLink.isChanged) return;

    final focusValue = focusLink?.value ?? focus;
    if (focusValue == null) return;

    if (withPut) {
      await IsarDB.isar.focus
          .put(focusValue)
          .onError(ErrorService.onErrorWithTraceInteger);
    }

    if (focusLink != null) {
      await focusLink.save().onError(ErrorService.onErrorWithTrace);
    }
  }

  /// Creates, updates and persists all of focuses and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> initAll({
    List<Focus>? focuses,
    IsarLinks<Focus>? focusesLink,
  }) async {
    if (focusesLink == null && focuses == null) return;

    if (focusesLink != null && focusesLink.isChanged) {
      await focusesLink.load().onError(ErrorService.onErrorWithTrace);
      await IsarDB.isar.focus
          .putAll(focusesLink.toList())
          .onError(ErrorService.onErrorWithTraceIterable);
      await focusesLink.save().onError(ErrorService.onErrorWithTrace);

      await FocusMetricsRepository.initAll(focusesLink: focusesLink)
          .onError(ErrorService.onErrorWithTrace);
    }
  }
}
