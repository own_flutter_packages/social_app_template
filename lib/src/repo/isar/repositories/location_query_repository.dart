import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/geo/location_query.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to manage location objects.
class LocationQueryRepository {
  /// When location is created also embedded objects are persisted and created.
  static Future<void> create(LocationQuery location) async {
    await IsarDB.isar
        .writeTxn(() => init(location: location, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static LocationQuery? get(int id) {
    return IsarDB.isar.locationQuery.getSync(id);
  }

  /// Returns location by query.
  ///
  /// Sync get call (should not be used in context with async create)
  static LocationQuery? getByQuery(String query) {
    return IsarDB.isar.locationQuery
        .where()
        .filter()
        .queryEqualTo(query)
        .findFirstSync();
  }

  /// Returns location by query.
  ///
  /// Async get call
  static Future<LocationQuery?> getByQueryAsync(String query) async {
    return await IsarDB.isar.locationQuery
        .where()
        .filter()
        .queryEqualTo(query)
        .findFirst()
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Returns location by latitude and longitude.
  ///
  /// Sync get call (should not be used in context with async create)
  static LocationQuery? getByLatLng(double latitude, double longitude) {
    return IsarDB.isar.locationQuery
        .where()
        .latitudeEqualTo(latitude)
        .filter()
        .longitudeEqualTo(longitude)
        .findFirstSync();
  }

  /// Returns location by latitude and longitude.
  ///
  /// Async get call
  static Future<LocationQuery?> getByLatLngAsync(
    double latitude,
    double longitude,
  ) async {
    return await IsarDB.isar.locationQuery
        .where()
        .latitudeEqualTo(latitude)
        .filter()
        .longitudeEqualTo(longitude)
        .findFirst()
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Counts all locations in database.
  static int countAll() {
    return IsarDB.isar.locationQuery.countSync();
  }

  /// Returns list of all locations.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<LocationQuery> getAll() {
    return IsarDB.isar.locationQuery.where().findAllSync();
  }

  /// Returns list of locations by ids.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<LocationQuery?> getByIds(List<int> ids) {
    return IsarDB.isar.locationQuery.getAllSync(ids);
  }

  /// Returns list of locations by queries.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<LocationQuery> getByQueries(List<String> queries) {
    return IsarDB.isar.locationQuery
        .where()
        .queryIsNotEmpty()
        .filter()
        .anyOf(queries, (q, String query) => q.queryEqualTo(query))
        .findAllSync();
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(int id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.locationQuery.deleteSync(
        id,
      ),
    );
  }

  /// Creates, updates and persists location and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init({
    LocationQuery? location,
    IsarLink<LocationQuery>? locationLink,
    bool withPut = true,
  }) async {
    if (locationLink != null && !locationLink.isChanged) return;

    final locationValue = locationLink?.value ?? location;

    if (locationValue == null) return;

    if (withPut) {
      await IsarDB.isar.locationQuery
          .put(locationValue)
          .onError(ErrorService.onErrorWithTraceInteger);
    }

    if (locationLink != null) {
      await locationLink.save().onError(ErrorService.onErrorWithTrace);
    }

    await MapBoxPlaceRepository.init(
      placeLink: locationValue.address,
      withPut: withPut,
    ).onError(ErrorService.onErrorWithTrace);
  }
}
