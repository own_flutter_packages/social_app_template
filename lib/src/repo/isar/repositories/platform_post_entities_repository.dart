import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post_entities.dart';
import 'package:social_app_template/src/services/error_service.dart';

/// Repository to post entities objects.
class PlatformPostEntitiesRepository {
  /// When entities is created also embedded objects are persisted and created.
  static Future<void> createAsync(PlatformPostEntities entities) async {
    await IsarDB.isar
        .writeTxn(() => init(entities: entities, withPut: true))
        .onError(ErrorService.onErrorWithTrace);
  }

  /// Sync get call (should not be used in context with async create).
  static PlatformPostEntities? get(int id) {
    return IsarDB.isar.platformPostEntities.getSync(id);
  }

  /// Sync getAll call (should not be used in context with async create).
  static List<PlatformPostEntities> getAll() {
    return IsarDB.isar.platformPostEntities.where().findAllSync();
  }

  /// Returns list of post entities by ids.
  ///
  /// Sync getAll call (should not be used in context with async create)
  static List<PlatformPostEntities?> getByIds(List<int> ids) {
    return IsarDB.isar.platformPostEntities.getAllSync(ids);
  }

  /// Sync delete call (should not be used in context with async create).
  static delete(int id) {
    IsarDB.isar.writeTxnSync(
      () => IsarDB.isar.platformPostEntities.deleteSync(id),
    );
  }

  /// Async delete call.
  static Future<void> deleteAsync(int id) async {
    await IsarDB.isar
        .writeTxn(
          () => IsarDB.isar.platformPostEntities.delete(id),
        )
        .onError(ErrorService.onErrorWithTraceBool);
  }

  /// Creates, updates and persists post entities and its embedded objects - has to be used in IsarDB.isar.writeTxt transaction.
  static Future<void> init({
    PlatformPostEntities? entities,
    IsarLink<PlatformPostEntities>? entitiesLink,
    bool withPut = true,
  }) async {
    if (entitiesLink != null && !entitiesLink.isChanged) return;

    final entitiesValue = entitiesLink?.value ?? entities;

    if (entitiesValue == null) return;

    if (withPut) {
      await IsarDB.isar.platformPostEntities
          .put(entitiesValue)
          .onError(ErrorService.onErrorWithTraceInteger);
    }

    if (entitiesLink != null) {
      await entitiesLink.save().onError(ErrorService.onErrorWithTrace);
    }

    await PlatformMediaRepository.initAll(
      mediaLink: entitiesValue.media,
    ).onError(ErrorService.onErrorWithTrace);

    await PlatformUserRepository.initAll(
      usersLink: entitiesValue.userMentions,
    ).onError(ErrorService.onErrorWithTrace);
  }
}
