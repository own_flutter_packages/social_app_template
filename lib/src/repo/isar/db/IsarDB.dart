import 'package:isar/isar.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place_context.dart';
import 'package:social_app_template/src/modules/social_media/twitter/repo/model/twitter_context_entity.dart';
import 'package:social_app_template/src/repo/isar/model/focus/focus.dart';
import 'package:social_app_template/src/repo/isar/model/focus/focus_metrics.dart';
import 'package:social_app_template/src/repo/isar/model/geo/location_query.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_media.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_post_entities.dart';
import 'package:social_app_template/src/repo/isar/model/platform/platform_user.dart';

class IsarDB {
  static Isar? _isar;

  static Isar get isar => _isarInstance();

  static _isarInstance({List<CollectionSchema>? furtherSchema}) {
    _isar ??= newInstance(furtherSchema: furtherSchema);
    return _isar;
  }

  static Isar newInstance({List<CollectionSchema>? furtherSchema}) =>
      Isar.openSync(
        [
          LocationQuerySchema,
          PlatformUserSchema,
          PlatformPostSchema,
          PlatformPostEntitiesSchema,
          PlatformMediaSchema,
          FocusSchema,
          FocusMetricsSchema,
          MapBoxPlaceSchema,
          MapBoxPlaceContextSchema,
          TwitterContextEntitySchema,
          ...(furtherSchema ?? []),
        ],
      );

  init({List<CollectionSchema>? furtherSchema}) {
    _isar = _isarInstance(furtherSchema: furtherSchema);
  }

  /// FNV-1a 64bit hash algorithm optimized for Dart Strings
  static int fastHash(String string) {
    var hash = 0xcbf29ce484222325;

    var i = 0;
    while (i < string.length) {
      final codeUnit = string.codeUnitAt(i++);
      hash ^= codeUnit >> 8;
      hash *= 0x100000001b3;
      hash ^= codeUnit & 0xFF;
      hash *= 0x100000001b3;
    }

    return hash;
  }
}
