import 'package:flutter/foundation.dart';

class BlobImage {
  BlobImage({
    required this.id,
    this.hash,
    this.imageString,
    this.byteData,
  });

  int id;
  String? hash;
  String? imageString;
  Uint8List? byteData;
}
