// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'platform_media.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetPlatformMediaCollection on Isar {
  IsarCollection<PlatformMedia> get platformMedia => this.collection();
}

const PlatformMediaSchema = CollectionSchema(
  name: r'PlatformMedia',
  id: 6287989640045467527,
  properties: {
    r'authorId': PropertySchema(
      id: 0,
      name: r'authorId',
      type: IsarType.string,
    ),
    r'createdTimestamp': PropertySchema(
      id: 1,
      name: r'createdTimestamp',
      type: IsarType.dateTime,
    ),
    r'height': PropertySchema(
      id: 2,
      name: r'height',
      type: IsarType.double,
    ),
    r'id': PropertySchema(
      id: 3,
      name: r'id',
      type: IsarType.string,
    ),
    r'isMain': PropertySchema(
      id: 4,
      name: r'isMain',
      type: IsarType.bool,
    ),
    r'isVideo': PropertySchema(
      id: 5,
      name: r'isVideo',
      type: IsarType.bool,
    ),
    r'lastEditedTimestamp': PropertySchema(
      id: 6,
      name: r'lastEditedTimestamp',
      type: IsarType.dateTime,
    ),
    r'loadingTimestamp': PropertySchema(
      id: 7,
      name: r'loadingTimestamp',
      type: IsarType.dateTime,
    ),
    r'parentId': PropertySchema(
      id: 8,
      name: r'parentId',
      type: IsarType.string,
    ),
    r'parentType': PropertySchema(
      id: 9,
      name: r'parentType',
      type: IsarType.string,
    ),
    r'platform': PropertySchema(
      id: 10,
      name: r'platform',
      type: IsarType.string,
    ),
    r'previewUrl': PropertySchema(
      id: 11,
      name: r'previewUrl',
      type: IsarType.string,
    ),
    r'url': PropertySchema(
      id: 12,
      name: r'url',
      type: IsarType.string,
    ),
    r'width': PropertySchema(
      id: 13,
      name: r'width',
      type: IsarType.double,
    )
  },
  estimateSize: _platformMediaEstimateSize,
  serialize: _platformMediaSerialize,
  deserialize: _platformMediaDeserialize,
  deserializeProp: _platformMediaDeserializeProp,
  idName: r'isarId',
  indexes: {
    r'id': IndexSchema(
      id: -3268401673993471357,
      name: r'id',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'id',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'parentId': IndexSchema(
      id: -809199838039056779,
      name: r'parentId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'parentId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'authorId': IndexSchema(
      id: 8112877077417469315,
      name: r'authorId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'authorId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    ),
    r'isVideo': IndexSchema(
      id: 5063265645062760613,
      name: r'isVideo',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'isVideo',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _platformMediaGetId,
  getLinks: _platformMediaGetLinks,
  attach: _platformMediaAttach,
  version: '3.0.5',
);

int _platformMediaEstimateSize(
  PlatformMedia object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.authorId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  bytesCount += 3 + object.id.length * 3;
  {
    final value = object.parentId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.parentType;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.platform;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.previewUrl;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.url;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _platformMediaSerialize(
  PlatformMedia object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.authorId);
  writer.writeDateTime(offsets[1], object.createdTimestamp);
  writer.writeDouble(offsets[2], object.height);
  writer.writeString(offsets[3], object.id);
  writer.writeBool(offsets[4], object.isMain);
  writer.writeBool(offsets[5], object.isVideo);
  writer.writeDateTime(offsets[6], object.lastEditedTimestamp);
  writer.writeDateTime(offsets[7], object.loadingTimestamp);
  writer.writeString(offsets[8], object.parentId);
  writer.writeString(offsets[9], object.parentType);
  writer.writeString(offsets[10], object.platform);
  writer.writeString(offsets[11], object.previewUrl);
  writer.writeString(offsets[12], object.url);
  writer.writeDouble(offsets[13], object.width);
}

PlatformMedia _platformMediaDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = PlatformMedia(
    authorId: reader.readStringOrNull(offsets[0]),
    createdTimestamp: reader.readDateTimeOrNull(offsets[1]),
    height: reader.readDoubleOrNull(offsets[2]),
    id: reader.readString(offsets[3]),
    isMain: reader.readBoolOrNull(offsets[4]),
    isVideo: reader.readBoolOrNull(offsets[5]),
    lastEditedTimestamp: reader.readDateTimeOrNull(offsets[6]),
    loadingTimestamp: reader.readDateTimeOrNull(offsets[7]),
    parentId: reader.readStringOrNull(offsets[8]),
    parentType: reader.readStringOrNull(offsets[9]),
    platform: reader.readStringOrNull(offsets[10]),
    previewUrl: reader.readStringOrNull(offsets[11]),
    url: reader.readStringOrNull(offsets[12]),
    width: reader.readDoubleOrNull(offsets[13]),
  );
  return object;
}

P _platformMediaDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 2:
      return (reader.readDoubleOrNull(offset)) as P;
    case 3:
      return (reader.readString(offset)) as P;
    case 4:
      return (reader.readBoolOrNull(offset)) as P;
    case 5:
      return (reader.readBoolOrNull(offset)) as P;
    case 6:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 7:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 8:
      return (reader.readStringOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readStringOrNull(offset)) as P;
    case 11:
      return (reader.readStringOrNull(offset)) as P;
    case 12:
      return (reader.readStringOrNull(offset)) as P;
    case 13:
      return (reader.readDoubleOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _platformMediaGetId(PlatformMedia object) {
  return object.isarId;
}

List<IsarLinkBase<dynamic>> _platformMediaGetLinks(PlatformMedia object) {
  return [];
}

void _platformMediaAttach(
    IsarCollection<dynamic> col, Id id, PlatformMedia object) {}

extension PlatformMediaByIndex on IsarCollection<PlatformMedia> {
  Future<PlatformMedia?> getById(String id) {
    return getByIndex(r'id', [id]);
  }

  PlatformMedia? getByIdSync(String id) {
    return getByIndexSync(r'id', [id]);
  }

  Future<bool> deleteById(String id) {
    return deleteByIndex(r'id', [id]);
  }

  bool deleteByIdSync(String id) {
    return deleteByIndexSync(r'id', [id]);
  }

  Future<List<PlatformMedia?>> getAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndex(r'id', values);
  }

  List<PlatformMedia?> getAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'id', values);
  }

  Future<int> deleteAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'id', values);
  }

  int deleteAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'id', values);
  }

  Future<Id> putById(PlatformMedia object) {
    return putByIndex(r'id', object);
  }

  Id putByIdSync(PlatformMedia object, {bool saveLinks = true}) {
    return putByIndexSync(r'id', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllById(List<PlatformMedia> objects) {
    return putAllByIndex(r'id', objects);
  }

  List<Id> putAllByIdSync(List<PlatformMedia> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'id', objects, saveLinks: saveLinks);
  }
}

extension PlatformMediaQueryWhereSort
    on QueryBuilder<PlatformMedia, PlatformMedia, QWhere> {
  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhere> anyIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'id'),
      );
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhere> anyParentId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'parentId'),
      );
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhere> anyAuthorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'authorId'),
      );
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhere> anyIsVideo() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'isVideo'),
      );
    });
  }
}

extension PlatformMediaQueryWhere
    on QueryBuilder<PlatformMedia, PlatformMedia, QWhereClause> {
  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> isarIdEqualTo(
      Id isarId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: isarId,
        upper: isarId,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      isarIdNotEqualTo(Id isarId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      isarIdGreaterThan(Id isarId, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: isarId, includeLower: include),
      );
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> isarIdLessThan(
      Id isarId,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: isarId, includeUpper: include),
      );
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> isarIdBetween(
    Id lowerIsarId,
    Id upperIsarId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerIsarId,
        includeLower: includeLower,
        upper: upperIsarId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [id],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idNotEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idGreaterThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [id],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idLessThan(
    String id, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [],
        upper: [id],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idBetween(
    String lowerId,
    String upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [lowerId],
        includeLower: includeLower,
        upper: [upperId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idStartsWith(
      String IdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'id',
        lower: [IdPrefix],
        upper: ['$IdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [''],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'id',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'id',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'parentId',
        value: [null],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'parentId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> parentIdEqualTo(
      String? parentId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'parentId',
        value: [parentId],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdNotEqualTo(String? parentId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'parentId',
              lower: [],
              upper: [parentId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'parentId',
              lower: [parentId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'parentId',
              lower: [parentId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'parentId',
              lower: [],
              upper: [parentId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdGreaterThan(
    String? parentId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'parentId',
        lower: [parentId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdLessThan(
    String? parentId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'parentId',
        lower: [],
        upper: [parentId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> parentIdBetween(
    String? lowerParentId,
    String? upperParentId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'parentId',
        lower: [lowerParentId],
        includeLower: includeLower,
        upper: [upperParentId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdStartsWith(String ParentIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'parentId',
        lower: [ParentIdPrefix],
        upper: ['$ParentIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'parentId',
        value: [''],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      parentIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'parentId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'parentId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'parentId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'parentId',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'authorId',
        value: [null],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'authorId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> authorIdEqualTo(
      String? authorId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'authorId',
        value: [authorId],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdNotEqualTo(String? authorId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'authorId',
              lower: [],
              upper: [authorId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'authorId',
              lower: [authorId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'authorId',
              lower: [authorId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'authorId',
              lower: [],
              upper: [authorId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdGreaterThan(
    String? authorId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'authorId',
        lower: [authorId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdLessThan(
    String? authorId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'authorId',
        lower: [],
        upper: [authorId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> authorIdBetween(
    String? lowerAuthorId,
    String? upperAuthorId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'authorId',
        lower: [lowerAuthorId],
        includeLower: includeLower,
        upper: [upperAuthorId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdStartsWith(String AuthorIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'authorId',
        lower: [AuthorIdPrefix],
        upper: ['$AuthorIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'authorId',
        value: [''],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      authorIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'authorId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'authorId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'authorId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'authorId',
              upper: [''],
            ));
      }
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      isVideoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'isVideo',
        value: [null],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      isVideoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'isVideo',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause> isVideoEqualTo(
      bool? isVideo) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'isVideo',
        value: [isVideo],
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterWhereClause>
      isVideoNotEqualTo(bool? isVideo) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isVideo',
              lower: [],
              upper: [isVideo],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isVideo',
              lower: [isVideo],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isVideo',
              lower: [isVideo],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'isVideo',
              lower: [],
              upper: [isVideo],
              includeUpper: false,
            ));
      }
    });
  }
}

extension PlatformMediaQueryFilter
    on QueryBuilder<PlatformMedia, PlatformMedia, QFilterCondition> {
  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'authorId',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'authorId',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'authorId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'authorId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'authorId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'authorId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      authorIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'authorId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'createdTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'createdTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'createdTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'createdTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'createdTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      createdTimestampBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'createdTimestamp',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'height',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'height',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'height',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'height',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'height',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      heightBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'height',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      idGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      idStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> idMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'id',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isMainIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isMain',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isMainIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isMain',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isMainEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isMain',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isVideoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isVideo',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isVideoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isVideo',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isVideoEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isVideo',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isarIdEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isarIdGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isarIdLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      isarIdBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'isarId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lastEditedTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lastEditedTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastEditedTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastEditedTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastEditedTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      lastEditedTimestampBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastEditedTimestamp',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'loadingTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'loadingTimestamp',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'loadingTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'loadingTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'loadingTimestamp',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      loadingTimestampBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'loadingTimestamp',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'parentId',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'parentId',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'parentId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'parentId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'parentId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'parentId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'parentType',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'parentType',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'parentType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'parentType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'parentType',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentType',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      parentTypeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'parentType',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'platform',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'platform',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'platform',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'platform',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'platform',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'platform',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      platformIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'platform',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'previewUrl',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'previewUrl',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'previewUrl',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'previewUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'previewUrl',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'previewUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      previewUrlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'previewUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'url',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition> urlMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'url',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      urlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'url',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'width',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'width',
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'width',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'width',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'width',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterFilterCondition>
      widthBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'width',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }
}

extension PlatformMediaQueryObject
    on QueryBuilder<PlatformMedia, PlatformMedia, QFilterCondition> {}

extension PlatformMediaQueryLinks
    on QueryBuilder<PlatformMedia, PlatformMedia, QFilterCondition> {}

extension PlatformMediaQuerySortBy
    on QueryBuilder<PlatformMedia, PlatformMedia, QSortBy> {
  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByAuthorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authorId', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByAuthorIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authorId', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByCreatedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'createdTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByCreatedTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'createdTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByHeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByHeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByIsMain() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isMain', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByIsMainDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isMain', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByIsVideo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isVideo', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByIsVideoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isVideo', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByLastEditedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastEditedTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByLastEditedTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastEditedTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByLoadingTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'loadingTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByLoadingTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'loadingTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByParentId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentId', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByParentIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentId', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByParentType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentType', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByParentTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentType', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByPlatform() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'platform', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByPlatformDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'platform', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByPreviewUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'previewUrl', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      sortByPreviewUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'previewUrl', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByWidth() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'width', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> sortByWidthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'width', Sort.desc);
    });
  }
}

extension PlatformMediaQuerySortThenBy
    on QueryBuilder<PlatformMedia, PlatformMedia, QSortThenBy> {
  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByAuthorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authorId', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByAuthorIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authorId', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByCreatedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'createdTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByCreatedTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'createdTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByHeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByHeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsMain() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isMain', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsMainDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isMain', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsVideo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isVideo', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsVideoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isVideo', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByIsarIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByLastEditedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastEditedTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByLastEditedTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastEditedTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByLoadingTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'loadingTimestamp', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByLoadingTimestampDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'loadingTimestamp', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByParentId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentId', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByParentIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentId', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByParentType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentType', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByParentTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'parentType', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByPlatform() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'platform', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByPlatformDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'platform', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByPreviewUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'previewUrl', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy>
      thenByPreviewUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'previewUrl', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByWidth() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'width', Sort.asc);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QAfterSortBy> thenByWidthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'width', Sort.desc);
    });
  }
}

extension PlatformMediaQueryWhereDistinct
    on QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> {
  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByAuthorId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'authorId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct>
      distinctByCreatedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'createdTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByHeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'height');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctById(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'id', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByIsMain() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isMain');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByIsVideo() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isVideo');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct>
      distinctByLastEditedTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastEditedTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct>
      distinctByLoadingTimestamp() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'loadingTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByParentId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'parentId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByParentType(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'parentType', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByPlatform(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'platform', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByPreviewUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'previewUrl', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'url', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformMedia, PlatformMedia, QDistinct> distinctByWidth() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'width');
    });
  }
}

extension PlatformMediaQueryProperty
    on QueryBuilder<PlatformMedia, PlatformMedia, QQueryProperty> {
  QueryBuilder<PlatformMedia, int, QQueryOperations> isarIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isarId');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> authorIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'authorId');
    });
  }

  QueryBuilder<PlatformMedia, DateTime?, QQueryOperations>
      createdTimestampProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'createdTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, double?, QQueryOperations> heightProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'height');
    });
  }

  QueryBuilder<PlatformMedia, String, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<PlatformMedia, bool?, QQueryOperations> isMainProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isMain');
    });
  }

  QueryBuilder<PlatformMedia, bool?, QQueryOperations> isVideoProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isVideo');
    });
  }

  QueryBuilder<PlatformMedia, DateTime?, QQueryOperations>
      lastEditedTimestampProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastEditedTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, DateTime?, QQueryOperations>
      loadingTimestampProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'loadingTimestamp');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> parentIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'parentId');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> parentTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'parentType');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> platformProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'platform');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> previewUrlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'previewUrl');
    });
  }

  QueryBuilder<PlatformMedia, String?, QQueryOperations> urlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'url');
    });
  }

  QueryBuilder<PlatformMedia, double?, QQueryOperations> widthProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'width');
    });
  }
}
