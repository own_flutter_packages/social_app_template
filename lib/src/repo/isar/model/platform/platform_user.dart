import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'platform_user.g.dart';

@Collection(accessor: 'platformUsers')
class PlatformUser {
  Id get isarId => IsarDB.fastHash(id);

  @Index(type: IndexType.value)
  double? get latitude => location.value?.latitude;
  @Index(type: IndexType.value)
  double? get longitude => location.value?.longitude;

  @Index(type: IndexType.hash, replace: true, unique: true)
  late String id;
  int? createdAt;
  IsarLink<LocationQuery> location = IsarLink<LocationQuery>();
  bool? protected;
  bool? verified;
  @Index(type: IndexType.value)
  String? name;
  @Index(type: IndexType.value)
  String? username;
  String? description;
  String? profileImageUrl;
  IsarLinks<Focus> focuses = IsarLinks<Focus>();
  @Index(type: IndexType.value)
  int? age;
  @Index(type: IndexType.value)
  bool own = false;

  static PlatformUser of({
    required String id,
    int? createdAt,
    LocationQuery? location,
    bool? protected,
    bool? verified,
    String? name,
    String? username,
    String? description,
    String? profileImageUrl,
    List<Focus>? focuses,
    int? age,
    bool own = false,
  }) {
    return PlatformUser()
      ..id = id
      ..createdAt = createdAt
      ..location.value = location
      ..protected = protected
      ..verified = verified
      ..name = name
      ..username = username
      ..description = description
      ..profileImageUrl = profileImageUrl
      ..age = age
      ..own = own
      ..focuses.addAll(focuses ?? []);
  }
}
