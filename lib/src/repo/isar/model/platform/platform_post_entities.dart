import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'platform_post_entities.g.dart';

@Collection(accessor: 'platformPostEntities')
class PlatformPostEntities {
  Id? isarId;

  @Index(type: IndexType.value, replace: true, unique: true)
  String? postId;
  IsarLinks<PlatformUser> userMentions = IsarLinks<PlatformUser>();
  IsarLinks<PlatformMedia> media = IsarLinks<PlatformMedia>();
  List<String>? polls;
  List<String>? urls;
  List<String>? hashtags;

  static PlatformPostEntities of({
    required String postId,
    List<PlatformUser>? userMentions,
    List<PlatformMedia>? media,
    List<String>? polls,
    List<String>? urls,
    List<String>? hashtags,
  }) {
    return PlatformPostEntities()
      ..postId = postId
      ..userMentions.addAll(userMentions ?? [])
      ..media.addAll(media ?? [])
      ..polls = polls
      ..urls = urls
      ..hashtags = hashtags;
  }
}
