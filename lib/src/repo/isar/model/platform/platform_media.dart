import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'platform_media.g.dart';

@Collection(accessor: 'platformMedia')
class PlatformMedia {
  Id get isarId => IsarDB.fastHash(id);

  PlatformMedia({
    required this.id,
    this.parentId,
    this.parentType,
    this.url,
    this.previewUrl,
    this.authorId,
    this.platform,
    this.isVideo,
    this.isMain,
    this.loadingTimestamp,
    this.createdTimestamp,
    this.lastEditedTimestamp,
    this.width,
    this.height,
  });

  @Index(type: IndexType.value, replace: true, unique: true)
  final String id;
  @Index(type: IndexType.value)
  String? parentId;
  String? parentType;
  String? url;
  String? previewUrl;
  @Index(type: IndexType.value)
  String? authorId;
  String? platform;
  @Index(type: IndexType.value)
  bool? isVideo;
  bool? isMain;
  DateTime? loadingTimestamp;
  DateTime? createdTimestamp;
  DateTime? lastEditedTimestamp;
  double? width;
  double? height;

  @ignore
  Size? get size => width != null && height != null
      ? Size(
          width!,
          height!,
        )
      : null;

  @ignore
  bool get isValid =>
      (url?.isNotEmpty ?? false) && !url!.contains('ext_tw_video_thumb');
}
