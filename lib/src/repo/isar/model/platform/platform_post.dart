import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'platform_post.g.dart';

@Collection(accessor: 'platformPosts')
class PlatformPost {
  Id get isarId => IsarDB.fastHash(id);

  @Index(type: IndexType.value)
  String? get userId => user.value?.id;

  @Index(type: IndexType.hash, replace: true, unique: true)
  late String id;
  int? createdAt;
  String? text;
  String? parentId;
  String? parentType;
  String? location;
  int? replyCount;
  int? repostCount;
  @Index()
  String? conversationId;
  final IsarLink<PlatformPostEntities> entities =
      IsarLink<PlatformPostEntities>();
  final IsarLink<PlatformUser> user = IsarLink<PlatformUser>();
  bool needsReload = false;

  static of({
    required String id,
    int? createdAt,
    String? text,
    String? parentId,
    String? parentType,
    String? location,
    int? replyCount,
    int? repostCount,
    String? conversationId,
    PlatformPostEntities? entities,
    PlatformUser? user,
    bool? needsReload = false,
  }) {
    return PlatformPost()
      ..id = id
      ..createdAt = createdAt
      ..text = text
      ..parentId = parentId
      ..parentType = parentType
      ..location = location
      ..replyCount = replyCount
      ..repostCount = repostCount
      ..conversationId = conversationId
      ..needsReload = needsReload ?? false
      ..entities.value = entities
      ..user.value = user;
  }

  @ignore
  List<PlatformMedia> get media => entities.value?.media.toList() ?? [];

  @ignore
  List<PlatformMedia> get mediaWithUrl => media
      .toList()
      .where(
        (e) => e.url != null,
      )
      .toList();

  @ignore
  List<PlatformMedia> get mediaWithUrlValid =>
      media.toList().where((e) => e.isValid).toList();
}
