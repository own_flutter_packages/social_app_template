// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'platform_post_entities.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetPlatformPostEntitiesCollection on Isar {
  IsarCollection<PlatformPostEntities> get platformPostEntities =>
      this.collection();
}

const PlatformPostEntitiesSchema = CollectionSchema(
  name: r'PlatformPostEntities',
  id: -6017387150732726288,
  properties: {
    r'hashtags': PropertySchema(
      id: 0,
      name: r'hashtags',
      type: IsarType.stringList,
    ),
    r'polls': PropertySchema(
      id: 1,
      name: r'polls',
      type: IsarType.stringList,
    ),
    r'postId': PropertySchema(
      id: 2,
      name: r'postId',
      type: IsarType.string,
    ),
    r'urls': PropertySchema(
      id: 3,
      name: r'urls',
      type: IsarType.stringList,
    )
  },
  estimateSize: _platformPostEntitiesEstimateSize,
  serialize: _platformPostEntitiesSerialize,
  deserialize: _platformPostEntitiesDeserialize,
  deserializeProp: _platformPostEntitiesDeserializeProp,
  idName: r'isarId',
  indexes: {
    r'postId': IndexSchema(
      id: -544810920068516617,
      name: r'postId',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'postId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {
    r'userMentions': LinkSchema(
      id: 4288800083127776026,
      name: r'userMentions',
      target: r'PlatformUser',
      single: false,
    ),
    r'media': LinkSchema(
      id: -9160704407052030096,
      name: r'media',
      target: r'PlatformMedia',
      single: false,
    )
  },
  embeddedSchemas: {},
  getId: _platformPostEntitiesGetId,
  getLinks: _platformPostEntitiesGetLinks,
  attach: _platformPostEntitiesAttach,
  version: '3.0.5',
);

int _platformPostEntitiesEstimateSize(
  PlatformPostEntities object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final list = object.hashtags;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final list = object.polls;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.postId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.urls;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  return bytesCount;
}

void _platformPostEntitiesSerialize(
  PlatformPostEntities object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeStringList(offsets[0], object.hashtags);
  writer.writeStringList(offsets[1], object.polls);
  writer.writeString(offsets[2], object.postId);
  writer.writeStringList(offsets[3], object.urls);
}

PlatformPostEntities _platformPostEntitiesDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = PlatformPostEntities();
  object.hashtags = reader.readStringList(offsets[0]);
  object.isarId = id;
  object.polls = reader.readStringList(offsets[1]);
  object.postId = reader.readStringOrNull(offsets[2]);
  object.urls = reader.readStringList(offsets[3]);
  return object;
}

P _platformPostEntitiesDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringList(offset)) as P;
    case 1:
      return (reader.readStringList(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringList(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _platformPostEntitiesGetId(PlatformPostEntities object) {
  return object.isarId ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _platformPostEntitiesGetLinks(
    PlatformPostEntities object) {
  return [object.userMentions, object.media];
}

void _platformPostEntitiesAttach(
    IsarCollection<dynamic> col, Id id, PlatformPostEntities object) {
  object.isarId = id;
  object.userMentions
      .attach(col, col.isar.collection<PlatformUser>(), r'userMentions', id);
  object.media.attach(col, col.isar.collection<PlatformMedia>(), r'media', id);
}

extension PlatformPostEntitiesByIndex on IsarCollection<PlatformPostEntities> {
  Future<PlatformPostEntities?> getByPostId(String? postId) {
    return getByIndex(r'postId', [postId]);
  }

  PlatformPostEntities? getByPostIdSync(String? postId) {
    return getByIndexSync(r'postId', [postId]);
  }

  Future<bool> deleteByPostId(String? postId) {
    return deleteByIndex(r'postId', [postId]);
  }

  bool deleteByPostIdSync(String? postId) {
    return deleteByIndexSync(r'postId', [postId]);
  }

  Future<List<PlatformPostEntities?>> getAllByPostId(
      List<String?> postIdValues) {
    final values = postIdValues.map((e) => [e]).toList();
    return getAllByIndex(r'postId', values);
  }

  List<PlatformPostEntities?> getAllByPostIdSync(List<String?> postIdValues) {
    final values = postIdValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'postId', values);
  }

  Future<int> deleteAllByPostId(List<String?> postIdValues) {
    final values = postIdValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'postId', values);
  }

  int deleteAllByPostIdSync(List<String?> postIdValues) {
    final values = postIdValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'postId', values);
  }

  Future<Id> putByPostId(PlatformPostEntities object) {
    return putByIndex(r'postId', object);
  }

  Id putByPostIdSync(PlatformPostEntities object, {bool saveLinks = true}) {
    return putByIndexSync(r'postId', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByPostId(List<PlatformPostEntities> objects) {
    return putAllByIndex(r'postId', objects);
  }

  List<Id> putAllByPostIdSync(List<PlatformPostEntities> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'postId', objects, saveLinks: saveLinks);
  }
}

extension PlatformPostEntitiesQueryWhereSort
    on QueryBuilder<PlatformPostEntities, PlatformPostEntities, QWhere> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhere>
      anyIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhere>
      anyPostId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'postId'),
      );
    });
  }
}

extension PlatformPostEntitiesQueryWhere
    on QueryBuilder<PlatformPostEntities, PlatformPostEntities, QWhereClause> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      isarIdEqualTo(Id isarId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: isarId,
        upper: isarId,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      isarIdNotEqualTo(Id isarId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      isarIdGreaterThan(Id isarId, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: isarId, includeLower: include),
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      isarIdLessThan(Id isarId, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: isarId, includeUpper: include),
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      isarIdBetween(
    Id lowerIsarId,
    Id upperIsarId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerIsarId,
        includeLower: includeLower,
        upper: upperIsarId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'postId',
        value: [null],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'postId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdEqualTo(String? postId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'postId',
        value: [postId],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdNotEqualTo(String? postId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'postId',
              lower: [],
              upper: [postId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'postId',
              lower: [postId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'postId',
              lower: [postId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'postId',
              lower: [],
              upper: [postId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdGreaterThan(
    String? postId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'postId',
        lower: [postId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdLessThan(
    String? postId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'postId',
        lower: [],
        upper: [postId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdBetween(
    String? lowerPostId,
    String? upperPostId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'postId',
        lower: [lowerPostId],
        includeLower: includeLower,
        upper: [upperPostId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdStartsWith(String PostIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'postId',
        lower: [PostIdPrefix],
        upper: ['$PostIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'postId',
        value: [''],
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterWhereClause>
      postIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'postId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'postId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'postId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'postId',
              upper: [''],
            ));
      }
    });
  }
}

extension PlatformPostEntitiesQueryFilter on QueryBuilder<PlatformPostEntities,
    PlatformPostEntities, QFilterCondition> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'hashtags',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'hashtags',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'hashtags',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      hashtagsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'hashtags',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      hashtagsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'hashtags',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hashtags',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'hashtags',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> hashtagsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'hashtags',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isarId',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isarId',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> isarIdBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'isarId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'polls',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'polls',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'polls',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      pollsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'polls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      pollsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'polls',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'polls',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'polls',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> pollsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'polls',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'postId',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'postId',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'postId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      postIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'postId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      postIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'postId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'postId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> postIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'postId',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'urls',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'urls',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'urls',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      urlsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'urls',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
          QAfterFilterCondition>
      urlsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'urls',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'urls',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'urls',
        value: '',
      ));
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> urlsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'urls',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }
}

extension PlatformPostEntitiesQueryObject on QueryBuilder<PlatformPostEntities,
    PlatformPostEntities, QFilterCondition> {}

extension PlatformPostEntitiesQueryLinks on QueryBuilder<PlatformPostEntities,
    PlatformPostEntities, QFilterCondition> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentions(FilterQuery<PlatformUser> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'userMentions');
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'userMentions', length, true, length, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'userMentions', 0, true, 0, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'userMentions', 0, false, 999999, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'userMentions', 0, true, length, include);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'userMentions', length, include, 999999, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> userMentionsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'userMentions', lower, includeLower, upper, includeUpper);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> media(FilterQuery<PlatformMedia> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'media');
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'media', length, true, length, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'media', 0, true, 0, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'media', 0, false, 999999, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'media', 0, true, length, include);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'media', length, include, 999999, true);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities,
      QAfterFilterCondition> mediaLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'media', lower, includeLower, upper, includeUpper);
    });
  }
}

extension PlatformPostEntitiesQuerySortBy
    on QueryBuilder<PlatformPostEntities, PlatformPostEntities, QSortBy> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      sortByPostId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'postId', Sort.asc);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      sortByPostIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'postId', Sort.desc);
    });
  }
}

extension PlatformPostEntitiesQuerySortThenBy
    on QueryBuilder<PlatformPostEntities, PlatformPostEntities, QSortThenBy> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      thenByIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.asc);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      thenByIsarIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.desc);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      thenByPostId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'postId', Sort.asc);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QAfterSortBy>
      thenByPostIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'postId', Sort.desc);
    });
  }
}

extension PlatformPostEntitiesQueryWhereDistinct
    on QueryBuilder<PlatformPostEntities, PlatformPostEntities, QDistinct> {
  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QDistinct>
      distinctByHashtags() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'hashtags');
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QDistinct>
      distinctByPolls() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'polls');
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QDistinct>
      distinctByPostId({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'postId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PlatformPostEntities, PlatformPostEntities, QDistinct>
      distinctByUrls() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'urls');
    });
  }
}

extension PlatformPostEntitiesQueryProperty on QueryBuilder<
    PlatformPostEntities, PlatformPostEntities, QQueryProperty> {
  QueryBuilder<PlatformPostEntities, int, QQueryOperations> isarIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isarId');
    });
  }

  QueryBuilder<PlatformPostEntities, List<String>?, QQueryOperations>
      hashtagsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'hashtags');
    });
  }

  QueryBuilder<PlatformPostEntities, List<String>?, QQueryOperations>
      pollsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'polls');
    });
  }

  QueryBuilder<PlatformPostEntities, String?, QQueryOperations>
      postIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'postId');
    });
  }

  QueryBuilder<PlatformPostEntities, List<String>?, QQueryOperations>
      urlsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'urls');
    });
  }
}
