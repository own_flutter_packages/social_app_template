import 'dart:math';

import 'package:isar/isar.dart';
import 'package:social_app_template/src/modules/map_box/repo/model/map_box_place.dart';

part 'location_query.g.dart';

@Collection(accessor: 'locationQuery')
class LocationQuery {
  late Id id = Isar.autoIncrement;
  @Index(type: IndexType.value)
  double? latitude;
  @Index(type: IndexType.value)
  double? longitude;
  @Index(type: IndexType.value) //, replace: true, unique: true)
  String? query;
  final IsarLink<MapBoxPlace> address = IsarLink<MapBoxPlace>();

  static of({
    Id? id,
    double? latitude,
    double? longitude,
    String? query,
    MapBoxPlace? address,
  }) {
    return LocationQuery()
      ..id = id ?? Isar.autoIncrement
      ..query = query
      ..latitude = latitude
      ..longitude = longitude
      ..address.value = address;
  }

  double? getDistance(latitude, longitude) => calcDistanceByLocations(
        [this.latitude, this.longitude, latitude, longitude],
      );

  static double calcDistanceByLocations(List<double?> coords) {
    double? lat1 = coords.isNotEmpty && coords[0] != null ? coords[0] : null;
    double? lon1 = coords.length > 1 && coords[1] != null ? coords[1] : null;
    double? lat2 = coords.length > 2 && coords[2] != null ? coords[2] : null;
    double? lon2 = coords.length > 3 && coords[3] != null ? coords[3] : null;

    if (lat1 == null || lon1 == null || lat2 == null || lon2 == null) return -1;

    const p = 0.017453292519943295;
    const c = cos;

    final a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * asin(sqrt(a));
  }
}
