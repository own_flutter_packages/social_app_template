// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'focus_metrics.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters

extension GetFocusMetricsCollection on Isar {
  IsarCollection<FocusMetrics> get focusMetrics => this.collection();
}

const FocusMetricsSchema = CollectionSchema(
  name: r'FocusMetrics',
  id: -2038069505397825092,
  properties: {
    r'count': PropertySchema(
      id: 0,
      name: r'count',
      type: IsarType.long,
    ),
    r'focusId': PropertySchema(
      id: 1,
      name: r'focusId',
      type: IsarType.string,
    ),
    r'id': PropertySchema(
      id: 2,
      name: r'id',
      type: IsarType.string,
    ),
    r'lastUpdated': PropertySchema(
      id: 3,
      name: r'lastUpdated',
      type: IsarType.dateTime,
    ),
    r'relevance': PropertySchema(
      id: 4,
      name: r'relevance',
      type: IsarType.double,
    )
  },
  estimateSize: _focusMetricsEstimateSize,
  serialize: _focusMetricsSerialize,
  deserialize: _focusMetricsDeserialize,
  deserializeProp: _focusMetricsDeserializeProp,
  idName: r'isarId',
  indexes: {
    r'id': IndexSchema(
      id: -3268401673993471357,
      name: r'id',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'id',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'focusId': IndexSchema(
      id: 3508210846612319627,
      name: r'focusId',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'focusId',
          type: IndexType.value,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _focusMetricsGetId,
  getLinks: _focusMetricsGetLinks,
  attach: _focusMetricsAttach,
  version: '3.0.5',
);

int _focusMetricsEstimateSize(
  FocusMetrics object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.focusId.length * 3;
  bytesCount += 3 + object.id.length * 3;
  return bytesCount;
}

void _focusMetricsSerialize(
  FocusMetrics object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.count);
  writer.writeString(offsets[1], object.focusId);
  writer.writeString(offsets[2], object.id);
  writer.writeDateTime(offsets[3], object.lastUpdated);
  writer.writeDouble(offsets[4], object.relevance);
}

FocusMetrics _focusMetricsDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = FocusMetrics(
    count: reader.readLong(offsets[0]),
    focusId: reader.readString(offsets[1]),
    id: reader.readString(offsets[2]),
    lastUpdated: reader.readDateTime(offsets[3]),
    relevance: reader.readDoubleOrNull(offsets[4]),
  );
  return object;
}

P _focusMetricsDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLong(offset)) as P;
    case 1:
      return (reader.readString(offset)) as P;
    case 2:
      return (reader.readString(offset)) as P;
    case 3:
      return (reader.readDateTime(offset)) as P;
    case 4:
      return (reader.readDoubleOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _focusMetricsGetId(FocusMetrics object) {
  return object.isarId;
}

List<IsarLinkBase<dynamic>> _focusMetricsGetLinks(FocusMetrics object) {
  return [];
}

void _focusMetricsAttach(
    IsarCollection<dynamic> col, Id id, FocusMetrics object) {}

extension FocusMetricsByIndex on IsarCollection<FocusMetrics> {
  Future<FocusMetrics?> getById(String id) {
    return getByIndex(r'id', [id]);
  }

  FocusMetrics? getByIdSync(String id) {
    return getByIndexSync(r'id', [id]);
  }

  Future<bool> deleteById(String id) {
    return deleteByIndex(r'id', [id]);
  }

  bool deleteByIdSync(String id) {
    return deleteByIndexSync(r'id', [id]);
  }

  Future<List<FocusMetrics?>> getAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndex(r'id', values);
  }

  List<FocusMetrics?> getAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'id', values);
  }

  Future<int> deleteAllById(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'id', values);
  }

  int deleteAllByIdSync(List<String> idValues) {
    final values = idValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'id', values);
  }

  Future<Id> putById(FocusMetrics object) {
    return putByIndex(r'id', object);
  }

  Id putByIdSync(FocusMetrics object, {bool saveLinks = true}) {
    return putByIndexSync(r'id', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllById(List<FocusMetrics> objects) {
    return putAllByIndex(r'id', objects);
  }

  List<Id> putAllByIdSync(List<FocusMetrics> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'id', objects, saveLinks: saveLinks);
  }

  Future<FocusMetrics?> getByFocusId(String focusId) {
    return getByIndex(r'focusId', [focusId]);
  }

  FocusMetrics? getByFocusIdSync(String focusId) {
    return getByIndexSync(r'focusId', [focusId]);
  }

  Future<bool> deleteByFocusId(String focusId) {
    return deleteByIndex(r'focusId', [focusId]);
  }

  bool deleteByFocusIdSync(String focusId) {
    return deleteByIndexSync(r'focusId', [focusId]);
  }

  Future<List<FocusMetrics?>> getAllByFocusId(List<String> focusIdValues) {
    final values = focusIdValues.map((e) => [e]).toList();
    return getAllByIndex(r'focusId', values);
  }

  List<FocusMetrics?> getAllByFocusIdSync(List<String> focusIdValues) {
    final values = focusIdValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'focusId', values);
  }

  Future<int> deleteAllByFocusId(List<String> focusIdValues) {
    final values = focusIdValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'focusId', values);
  }

  int deleteAllByFocusIdSync(List<String> focusIdValues) {
    final values = focusIdValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'focusId', values);
  }

  Future<Id> putByFocusId(FocusMetrics object) {
    return putByIndex(r'focusId', object);
  }

  Id putByFocusIdSync(FocusMetrics object, {bool saveLinks = true}) {
    return putByIndexSync(r'focusId', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByFocusId(List<FocusMetrics> objects) {
    return putAllByIndex(r'focusId', objects);
  }

  List<Id> putAllByFocusIdSync(List<FocusMetrics> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'focusId', objects, saveLinks: saveLinks);
  }
}

extension FocusMetricsQueryWhereSort
    on QueryBuilder<FocusMetrics, FocusMetrics, QWhere> {
  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhere> anyIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhere> anyFocusId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'focusId'),
      );
    });
  }
}

extension FocusMetricsQueryWhere
    on QueryBuilder<FocusMetrics, FocusMetrics, QWhereClause> {
  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> isarIdEqualTo(
      Id isarId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: isarId,
        upper: isarId,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> isarIdNotEqualTo(
      Id isarId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: isarId, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: isarId, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> isarIdGreaterThan(
      Id isarId,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: isarId, includeLower: include),
      );
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> isarIdLessThan(
      Id isarId,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: isarId, includeUpper: include),
      );
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> isarIdBetween(
    Id lowerIsarId,
    Id upperIsarId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerIsarId,
        includeLower: includeLower,
        upper: upperIsarId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> idEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'id',
        value: [id],
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> idNotEqualTo(
      String id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [id],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'id',
              lower: [],
              upper: [id],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdEqualTo(
      String focusId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'focusId',
        value: [focusId],
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdNotEqualTo(
      String focusId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'focusId',
              lower: [],
              upper: [focusId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'focusId',
              lower: [focusId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'focusId',
              lower: [focusId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'focusId',
              lower: [],
              upper: [focusId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause>
      focusIdGreaterThan(
    String focusId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'focusId',
        lower: [focusId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdLessThan(
    String focusId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'focusId',
        lower: [],
        upper: [focusId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdBetween(
    String lowerFocusId,
    String upperFocusId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'focusId',
        lower: [lowerFocusId],
        includeLower: includeLower,
        upper: [upperFocusId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdStartsWith(
      String FocusIdPrefix) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'focusId',
        lower: [FocusIdPrefix],
        upper: ['$FocusIdPrefix\u{FFFFF}'],
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause> focusIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'focusId',
        value: [''],
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterWhereClause>
      focusIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'focusId',
              upper: [''],
            ))
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'focusId',
              lower: [''],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.greaterThan(
              indexName: r'focusId',
              lower: [''],
            ))
            .addWhereClause(IndexWhereClause.lessThan(
              indexName: r'focusId',
              upper: [''],
            ));
      }
    });
  }
}

extension FocusMetricsQueryFilter
    on QueryBuilder<FocusMetrics, FocusMetrics, QFilterCondition> {
  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> countEqualTo(
      int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'count',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      countGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'count',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> countLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'count',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> countBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'count',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'focusId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'focusId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'focusId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'focusId',
        value: '',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      focusIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'focusId',
        value: '',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'id',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'id',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> idIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      idIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'id',
        value: '',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> isarIdEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      isarIdGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      isarIdLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'isarId',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition> isarIdBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'isarId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      lastUpdatedEqualTo(DateTime value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastUpdated',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      lastUpdatedGreaterThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastUpdated',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      lastUpdatedLessThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastUpdated',
        value: value,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      lastUpdatedBetween(
    DateTime lower,
    DateTime upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastUpdated',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relevance',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relevance',
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relevance',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relevance',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relevance',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterFilterCondition>
      relevanceBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relevance',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }
}

extension FocusMetricsQueryObject
    on QueryBuilder<FocusMetrics, FocusMetrics, QFilterCondition> {}

extension FocusMetricsQueryLinks
    on QueryBuilder<FocusMetrics, FocusMetrics, QFilterCondition> {}

extension FocusMetricsQuerySortBy
    on QueryBuilder<FocusMetrics, FocusMetrics, QSortBy> {
  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'count', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'count', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByFocusId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'focusId', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByFocusIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'focusId', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByLastUpdated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdated', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy>
      sortByLastUpdatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdated', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByRelevance() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relevance', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> sortByRelevanceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relevance', Sort.desc);
    });
  }
}

extension FocusMetricsQuerySortThenBy
    on QueryBuilder<FocusMetrics, FocusMetrics, QSortThenBy> {
  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'count', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'count', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByFocusId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'focusId', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByFocusIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'focusId', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByIsarId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByIsarIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isarId', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByLastUpdated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdated', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy>
      thenByLastUpdatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdated', Sort.desc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByRelevance() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relevance', Sort.asc);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QAfterSortBy> thenByRelevanceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relevance', Sort.desc);
    });
  }
}

extension FocusMetricsQueryWhereDistinct
    on QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> {
  QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> distinctByCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'count');
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> distinctByFocusId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'focusId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> distinctById(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'id', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> distinctByLastUpdated() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastUpdated');
    });
  }

  QueryBuilder<FocusMetrics, FocusMetrics, QDistinct> distinctByRelevance() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'relevance');
    });
  }
}

extension FocusMetricsQueryProperty
    on QueryBuilder<FocusMetrics, FocusMetrics, QQueryProperty> {
  QueryBuilder<FocusMetrics, int, QQueryOperations> isarIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isarId');
    });
  }

  QueryBuilder<FocusMetrics, int, QQueryOperations> countProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'count');
    });
  }

  QueryBuilder<FocusMetrics, String, QQueryOperations> focusIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'focusId');
    });
  }

  QueryBuilder<FocusMetrics, String, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<FocusMetrics, DateTime, QQueryOperations> lastUpdatedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastUpdated');
    });
  }

  QueryBuilder<FocusMetrics, double?, QQueryOperations> relevanceProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'relevance');
    });
  }
}
