import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';

part 'focus_metrics.g.dart';

@Collection(accessor: 'focusMetrics')
class FocusMetrics {
  Id get isarId => IsarDB.fastHash(id);

  FocusMetrics({
    required this.id,
    required this.focusId,
    required this.count,
    required this.lastUpdated,
    this.relevance,
  });

  @Index(type: IndexType.hash, replace: true, unique: true)
  final String id;
  @Index(type: IndexType.value, replace: true, unique: true)
  final String focusId;
  final int count;
  final DateTime lastUpdated;
  final double? relevance;
}
