import 'package:isar/isar.dart';
import 'package:social_app_template/social_app_template.dart';
import 'package:uuid/uuid.dart';

part 'focus.g.dart';

@Collection(accessor: 'focus')
class Focus {
  @Index(type: IndexType.value)
  int? get count => metrics.value?.count;

  Id get isarId => IsarDB.fastHash(id);

  @Index(type: IndexType.value, replace: true, unique: true)
  late String id;
  @Index(type: IndexType.value)
  late String value;
  @Index(type: IndexType.value)
  late String name;
  bool userDefining = false;
  bool isGroup = false;
  String? description;
  @Index()
  String? connectedFocusValue;
  final IsarLink<FocusMetrics> metrics = IsarLink<FocusMetrics>();

  static of({
    String? id,
    required String value,
    required String name,
    bool userDefining = false,
    bool isGroup = false,
    String? description,
    String? connectedFocusValue,
    FocusMetrics? metrics,
  }) {
    return Focus()
      ..id = id ?? const Uuid().v1()
      ..value = value
      ..name = name
      ..userDefining = userDefining
      ..isGroup = isGroup
      ..description = description
      ..connectedFocusValue = connectedFocusValue
      ..metrics.value = metrics;
  }
}
