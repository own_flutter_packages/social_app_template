import 'package:social_app_template/social_app_template.dart';

abstract class PlatformApi {
  Future<PlatformPost> getPost(String id);

  Future<List<PlatformPost>> getPosts(List<String> ids);

  Future<PlatformUser> getUser(String id);

  Future<List<PlatformUser>> getUsers(List<String> ids);
}
