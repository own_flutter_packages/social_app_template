import 'package:social_app_template/social_app_template.dart';
import 'package:social_app_template/src/repo/api/platform_api.dart';

/// Not part of public API
class PlatformRepo {
  static late PlatformApi platformApi;
  static PlatformPostRepository postRepository = PlatformPostRepository();

  static PlatformPostRepo get posts => PlatformPostRepo(
        postRepository: postRepository,
        api: platformApi,
      );

  static PlatformUserRepo get users => PlatformUserRepo(api: platformApi);
}

/// Not part of public API
class PlatformPostRepo {
  /// Not part of public API
  PlatformPostRepo({
    required this.postRepository,
    required this.api,
  });

  final PlatformPostRepository postRepository;
  final PlatformApi api;

  Future<PlatformPost> get(String id) async {
    return await _PlatformRepoGet.of<PlatformPost>(
      id: id,
      getByRepository: PlatformPostRepository.getAsync,
      getByApi: api.getPost,
    );
  }

  Future<List<PlatformPost>> getAll(List<String> ids) async {
    return await _PlatformRepoGetAll.of<PlatformPost>(
      ids: ids,
      getAllByRepository: PlatformPostRepository.getByIds,
      getAllByApi: api.getPosts,
      mapId: (e) => e.id,
    );
  }
}

/// Not part of public API
class PlatformUserRepo {
  /// Not part of public API
  PlatformUserRepo({required this.api});

  final PlatformApi api;

  Future<PlatformUser> get(String id) async {
    return await _PlatformRepoGet.of<PlatformUser>(
      id: id,
      getByRepository: PlatformUserRepository.getAsync,
      getByApi: api.getUser,
    );
  }

  Future<List<PlatformUser>> getAll(List<String> ids) async {
    return await _PlatformRepoGetAll.of<PlatformUser>(
      ids: ids,
      getAllByRepository: (ids) => PlatformUserRepository.getAllBy(ids: ids),
      getAllByApi: api.getUsers,
      mapId: (e) => e.id,
    );
  }
}

typedef _GetByRepository<T> = Future<T?> Function(String);
typedef _GetByApi<T> = Future<T> Function(String);

/// Not part of public API
class _PlatformRepoGet {
  static Future<T> of<T>({
    required String id,
    required _GetByRepository<T> getByRepository,
    required _GetByApi<T> getByApi,
  }) async {
    final getResult = await getByRepository(id);
    return getResult ?? await getByApi(id);
  }
}

typedef MapId<T> = String Function(T t);
typedef GetAllByRepository<T> = Future<List<T?>> Function(List<String>);
typedef GetAllByApi<T> = Future<List<T>> Function(List<String>);

/// Not part of public API
class _PlatformRepoGetAll {
  static Future<List<T>> of<T>({
    required List<String> ids,
    required GetAllByRepository<T> getAllByRepository,
    required GetAllByApi<T> getAllByApi,
    required MapId<T> mapId,
  }) async {
    final results = (await getAllByRepository(ids))
        .where((result) => result != null)
        .map((result) => result!)
        .toList();

    final resultIds = results.map((e) => mapId(e)).toSet();

    if (resultIds.length != ids.length) {
      ids.removeWhere((id) => resultIds.contains(id));

      final resultsFromApi = await getAllByApi(ids);

      if (resultsFromApi.isNotEmpty) {
        results.addAll(resultsFromApi);
      }
    }

    return results;
  }
}
