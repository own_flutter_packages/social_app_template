library social_app_template;

import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:social_app_template/src/impl/social_app_template_impl.dart';
import 'package:social_app_template/src/services/image_service.dart';
import 'package:social_app_template/src/widgets/template/list/post_list/platform_posts_list_with_progress.dart';
import 'package:social_app_template/src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart';
import 'package:social_app_template/src/widgets/template/media/platform_video/platform_video_embedded.dart';

import 'src/impl/examples/posts_list_with_examples.dart';
import 'src/widgets/template/list/post_list/platform_posts_list.dart';
import 'src/widgets/template/media/platform_image/gallery/platform_image_embedded_gallery.dart';

export 'src/modules/map_box/repo/model/map_box_place.dart' show MapBoxPlace;
export 'src/modules/map_box/repo/model/map_box_place_context.dart'
    show MapBoxPlaceContext;
export 'src/modules/map_box/repo/repositories/map_box_place_context_repository.dart'
    show MapBoxPlaceContextRepository;
export 'src/modules/map_box/repo/repositories/map_box_place_repository.dart'
    show MapBoxPlaceRepository;
export 'src/modules/social_media/twitter/repo/model/twitter_context_entity.dart'
    show TwitterContextEntity;
export 'src/modules/social_media/twitter/repo/repositories/twitter_context_entity_repository.dart'
    show TwitterContextEntityRepository;
export 'src/repo/isar/db/IsarDB.dart' show IsarDB;
export 'src/repo/isar/model/focus/focus.dart' show Focus;
export 'src/repo/isar/model/focus/focus_metrics.dart' show FocusMetrics;
export 'src/repo/isar/model/geo/location_query.dart' show LocationQuery;
export 'src/repo/isar/model/platform/platform_media.dart' show PlatformMedia;
export 'src/repo/isar/model/platform/platform_post.dart' show PlatformPost;
export 'src/repo/isar/model/platform/platform_post_entities.dart'
    show PlatformPostEntities;
export 'src/repo/isar/model/platform/platform_user.dart' show PlatformUser;
export 'src/repo/isar/repositories/focus_metrics_repository.dart'
    show FocusMetricsRepository;
export 'src/repo/isar/repositories/focus_repository.dart' show FocusRepository;
export 'src/repo/isar/repositories/location_query_repository.dart'
    show LocationQueryRepository;
export 'src/repo/isar/repositories/platform_media_repository.dart'
    show PlatformMediaRepository;
export 'src/repo/isar/repositories/platform_post_entities_repository.dart'
    show PlatformPostEntitiesRepository;
export 'src/repo/isar/repositories/platform_post_repository.dart'
    show PlatformPostRepository;
export 'src/repo/isar/repositories/platform_user_repository.dart'
    show PlatformUserRepository;
export 'src/services/image_service.dart' show ImageService;
export 'src/services/size_service.dart' show SizeService;
export 'src/widgets/core/indicator/progress/progress_indicator.dart'
    show ProgressIndicator, ProgressIndicatorState;
export 'src/widgets/core/media/image/cached/cached_image.dart' show CachedImage;
export 'src/widgets/core/media/image/image_embedded.dart' show ImageEmbedded;
export 'src/widgets/template/list/post_list/platform_posts_list.dart'
    show
        PlatformPostList,
        PlatformPostListState,
        OnRefresh,
        OnInitPost,
        OnTapPost;
export 'src/widgets/template/list/post_list/platform_posts_list_with_progress.dart'
    show PlatformPostListWithProgress, PlatformPostListWithProgressState;
export 'src/widgets/template/media/platform_image/platform_user_profile_image/platform_user_image_profile.dart'
    show PlatformUserProfileImage;
export 'src/widgets/template/media/platform_media_embedded.dart'
    show PlatformMediaEmbedded, PlatformMediaEmbeddedState;

/// Package to enable you to create a social app in a very quick way.
part 'src/social_app_template.dart';

/// Global constant to access SocialApp.
// ignore: non_constant_identifier_names
final SocialAppTemplateInterface SocialApp = SocialAppTemplateImpl();
